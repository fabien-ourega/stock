<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Produit extends Model
{
    use SoftDeletes;

    public function categorie()
    {
        return $this->belongsTo('App\Categorie','categorie_id');
    }

    public function scategorie()
    {
        return $this->belongsTo('App\Scategorie','scategorie_id');
    }

    public function type()
    {
        return $this->belongsTo('App\Typeproduit','type_id');
    }
}
