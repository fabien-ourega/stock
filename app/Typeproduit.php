<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Typeproduit extends Model
{
    use SoftDeletes;
    protected $table="typelibelles";
}
