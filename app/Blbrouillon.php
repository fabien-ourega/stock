<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Blbrouillon extends Model
{
    use SoftDeletes;
    protected $table="bon_livraison_brouillons";

    public function project()
    {
        return $this->belongsTo('App\Project','pjt_id');
    }

    public function client()
    {
        return $this->belongsTo('App\Client','client_id');
    }
}
