<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockHistory extends Model
{
    protected $table="stock_histories";

    public function project()
    {
        return $this->belongsTo('App\Project','project_id');
    }

    public function produit()
    {
        return $this->belongsTo('App\Produit','produit_id');
    }
}
