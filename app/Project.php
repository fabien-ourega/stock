<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
    use SoftDeletes;

    public function produits()
    {
        return $this->hasMany('App\Bstock','pjt_id');
    }
}
