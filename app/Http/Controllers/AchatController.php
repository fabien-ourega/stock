<?php

namespace App\Http\Controllers;

use App\Achat;
use App\AchatItem;
use App\Bstock;
use App\Fournisseur;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Imports\ImportGachat;
use App\Magasin;
use App\Mpaiement;
use App\Produit;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;

class AchatController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $achats = Achat::orderBy('id','desc')->where('status','<>',0)->get();
        if (!in_array('add_achat', Session::get('permissions'))){
            return redirect()->back()->with('success',"Vous n'avez pas le droit d'accéder à cette page");
        }
        return view('achats.index',compact('achats'));
    }

    public function rejeter() {
        $achats = Achat::orderBy('id','desc')->where('status',0)->get();
        if (!in_array('add_achat', Session::get('permissions'))){
            return redirect()->back()->with('success',"Vous n'avez pas le droit d'accéder à cette page");
        }
        return view('achats.rejeter',compact('achats'));
    }

    public function create()
    {
        $fournisseurs = Fournisseur::get();
        $produits = Produit::orderBy('nom','asc')->get();
        $modes = Mpaiement::get();
        $magasins = Magasin::get();

        if (!in_array('add_achat', Session::get('permissions'))){
            return redirect()->back()->with('success',"Vous n'avez pas le droit d'accéder à cette page");
        }

        return view('achats.create',compact('fournisseurs','produits','modes','magasins'));
    }

    public function getRefBC()
    {
        $code = "ACH".date('ym');
        $count = Achat::withTrashed()->count() + 1;
        $num=sprintf("%06d",$count);
        $val = $code.'-'.$num;

        return $val;
    }

    public function store(Request $request)
    {
        //dd($request->all());
        $valider = Validator::make($request->all(), [
            'reference' => ['required'],
            'fournid' => ['required'],
            'dateachat' => ['required'],
            'mode' =>['required'],
            'file' =>['required'],
            'datasession' =>['required'],
            'magasin' =>['required'],
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }

        $slug = $this->getRefBC();
        //dd($slug);
        $bc = new Achat();
        $bc->slug = $slug;
        $bc->magasin_id = $request->magasin;
        $bc->fournisseur_id = $request->fournid;
        $bc->reference = $request->reference;
        $bc->mode_id = $request->mode;
        $bc->dateachat = $request->dateachat;
        $bc->datepaiement = $request->datepaiement;
        $bc->user_id = Auth::user()->id;
        $bc->status = 2;
        if($request->file('file')){
            $file=$request->file('file');
            $extension = $file->getClientOriginalExtension() ?: 'png';
            $folderName = 'assets/achats/';
            $picture = Str::random(8).'.'. $extension;
            $file->move($folderName,$picture);
            $bc->fichier = $picture;
        }

        $bc->save();

        $mttsm = 0;
        foreach ($request->datasession as $data){
            $item = explode(';',$data);
            //dd($item);
            //select Bstock
            //$check = AchatItem::where('achat_id',$bc->id)->first();
            // if(!$check){
                $pxht = $item[4] + (!empty($item[5]) ? (($item[5] /100) * $item[4]) : 0);
                $mttble = $pxht * floatval($item[3]);
                $mttsm = $mttsm + $mttble;

                $bcitem = new AchatItem();
                $bcitem->achat_id = $bc->id;
                $bcitem->produit_id = $item[0];
                $bcitem->qte = $item[3];
                $bcitem->price = $item[4];
                $bcitem->taxe = !empty($item[5]) ? $item[5] : 0;
                $bcitem->mtt = $mttble;
                $bcitem->save();

                //Save stock magasin
                $stock = Bstock::where('magasin_id',$request->magasin)->where('pdt_id',$item[0])->first();
                if(!$stock){
                    $stock = new Bstock();
                    $stock->magasin_id = $request->magasin;
                    $stock->pdt_id = $item[0];
                    $stock->qte_first = 0;
                    $stock->qte = $item[3];
                    $stock->save();
                }else{
                    $stock->qte = $stock->qte + $item[3];
                    $stock->save();
                }

            // }
        }
        // dd($mttsm);
        $bcitem = Achat::where('id',$bc->id)->first();
        $bcitem->montant = $mttsm;
        $bcitem->save();

        return redirect()->route('achats')->with('success','L\'achat <b>'.$slug.'</b> a bien été ajouté.');


    }

    public function show($id)
    {
        if (!in_array('add_achat', Session::get('permissions'))){
            return redirect()->back()->with('success',"Vous n'avez pas le droit d'accéder à cette page");
        }

        $achat = Achat::where('id',$id)->first();
        return view('achats.show',compact('achat'));
    }

    public function getFournisseurUpload($libel) {
        $fournisseur = Fournisseur::where('libelle',$libel)->first();
        if(!$fournisseur){
            $fournisseur = new Fournisseur();
            $fournisseur->libelle = $libel;
            $fournisseur->contact = 0000000000;
            $fournisseur->save();

        }
        return $fournisseur->id;
    }

    public function getProduitUpload($libel) {
        $produit = Produit::where('nom',$libel)->first();
        if(!$produit){
            $produitid = 556;
        }else{
            $produitid = $produit->id;
        }
        return $produitid;
    }

    function excel(Request $request) {
        //dd($request->all());
        $file = $request->file('file');
        $ext = $file->getClientOriginalExtension();
        if(!in_array($ext,['xls','xlsx'])) {
            $response = "Ce type de fichier (<b>.{$ext}</b>) n'est pas autorisé !";
        }else{
            try{
                $filedata = Excel::toCollection(new ImportGachat, $file);
                $filedata = $filedata[0]->toArray();
                //dump($filedata);

                if(count($filedata)){
                    $summtt = [];
                    foreach ($filedata as $key => $row) {
                        if($key != 0){
                            if(is_numeric($row[0])){
                                $datefacture = Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[0]))->format('Y-m-d');
                            }else{
                                $datefacture = Carbon::createFromFormat('d/m/Y', $row[0])->format('Y-m-d');
                            }
                            //dd($datefacture,$row[0]);

                            if($row[0]){
                                //verifier si la facture existe
                                $acht = Achat::where('reference',$row[2])->first();
                                if(!$acht){

                                    // save un achat
                                    $acht = new Achat();
                                    $acht->slug = $this->getRefBC();
                                    $acht->fournisseur_id = $this->getFournisseurUpload($row[1]);
                                    $acht->reference = $row[2];
                                    $acht->mode_id = 1;
                                    $acht->dateachat = $datefacture;
                                    $acht->datepaiement = $datefacture;
                                    $acht->user_id = 1;
                                    $acht->status = 2;
                                    $acht->save();

                                    // initialiser summtt
                                    $summtt[$acht->id] = 0;
                                }
                                $idachat = $acht->id;

                                //Save les item achats
                                $achtitem = new AchatItem();
                                $achtitem->achat_id = $idachat;
                                $achtitem->produit_id = $this->getProduitUpload($row[4]);
                                $achtitem->qte = $row[5];
                                $achtitem->price = $row[6];
                                $achtitem->taxe = 18;
                                $achtitem->mtt = $row[14];
                                $achtitem->save();

                                //summtt
                                $summtt[$idachat] = $summtt[$idachat] + $row[14];

                                // dd($check,$row[1]);
                            }
                        }
                    }

                    //update les montants
                    foreach ($summtt as $key => $value) {
                        $bcitem = Achat::where('id',$key)->first();
                        $bcitem->montant = $value;
                        $bcitem->save();
                    }
                }
            $response = "Votre fichier a été importé avec succès !<br>";

            }catch (\Exception $e){
                $response = $e->getMessage();
                dd($response);
            }
        }

        return redirect()->back()->with('success',$response);
    }

    public function valid($id) {
        $id = decrypt($id);
        $ach = Achat::where('id',$id)->with('items')->first();
        if(!$ach){
            return redirect()->back()->with('error','Achat non trouvé');
        }

        //update stock
        foreach ($ach->items as $item) {
            $stock = Bstock::where('magasin_id',$ach->magasin_id)->where('pdt_id',$item->produit_id)->first();
            if(!$stock){
                $stock = new Bstock();
                $stock->magasin_id = $ach->magasin_id;
                $stock->pdt_id = $item->produit_id;
                $stock->qte_first = 0;
                $stock->qte = $item->qte;
                $stock->save();
            }else{
                $stock->qte = $stock->qte + $item->qte;
                $stock->save();
            }
        }

        $ach->status = 2;
        $ach->save();

        return redirect()->back()->with('success','Bon de livraison validé avec succès');
    }

    public function reject(Request $request) {
        //dd($request->all());
        //Validator
        $validator = Validator::make($request->all(), [
            'tidsluge' => ['required'],
            'motif' => ['required'],
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('error','Veuillez renseigner le motif de rejet');
        }

        $id = decrypt($request->tidsluge);

        $bl = Achat::find($id);
        $bl->status = 0;
        $bl->motif = $request->motif;
        $bl->save();

        return redirect()->back()->with('success','Achat a bien été rejeté.');

    }

}
