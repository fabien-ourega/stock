<?php

namespace App\Http\Controllers;

use App\Fournisseur;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class FournisseurController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $fourns = Fournisseur::get();
        return view('fournisseurs.index',compact('fourns'));
    }

    public function create()
    {
        return view('fournisseurs.create');
    }

    public function store(Request $request){
        //dd($request->all());
        $valider = Validator::make($request->all(),[
            'libelle' =>'required',
            'contact' =>'required',
        ]);

        if($valider->fails()){
            //dd($valider->errors());
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $cat = new Fournisseur();
            $cat->libelle = $request->libelle ;
            $cat->contact = $request->contact ;
            $cat->email = $request->email ;
            $cat->adresse = $request->adresse ;
            $cat->save();

            return redirect()->route('frsseur')->with('success','✔ Félicitation ! vous venez d\' ajoute un founisseur');
        }
    }

    public function edit($id){
        $item = Fournisseur::find($id);
        return view('fournisseurs.edit',compact('item'));
    }

    public function update(Request $request){
        //dd($request->all());
        $valider = Validator::make($request->all(),[
            'libelle' =>'required',
            'contact' =>'required',
            'idfourn' =>'required',
        ]);

        if($valider->fails()){
            //dd($valider->errors());
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $cat = Fournisseur::where('id',$request->idfourn)->first();
            $cat->libelle = $request->libelle ;
            $cat->contact = $request->contact ;
            $cat->email = $request->email ;
            $cat->adresse = $request->adresse ;
            $cat->save();

            return redirect()->route('frsseur')->with('success','Vous venez de modifier un fournisseur');
        }
    }

    public function delet($id){
        //dd($id);
        if($id){
            $role = Fournisseur::findOrFail($id);
            $role->delete();
            return redirect()->route('frsseur')->with('success','Vous venez de supprimer un fournisseur');
        }
        return redirect()->back();
    }

    public function allDelet(Request $request){
        //dd($request->all());
        $ids = explode(',',$request->value);

        foreach ($ids as $id){
            $role = Fournisseur::findOrFail($id);
            $role->delete();
        }
        echo $data = '1';
    }
}
