<?php

namespace App\Http\Controllers;

use App\Bl;
use App\Bstock;
use App\Categorie;
use App\Exports\BcExport;
use App\Exports\FactureExport;
use App\Exports\GstockExport;
use App\Exports\MagasinExport;
use App\Exports\ProduitExport;
use App\Exports\ProjetExport;
use App\Magasin;
use App\Produit;
use App\Project;
use App\Typeproduit;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use PDF;
use Maatwebsite\Excel\Facades\Excel;

class EtatsController extends Controller
{
    public $typereturn ;

    public function __construct()
    {
        $this->middleware('auth');
        $this->typereturn = "pdf";
    }

    public function gstock(Request $request)
    {
        //ini_set('memory_limit', '-1');
        //dd($request->all());
        Validator::make($request->all(), [
            'project' => ['required'],
            'nbpdtd'=>['required'],
        ])->validate();

        //dd($request->all());
        if($request->typexport){
            $this->typereturn = $request->typexport;
        }

        $customerArray = $request->project;
        if($customerArray[0] == 'all'){
            $pjt = Project::pluck('id');
            $customerArray = $pjt->toArray();
            $customerArray[] = 0;
        }

        $selectnbpdt = $request->nbpdtd;
        $selectcatId = [];
        if(isset($request->categorie)){
            $selectcatId = $request->categorie;
        }
        try{
            if($this->typereturn == 'pdf'){
                //$newcustomer = Arr::forget($customerArray, 0);
                $pdf = PDF::loadView('gstocks.download_etat',compact('customerArray','selectcatId','selectnbpdt'));
                $pdf->setPaper('a4', 'landscape');
                return view('gstocks.download_etat',compact('customerArray','selectcatId','selectnbpdt'));
                //return $pdf->stream('etat_gestion_stock-'.date("d-m-y").'.pdf');
                //return $pdf->download('etat_gestion_stock-'.date("d-m-y").'.pdf');
            }else{
                $date=new \DateTime('now');
                $date = $date->format('d-m-Y');
                $lib= "rapport-gestion-des-stock_".$date.'.xlsx';
                return Excel::download(new GstockExport($customerArray,$selectcatId,$selectnbpdt), $lib);
            }

        }catch (\Exception $e){
            //return redirect()->back()->with('error','Une erreur est survenue veuillez réessayer ultérieurement.');
            dd($e->getMessage());
        }


    }

    public function bc(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(), [
            'project' => ['required'],
        ])->validate();

        $requete = Bl::with('project')->where('status',2)->where('archive','0');

        $rproject = $request->project;
        $rstatus = $request->status;
        $rtype = $request->type;
        $date = $request->ddebut;
        $date2 = $request->dfin;
        //$rbon = $request->bon;

        if(isset($rproject)=== true){
            if($rproject != 0){
                $requete->where('pjt_id',$rproject);
            }
        }

        if(isset($rstatus)){
            $requete->where('etat',$rstatus);
        }

        if(isset($rtype)){
            $requete->where('type',$rtype);
        }

        if(isset($request->ddebut) and isset($request->dfin)){
            $dateD = $date .' 00:00:00';
            $dateF = $date2 .' 23:59:59';

            $requete->whereBetween('created_at',[$dateD, $dateF]);
        }

        $datas = $requete->get();
        //dd($datas);

        if(count($datas)>0){

            if($request->typexport){
                $this->typereturn = $request->typexport;
            }


            if($this->typereturn == 'pdf'){

                if($request->imprim == "2"){
                    //imprimer les bons de livraison multiple
                    $html = "";

                    foreach ($datas as $bl){
                        $view= view('bl.download_print_bl',compact('bl'));
                        $html .= $view->render();
                    }
                    $allhtml = '<html>'.$html.'</html>';
                    //return $allhtml;
                    $pdf = PDF::loadHTML($allhtml);
                    $sheet = $pdf->setPaper('a4','portrait');
                    return $sheet->download('impression_bon_livraison-'.date("d-m-y").'.pdf');

                }else{
                    $pdf = PDF::loadView('bl.download_etat',compact('datas','date','date2'));
                    return view('bl.download_etat',compact('datas','date','date2'));
                    return $pdf->download('etat_bon_commande-'.date("d-m-y").'.pdf');
                }

            }else{
                try{
                    $date=new \DateTime('now');
                    $date = $date->format('d-m-Y');
                    $lib= "rapport-bon-de-commande_".$date.'.xlsx';
                    return Excel::download(new BcExport($datas,$date,$date2), $lib);

                }catch (\Exception $e){
                    //return redirect()->back()->with('error','Une erreur est survenue veuillez réessayer ultérieurement.');
                    dd($e->getMessage());
                }
            }

        }
        return redirect()->back()->with('error','Une erreur est survenue veuillez réessayer ultérieurement.');
    }

    public function pdt(Request $request)
    {
        //set memory size
        //ini_set('memory_limit', '-1');
        //dd($request->all());

        $datas = Produit::with('categorie')->with('scategorie')->with('type')->get();

        if(count($datas)>0){

            $date = null;
            $date2 = null;

            if($request->typexport){
                $this->typereturn = $request->typexport;
            }

            if($this->typereturn == 'pdf'){

                $pdf = PDF::loadView('produits.download_etat',compact('datas'));
                $pdf->setPaper('a4', 'landscape');
                return view('produits.download_etat',compact('datas'));
                //return $pdf->download('etat_produits-'.date("d-m-y").'.pdf');

            }else{
                try{
                    $date=new \DateTime('now');
                    $date = $date->format('d-m-Y');
                    $lib= "rapport-produit_".$date.'.xlsx';
                    return Excel::download(new ProduitExport($datas), $lib);

                }catch (\Exception $e){
                    //return redirect()->back()->with('error','Une erreur est survenue veuillez réessayer ultérieurement.');
                    dd($e->getMessage());
                }
            }

        }
        return redirect()->back()->with('error','Une erreur est survenue veuillez réessayer ultérieurement.');
    }



    public function factures(Request $request)
    {
        //dd($request->all());
        $rproject = $request->project;
        $rstatus = $request->status;
        $rmode = $request->mode;
        $date = $request->ddebut;
        $date2 = $request->dfin;
        $client = $request->client;

        $requete = Bl::with('project')->with('modepaie')->with('client')->where('etat',1);

        if(isset($rproject)=== true){
            if($rproject != 0){
                $requete->where('pjt_id',$rproject);
            }
        }

        if(isset($client)){
            $requete->where('client_id',$client);
        }

        if(isset($rstatus)){
            $requete->where('etat_facture',$rstatus);
        }

        if(isset($rmode) and count($rmode)>0){
            $requete->whereIn('mode_facture',$rmode);
        }

        if(isset($request->ddebut) and isset($request->dfin)){
            $dateD = $date .' 00:00:00';
            $dateF = $date2 .' 23:59:59';

            $requete->whereBetween('updated_at',[$dateD, $dateF]);
        }

        $datas = $requete->orderBy('id','desc')->get();
        $datas->map(function ($item){
            $item->total = (new \App\Http\Controllers\FacturesController())->getMontantTotal($item->id,'all');
        });

        if($request->typexport){
            $this->typereturn = $request->typexport;
        }

        //dd($datas);
        if($this->typereturn == 'pdf'){

            if($request->imprim == "2"){
                //imprimer les bons de livraison multiple
                $html = "";

                foreach ($datas as $bl){
                    $view= view('factures.download_all',compact('bl'));
                    $html .= $view->render();
                }
                $allhtml = '<html>'.$html.'</html>';
                //return $allhtml;
                $pdf = PDF::loadHTML($allhtml);
                $sheet = $pdf->setPaper('a4','portrait');
                return $sheet->download('impression_factures-'.date("d-m-y").'.pdf');

            }else{
                $pdf = PDF::loadView('factures.download_etat',compact('datas','date','date2'));
                //return view('factures.download_etat',compact('datas','date','date2'));
                return $pdf->download('etat_factures-'.date("d-m-y").'.pdf');
            }

        }else{
            try{

                $date=new \DateTime('now');
                $date = $date->format('d-m-Y');
                $lib= "rapport-facture_".$date.'.xlsx';
                //return view('factures.download_etat_excel',compact('datas','date','date2'));
                return Excel::download(new FactureExport($datas,$date,$date2), $lib);

            }catch (\Exception $e){
                //return redirect()->back()->with('error','Une erreur est survenue veuillez réessayer ultérieurement.');
                dd($e->getMessage());
            }
        }

    }

    public function projets(Request $request)
    {
        //ini_set('memory_limit', '-1');
        //dd($request->all());
        Validator::make($request->all(), [
            'project' => ['required'],
        ])->validate();

        //dd($request->all());
        if($request->typexport){
            $this->typereturn = $request->typexport;
        }

        $customerArray = $request->project;
        if($customerArray[0] == 'all'){
            $pjt = Project::pluck('id');
            $customerArray = $pjt->toArray();
            $customerArray[] = 0;
        }

        $datas = Project::whereIn('id',$customerArray)->with('produits')->get();

        try{
            if($this->typereturn == 'pdf'){
                //$newcustomer = Arr::forget($customerArray, 0);
                $pdf = PDF::loadView('projects.download_etat',compact('datas'));
                $pdf->setPaper('a4', 'landscape');
                return view('projects.download_etat',compact('datas'));
                //return $pdf->stream('etat_gestion_stock-'.date("d-m-y").'.pdf');
                //return $pdf->download('etat_gestion_stock-'.date("d-m-y").'.pdf');
            }else{
                $date=new \DateTime('now');
                $date = $date->format('d-m-Y');
                $lib= "rapport-projets_".$date.'.xlsx';
                return Excel::download(new ProjetExport($datas), $lib);
            }

        }catch (\Exception $e){
            //return redirect()->back()->with('error','Une erreur est survenue veuillez réessayer ultérieurement.');
            dd($e->getMessage());
        }


    }

    public function magasin(Request $request){
        //ini_set('memory_limit', '-1');
        //dd($request->all());
        Validator::make($request->all(), [
            'magasin' => ['required'],
            'nbpdtd'=>['required'],
        ])->validate();

        //dd($request->all());
        if($request->typexport){
            $this->typereturn = $request->typexport;
        }

        $customerArray = $request->magasin;
        if($customerArray[0] == 'all'){
            $magasin = Magasin::pluck('id');
            $customerArray = $magasin->toArray();
            $customerArray[] = 0;
        }

        $selectnbpdt = $request->nbpdtd;

        try{
            if($this->typereturn == 'pdf'){
                //$newcustomer = Arr::forget($customerArray, 0);
                $pdf = PDF::loadView('magasins.download_etat',compact('customerArray','selectnbpdt'));
                $pdf->setPaper('a4', 'landscape');
                //return view('magasins.download_etat',compact('customerArray','selectnbpdt'));
                //return $pdf->stream('etat_magasin-'.date("d-m-y").'.pdf');
                return $pdf->download('etat_magasin-'.date("d-m-y").'.pdf');
            }else{
                $date=new \DateTime('now');
                $date = $date->format('d-m-Y');
                $lib= "etat-magasin_".$date.'.xlsx';
                return Excel::download(new MagasinExport($customerArray,$selectnbpdt), $lib);
            }

        }catch (\Exception $e){
            //return redirect()->back()->with('error','Une erreur est survenue veuillez réessayer ultérieurement.');
            dd($e->getMessage());
        }


    }

}
