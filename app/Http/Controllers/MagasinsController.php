<?php

namespace App\Http\Controllers;

use App\Bstock;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Magasin;
use App\Produit;

class MagasinsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $magasins = Magasin::get();
        $magasins->map(function ($magasin){
            $magasin->qte = Bstock::where('magasin_id',$magasin->id)->has('produit')->count();
            return $magasin;
        });
        return view('magasins.index',compact('magasins'));
    }

    public function create()
    {
        return view('magasins.create');
    }

    public function store(Request $request)
    {
        //dd($request->all());
        $this->validate($request, [
            'nom' => 'required',
            'adresse' => 'required',
        ]);

        $magasin = new Magasin();
        $magasin->nom = $request->nom;
        $magasin->adresse = $request->adresse;
        $magasin->description = $request->description;
        $magasin->save();

        //assignation des produits
        $produits = Produit::get();
        foreach ($produits as $produit) {
            $stock = new Bstock();
            $stock->magasin_id = $magasin->id;
            $stock->pdt_id = $produit->id;
            $stock->qte_first = 0;
            $stock->save();
        }

        return redirect()->route('magasins')->with('success','Magasin créé avec succès');
    }

    public function show($id)
    {
        $magasin = Magasin::where('id',$id)->with('bstocks')->first();
        $stocks = Bstock::where('magasin_id',$magasin->id)->with('produit')->get();
        $othermagasin = Magasin::where('id','<>',$id)->get();
        return view('magasins.show',compact('magasin','stocks','othermagasin'));
    }

    public function edit($id)
    {
        $magasin = Magasin::find($id);
        return view('magasins.edit',compact('magasin'));
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'nom' => 'required',
            'adresse' => 'required',
            'slug' => 'required',
        ]);

        $magasin = Magasin::find($request->slug);
        $magasin->nom = $request->nom;
        $magasin->adresse = $request->adresse;
        $magasin->description = $request->description;
        $magasin->save();
        return redirect()->route('magasins')->with('success','Magasin modifié avec succès');
    }

    public function destroy($id)
    {
        $magasin = Magasin::find($id);
        $magasin->delete();
        return redirect()->route('magasins')->with('success','Magasin supprimé avec succès');
    }

    public function appro(Request $request) {
        //dd($request->all());
        $this->validate($request, [
            'astock' => 'required',
            'idslug' => 'required',
            'slugpjt' => 'required',
        ]);

        $request->astock =str_replace(' ', '', $request->astock);

        $pdt = Produit::where('id',$request->idslug)->first();
        if($pdt){
            $bstock = Bstock::where('pdt_id',$pdt->id)->where('magasin_id',$request->slugpjt)->first();
            if($bstock){
                $newqte = $bstock->qte + $request->astock ;
                $bstock->qte = $newqte;

            }else{
                $bstock = new Bstock();
                $bstock->qte = $request->astock;
                $bstock->pdt_id = $pdt->id;
                $bstock->magasin_id = $request->slugpjt;
            }
            $bstock->save();

            //save historie
            $this->saveStockHistorie('approvisionnement',null,$request->slugpjt,$request->astock,'magasin',$request->idslug);

            return redirect()->back()->with('success',"L'approvisionnement a bien été effectué.");
        }

        return redirect()->back()->with('error','Une erreur est survenue veuillez réessayer ultérieurement');

    }

    public function reduir(Request $request) {
        //dd($request->all());
        $this->validate($request, [
            'astock' => 'required',
            'idslug' => 'required',
            'slugpjt' => 'required',
        ]);

        $request->astock =str_replace(' ', '', $request->astock);

        if($request->astock > $request->dispo){
            return redirect()->back()->with('error','La quantité a réduire doit être inférieur à la quantité disponible');
        }

        $pdt = Produit::where('id',$request->idslug)->first();
        if($pdt){
            $bstock = Bstock::where('pdt_id',$pdt->id)->where('magasin_id',$request->slugpjt)->first();
            if($bstock){
                $newqte = $bstock->qte - $request->astock ;
                $bstock->qte = $newqte;
                $bstock->save();
            }

            //save historie
            $this->saveStockHistorie('reduction',$request->idslug,null,$request->astock,'magasin',$request->slugpjt);

            return redirect()->back()->with('success',"La réduction a bien été effectué.");
        }

        return redirect()->back()->with('error','Une erreur est survenue veuillez réessayer ultérieurement');

    }

    public function transfer(Request $request)
    {
        //dd($request->all());
        $this->validate($request, [
            'dispo' =>'required',
            'astock' =>'required','min:1',
            'pjt' =>'required',
            'idslug' =>'required',
            'slugpjt' =>'required',
        ]);

        //verifier si la quantite est suffisant
        $bstock = Bstock::where('pdt_id',$request->idslug)->where('magasin_id',$request->slugpjt)->first();
        if(!$bstock){
            return redirect()->back()->with('error','Une erreur est survenue veuillez réessayer ultérieurement');
        }
        $request->astock =str_replace(' ', '', $request->astock);

        if($bstock->qte >= $request->astock){

            //send on stock generale

            if($request->pjt == 0){
                $check = Bstock::where('pdt_id',$request->idslug)->whereNull('magasin_id')->first();
                if($check){
                    $newqte = $check->qte + $request->astock ;
                    $check->qte = $newqte;
                }else{
                    $check = new Bstock();
                    $check->qte = $request->astock ;
                    $check->pdt_id = $request->idslug;
                    //$check->pjt_id = $request->pjt;
                }
                $check->save();
            }else{

                //Send à un project
                $check = Bstock::where('pdt_id',$request->idslug)->where('magasin_id',$request->pjt)->first();
                if($check){
                    $newqte = $check->qte + $request->astock ;
                    $check->qte = $newqte;
                }else{
                    $check = new Bstock();
                    $check->qte = $request->astock ;
                    $check->pdt_id = $request->idslug;
                    $check->magasin_id = $request->pjt;
                }
                $check->save();
            }
            //reduit le produit
            $bstock->qte = $bstock->qte - $request->astock;
            $bstock->save();

            //save historie
            $this->saveStockHistorie('transfere',$request->idslug,null,$request->astock,'magasin',$request->pjt);

            return redirect()->back()->with('success',"Le transfère a bien été effectué.");

        }
        return redirect()->back()->with('error','La quantité a transféré doit être inférieur à la quantité disponible');

        //dd($bstock);
    }

}
