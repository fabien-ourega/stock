<?php

namespace App\Http\Controllers;

use App\Bl;
use App\Blbrouillon;
use App\Blitem;
use App\Bstock;
use App\Client;
use App\Project;
use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class BlivraisonsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $bons = Bl::with('project')->where('status','2')->where('archive','0')->where('etat','0')->orderBy('id','desc')->get();
        $projects = Project::get();
        //dd($bons);
        return view('bl.index',compact('bons','projects'));
    }

    public function indexwaiting()
    {
        $bons = Bl::with('project')->where('status','1')->where('archive','0')->where('etat','0')->orderBy('id','desc')->get();
        $projects = Project::get();
        //dd($bons);
        return view('bl.index_waiting',compact('bons','projects'));
    }

    public function indexrejeter()
    {
        $bons = Bl::with('project')->where('status','0')->orderBy('id','desc')->get();
        $projects = Project::get();
        return view('bl.index_rejeter',compact('bons','projects'));
    }

    public function validerbl($slug){
        $id = decrypt($slug);

        $bl = Bl::find($id);
        $bl->status = 2;
        $bl->save();

        return redirect()->back()->with('success','Bon de livraison validé avec succès');
    }

    public function rejetbl(Request $request) {
        //dd($request->all());
        //Validator
        $validator = Validator::make($request->all(), [
            'tidsluge' => ['required'],
            'motif' => ['required'],
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('error','Veuillez renseigner le motif de rejet');
        }

        $id = decrypt($request->tidsluge);

        $bl = Bl::find($id);
        $bl->status = 0;
        $bl->motif = $request->motif;
        $bl->save();

        //Remettrer les produits en stock
        $items = Blitem::where('bl_id',$id)->get();
        foreach ($items as $item){
            $bstock = Bstock::where('pdt_id',$item->pdt_id)->where('pjt_id',$item->pjt_id)->where('id',$item->bstock_id)->first();
            if($bstock){
                $bstock->qte = $bstock->qte + $item->qte;
                $bstock->save();
            }
        }

        return redirect()->back()->with('success','Bon de commande a bien été rejeté.');

    }

    public function indexvalid()
    {
        $bons = Bl::with('project')->where('archive','0')->where('etat','1')->orderBy('id','desc')->get();
        $projects = Project::get();
        //dd($bons);
        return view('bl.index_valid',compact('bons','projects'));
    }

    public function indexarchive()
    {
        $listbons = Bl::with('project')->where('archive','1')->orderBy('id','desc')->get();
        $bons = Bl::with('project')->groupBy([\DB::raw("YEAR(updated_at)")])->where('archive','1')->orderBy('id','asc')->get();
        $q = null;
        //dd($bons);
        return view('bl.index_archive_group',compact('bons','listbons','q'));
        //return view('bl.index_archive',compact('bons','projects'));
    }

    public function indexarchiveitem($var)
    {
        $listbons = Bl::with('project')->where('archive','1')->whereYear('updated_at',$var)->orderBy('id','desc')->get();
        $bons = Bl::with('project')->groupBy([\DB::raw("YEAR(updated_at)")])->where('archive','1')->orderBy('id','asc')->get();
        $q = $var;

        return view('bl.index_archive_group',compact('bons','listbons','q'));
    }

    public function indexbrouillon()
    {
        $bons = Blbrouillon::with('project')->orderBy('id','desc')->get();
        return view('bl.index_brouillon',compact('bons'));
    }

    public function create()
    {
        $projects = Project::where('etat',0)->get();
        $clients = Client::get();
        if(in_array('add_bc',Session::get('permissions'))){
            return view('bl.create',compact('projects','clients'));
        }
        return redirect()->back()->with('error','Vous n\'avez pas les droits nécessaires pour accéder à cette page.');
    }

    public function getpjt($id)
    {
        if($id != '0'){
            $quartier = Bstock::where('pjt_id',$id)->with(['produit'=>function($item){
                $item->with('type');
            }])->has('produit')->get();
            return $quartier;
        }
        return null;
    }

    public function getdispopjt($qte,$id,$idpjt)
    {
        //dd($qte,$id);
        $bstock = Bstock::where('pjt_id',$idpjt)->where('id',$id)->first();
        //dd($bstock);
        if($bstock){
            $nb = $bstock->qte;

            if($nb >= $qte){
                return 'ok';
            }
            return $nb;
        }
        return "error";
    }

    public function getdispomagasin($qte,$id,$idpjt){
        $bstock = Bstock::where('magasin_id',$idpjt)->where('pdt_id',$id)->first();
        //dd($bstock);
        if($bstock){
            $nb = $bstock->qte;

            if($nb >= $qte){
                return 'ok';
            }
            return $nb;
        }
        return "error";
    }

    public function store(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(), [
            'reference' => ['required'],
            'clientid' => ['required'],
            'pjt' => ['required'],
            'datasession' =>['required'],
            'type' =>['required'],
            //'file' =>['required'],
        ])->validate();

        //dd('de');
        $blcheck = Bl::where('pjt_id',$request->pjt)->count();
        $pjts = Project::find($request->pjt);
        $slug1 = 'BL'. date('dmy');
        $slug2 ='BC '.sprintf("%'.03d", $blcheck+1).'_'.$pjts->libelle;
        $slug = $slug1.' / '.$slug2;
        //dd($slug);
        $bl = new Bl();

        $oldfile = null;
        if (isset($request->idoldbrouillon)){
            $blold = Blbrouillon::where('id',$request->idoldbrouillon)->first();
            $oldfile = $blold->fichier;
            $blold->delete();
        }


        if(isset($request->bouton) and $request->bouton=="brouillon"){
            $va = $this->storeBrouillon($request->all(),$slug,$oldfile);
            return redirect()->route('bl')->with('success','La bon de livraison a bien enregistré en brouillon.');
        }


        $bl->slug = $slug;
        $bl->reference = $request->reference;
        $bl->type = $request->type;
        $bl->client_id = $request->clientid;
        $bl->pjt_id = $request->pjt;
        $bl->fichier = $oldfile;

        if($request->file('file')){
            $file=$request->file('file');
            $extension = $file->getClientOriginalExtension() ?: 'png';
            $folderName = 'bc/';
            $picture = Str::random(8).'.'. $extension;
            $file->move($folderName,$picture);
            $bl->fichier = $picture;
        }

        $bl->save();

        foreach ($request->datasession as $data){
            $item = explode(';',$data);
            //select Bstock
            $bstock = Bstock::where('id',$item[0])->with('produit')->first();
            //dd($item,$bstock);
            if($bstock){
                $check = Blitem::where('pdt_id',$bstock->pdt_id)->where('bl_id',$bl->id)->get();
                if (count($check) == 0){
                    $blitem = new Blitem();
                    $blitem->bl_id = $bl->id;
                    $blitem->pdt_id = $bstock->pdt_id;
                    $blitem->price = $bstock->produit->price;
                    $blitem->pjt_id = $request->pjt;
                    $blitem->qtecmd = $item[2];
                    $blitem->qte = $item[3];
                    $blitem->qterestant = $item[4];
                    $blitem->bstock_id = $bstock->id;
                    $blitem->save();

                    $bstock->qte = $bstock->qte - $item[3];
                    $bstock->save();
                }else{
                    $check = Blitem::where('pdt_id',$bstock->pdt_id)->where('bl_id',$bl->id)->first();
                    $check->qtecmd = $check->qtecmd + $item[2];
                    $check->qte = $check->qte + $item[3];
                    $check->qterestant = $check->qterestant + $item[4];
                    $check->save();

                    $bstock->qte = $bstock->qte - $item[3];
                    $bstock->save();
                }
            }
        }

        return redirect()->route('bl')->with('success','La bon de livraison '.$slug.' a bien été ajouté.');
    }

    public function storeBrouillon($request,$slug,$oldfilname)
    {
        //dd($request['file']);
        $bl = new Blbrouillon();
        $bl->slug = $slug;
        $bl->reference = $request['reference'];
        $bl->type = $request['type'];
        $bl->client_id = $request['clientid'];
        $bl->pjt_id = $request['pjt'];
        $bl->donnes = json_encode($request['datasession']);
        $oldfilname ? $bl->fichier = $oldfilname : '';

        if(isset($request['file'])){
            $file=$request['file'];
            $extension = $file->getClientOriginalExtension() ?: 'png';
            $folderName = 'bc/';
            $picture = Str::random(10).'.'. $extension;
            $file->move($folderName,$picture);
            $bl->fichier = $picture;
        }
        $bl->save();

        return 1;
    }

    public function storeUpdate(Request $request)
    {
        dd($request->all());
        Validator::make($request->all(), [
            'clientid' => ['required'],
            'pjt' => ['required'],
            'datasession' =>['required'],
        ])->validate();

        if(isset($request['idoldbrouillon'])){
            $bl = Blbrouillon::where('id',$request['idoldbrouillon'])->first();
            $oldfile = $bl->fichier;
            $bl->delete();
        }

        if(isset($request->bouton) and $request->bouton=="brouillon"){
            $va = $this->storeBrouillon($request->all());
            return redirect()->route('bl')->with('success','La bon de livraison a bien enregistré en brouillon.');
        }

    }

    public function status(Request $request)
    {
        //dd($request->all());
        $bon = Bl::find($request->id);
        if($bon){
            $bon->etat = $request->status;
            $bon->save();

            return 1 ;
        }
        return 0 ;
    }

    public function show($id)
    {
        $bl = Bl::where('id',$id)->with('project')->with('client')->first();
        $listbls = Blitem::where('bl_id',$id)->with('produits')->get();
        return view('bl.show',compact('bl','listbls'));
    }

    public function destroy($id)
    {
        //dd($id);
        $item = Bl::find($id);
        if($item){
            //restaure qte pdf
            $this->restoreQte($id);

            //delete BLITEM
            $blitem = Blitem::where('bl_id',$id)->delete();
            $item->delete();
            return redirect()->back()->with('success','Vous venez de supprimer un BC');

        }
        return redirect()->back();
    }

    public function allDelet(Request $request){
        //dd($request->all());
        $ids = explode(',',$request->value);
        //dd($ids);
        foreach ($ids as $id){
            $user = Bl::findOrFail($id);
            $user->delete();
        }
        echo $data = '1';
    }

    public function archive($id)
    {
        //dd($id);
        $item = Bl::find($id);
        if($item){
            //restaure qte pdf
            $item->archive = 1 ;
            $item->save();

            return redirect()->back()->with('success','Vous venez d\'archiver un BC');

        }
        return redirect()->back();
    }

    public function allArchiv(Request $request)
    {
        //dd($request->all());
        $ids = explode(',',$request->value);
        //dd($ids);
        foreach ($ids as $id){
            $item = Bl::findOrFail($id);
            $item->archive = 1 ;
            $item->save();
        }
        echo $data = '1';
    }

    public function restoreQte($id)
    {
        $item = Bl::find($id);
        if($item){
            $blitems = Blitem::where('bl_id',$id)->get();

            foreach($blitems as $item){
                $bstock = Bstock::where('pjt_id',$item->pjt_id)->where('pdt_id',$item->pdt_id)->with('produit')->first();
                if ($bstock) {
                    $bstock->qte = $bstock->qte + $item->qte;
                    $bstock->save();
                }
            }
        }
    }

    public function download($id)
    {
        $bl = Bl::where('id',$id)->with('project')->with('client')->first();
        if($bl){
            $listbls = Blitem::where('bl_id',$id)->with('produits')->get();

            $pdf = PDF::loadView('bl.download',compact('bl','listbls'));
            //return view('bl.download',compact('bl','listbls'));
            return $pdf->download('bl_'.$bl->slug.'-'.date("d-m-y").'.pdf');

        }
        return redirect()->back();

    }

    public function downloadBc($id)
    {
        $bl = Bl::where('id',$id)->with('project')->with('client')->first();
        if($bl){
            $listbls = Blitem::where('bl_id',$id)->with('produits')->get();

            $pdf = PDF::loadView('bl.download_bc',compact('bl','listbls'));
            //return view('bl.download_bc',compact('bl','listbls'));
            return $pdf->download('bc_'.$bl->slug.'-'.date("d-m-y").'.pdf');

        }
        return redirect()->back();
    }

    public function blitem($id)
    {
        $bl = Bl::where('id',$id)->with('project')->with('client')->first();
        $listbls = Blitem::where('bl_id',$id)->with('produits')->get();
        return view('bl.itembl',compact('bl','listbls'));
    }

    public function validbl(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(), [
            'tidsluge' => ['required'],
            'file' => ['required'],
        ])->validate();

        $bl = Bl::where('id',$request->tidsluge)->where('etat',0)->first();
        if($bl){
            $bl->etat = 1;
            if($request->file('file')){
                $file=$request->file('file');
                $extension = $file->getClientOriginalExtension() ?: 'png';
                $folderName = 'bc/';
                $picture = Str::random(10).'.'. $extension;
                $file->move($folderName,$picture);
                $bl->file_bl = $picture;
            }
            //mettre a jours les produits qui on changer de prix
            $this->updateBlPdt($request->tidsluge);

            $bl->save();

            return redirect()->route('bl')->with('success','Le bon a bien été traité.');

        }
        return redirect()->back()->with('error','Une erreur est survenue veuillez réessayer ultérieurement.');
    }

    public function updateBlPdt($idbl)
    {
        $blitems = Blitem::where('bl_id',$idbl)->with('produits')->get();
        foreach($blitems as $item){
            if($item->produits) {
                if($item->produits->price != $item->price){
                    $item->price = $item->produits->price;
                    $item->save();
                }
            }
        }
    }

    public function editbl(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(), [
            'tidsluge' => ['required'],
            'file' => ['required'],
        ])->validate();

        $bl = Bl::where('id',$request->tidsluge)->first();
        //dd($bl);
        if($bl){
            if($request->file('file')){
                if($request->type=='bl'){

                    if(file_exists('bc/'.$bl->file_bl)){
                        unlink('bc/'.$bl->file_bl);
                    }
                    $file=$request->file('file');
                    $extension = $file->getClientOriginalExtension() ?: 'png';
                    $folderName = 'bl/';
                    $picture = Str::random(10).'.'. $extension;
                    $file->move($folderName,$picture);
                    $bl->file_bl = $picture;

                }else{

                    if(file_exists('bc/'.$bl->fichier)){
                        unlink('bc/'.$bl->fichier);
                    }

                    $file=$request->file('file');
                    $extension = $file->getClientOriginalExtension() ?: 'png';
                    $folderName = 'bc/';
                    $picture = Str::random(8).'.'. $extension;
                    $file->move($folderName,$picture);
                    $bl->fichier = $picture;
                }
            }

            $bl->save();

            return redirect()->back()->with('success','Le bon a bien été modifié.');

        }
        return redirect()->back()->with('error','Une erreur est survenue veuillez réessayer ultérieurement.');
    }

    public function editblref(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(), [
            'tidsluge' => ['required'],
            'ref' => ['required'],
        ])->validate();

        $bl = Bl::where('id',$request->tidsluge)->first();
        //dd($bl);
        if($bl){
            $bl->reference = $request->ref;
            $bl->save();

            return redirect()->back()->with('success','Le bon a bien été modifié.');

        }
        return redirect()->back()->with('error','Une erreur est survenue veuillez réessayer ultérieurement.');
    }


    public function showBrouillons($id)
    {
        //dd($id);
        $bl = Blbrouillon::where('id',$id)->with('project')->with('client')->first();
        return view('bl.show_brouillon',compact('bl'));
    }

    public function editBrouillons($id)
    {
        //dd($id);
        $projects = Project::get();
        $clients = Client::get();
        $bl = Blbrouillon::where('id',$id)->with('project')->with('client')->first();
        $dats = json_decode($bl->donnes);
        //dd($dats);
        //supprimer les produits supprimer
        foreach($dats as $i=>$item){
            $values = explode(";", $item);
            $bstock = Bstock::where('id',$values[0])->has('produit')->first();
            if(!$bstock) {
                unset($dats[$i]);
            }
        };

        return view('bl.edit_brouillon',compact('bl','projects','clients','dats'));
    }

    public function destroyBrouillons($id)
    {
        //dd($id);
        $item = Blbrouillon::find($id);
        if($item){
            $item->delete();
            return redirect()->back()->with('success','Vous venez de supprimer un brouillon');

        }
        return redirect()->back();
    }



}
