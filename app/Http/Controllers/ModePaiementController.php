<?php

namespace App\Http\Controllers;

use App\Mpaiement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ModePaiementController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $modes =Mpaiement::get();
        return view('modepaiement.index',compact('modes'));
    }

    public function store(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(), [
            'libelle' => ['required'],
        ])->validate();

        $mode = new Mpaiement();
        $mode->libelle = $request->libelle;
        $mode->save();

        return redirect()->route('modepaie')->with('success','Le mode a bien été ajouté.');
    }

    public function edit($id)
    {
        $mode = Mpaiement::find($id);
        return view('modepaiement.edit',compact('mode'));
    }

    public function update(Request $request)
    {
        Validator::make($request->all(), [
            'libelle' => ['required'],
            'slug' => ['required'],
        ])->validate();

        $mode = Mpaiement::where('id',$request->slug)->first();
        if($mode){
            $mode->libelle = $request->libelle;
            $mode->save();
            return redirect()->route('modepaie')->with('success','Le mode a bien été modifié.');
        }
        return redirect()->route('modepaie')->with('error','Une erreur est survenue veuillez réessayer ultérieurement');
    }

    public function destroy($id){
        //dd($id);
        if($id){
            $cat = Mpaiement::findOrFail($id);
            $cat->delete();
            return redirect()->route('modepaie')->with('success','Vous venez de supprimer un mode de paiement');
        }
        return redirect()->back();
    }
}
