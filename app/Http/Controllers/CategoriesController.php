<?php

namespace App\Http\Controllers;

use App\Actu;
use App\Categorie;
use App\Scategorie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class CategoriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $cats = Categorie::with('scategorie')->get()->map(function ($scats){
            $scats->setRelation('scategorie',$scats->scategorie->take(4));
            return $scats;
        });
        return view('categories.index',compact('cats'));
    }

    public function store(Request $request){
        //dd($request->all());
        $valider = Validator::make($request->all(),[
            'nom' =>'required',
        ]);

        if($valider->fails()){
            //dd($valider->errors());
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $cat = new Categorie();
            $cat->libelle = $request->nom ;
            $cat->slug= Str::slug($request->nom);
            $cat->description = $request->description ;
            $cat->save();

            return redirect()->route('categories')->with('success','✔ Félicitation ! vous venez d\' ajoute une catégorie');
        }
    }

    public function edit($id){
        $cat = Categorie::find($id);
        return view('categories.edit',compact('cat'));
    }

    public function update(Request $request){
        //dd($request->all());
        $valider = Validator::make($request->all(),[
            'nom' =>'required',
            'idActu'=>'required'
        ]);

        if($valider->fails()){
            //dd($valider->errors());
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $cat = Categorie::where('slug',$request->idActu)->first();
            $cat->libelle = $request->nom ;
            $cat->slug= Str::slug($request->nom);
            $cat->description = $request->description ;
            $cat->save();

            return redirect()->route('categories')->with('success','Vous venez de modifier une catégorie');
        }
    }

    public function delet($id){
        //dd($id);
        if($id){
            $cat = Categorie::findOrFail($id);
            $cat->delete();
            return redirect()->route('categories')->with('success','Vous venez de supprimer une catégorie');
        }
        return redirect()->back();
    }

    public function allDelet(Request $request){
        //dd($request->all());
        $ids = explode(',',$request->value);
        //dd($ids);
        foreach ($ids as $id){
            $cat = Categorie::findOrFail($id);
            $cat->delete();
        }
        echo $data = '1';
    }


    public function show($slug)
    {
        $cat = Categorie::where('id',$slug)->with('scategorie')->first();
        //dd($cat);
        if(isset($cat->id)){
            return view('categories.sindex',compact('cat'));
        }

        return redirect()->back();
    }

    public function sstore(Request $request)
    {
        //dd($request->all());
        $valider = Validator::make($request->all(),[
            'nom' =>'required',
            'slugcat' =>'required',
        ]);

        if($valider->fails()){
            //dd($valider->errors());
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $cat = new Scategorie();
            $cat->libelle = $request->nom ;
            $cat->slug= Str::slug($request->nom);
            $cat->description = $request->description ;
            $cat->cat_id = $request->slugcat ;
            $cat->save();

            return redirect()->route('categories.show',$request->slugcat)->with('success','✔ Félicitation ! vous venez d\' ajoute une sous catégorie');
        }
    }

    public function supdate(Request $request){
        //dd($request->all());
        $valider = Validator::make($request->all(),[
            'nom' =>'required',
            'idActu'=>'required',
            'slugid'=>'required'
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $cat = Scategorie::where('slug',$request->idActu)->first();
            $cat->libelle = $request->nom ;
            $cat->slug= Str::slug($request->nom);
            $cat->description = $request->description ;
            $cat->save();

            return redirect()->route('categories.show',$request->slugid)->with('success','Vous venez de modifier une sous catégorie');
        }
    }

    public function sedit($id){
        $cat = Scategorie::find($id);
        return view('categories.sedit',compact('cat'));
    }

    public function sdelet($id){
        //dd($id);
        if($id){
            $cat = Scategorie::findOrFail($id);
            $cat->delete();
            return redirect()->back()->with('success','Vous venez de supprimer une sous catégorie');
        }
        return redirect()->back();
    }

    public function getscat($id)
    {
        if($id != '0'){
            $quartier = Scategorie::where('cat_id',$id)->get();
            return $quartier;
        }
        return null;
    }

//    public function getNbArticle($id){
//        dd($id);
//        $actu = Actu::where('cat_id','like');
//    }
}
