<?php

namespace App\Http\Controllers;

use App\Bstock;
use App\Categorie;
use App\Imports\ImportProduitCodif;
use App\Produit;
use App\Project;
use App\Scategorie;
use App\Typeproduit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Facades\Excel;

class ProduitsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (!in_array('show_pdt', Session::get('permissions'))){
            return redirect()->back()->with('success',"Vous n'avez pas le droit d'accéder à cette page");
        }
        $produits = Produit::with('categorie')->with('scategorie')->with('type')->get();
        return view('produits.index',compact('produits'));
    }

    public function create()
    {
        if (!in_array('add_pdt', Session::get('permissions'))){
            return redirect()->back()->with('success',"Vous n'avez pas le droit d'accéder à cette page");
        }

        $cats = Categorie::get();
        $types = Typeproduit::get();
        return view('produits.create',compact('cats','types'));
    }

    public function store(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(), [
            'nom' => ['required','unique:produits'],
            'code'=>['required'],
        ])->validate();

        //Vérifier si le produit existe déjà par le code
        $produits = Produit::where('code',$request->code)->first();
        if($produits){
            return redirect()->back()->with('error','Le code saisie existe déjà');
        }

        $produit = new Produit();
        $produit->code = $request->code;
        $produit->nom = $request->nom;
        $produit->categorie_id = $request->cat_id;
        $produit->scategorie_id = $request->scat_id ?: '';
        $produit->type_id = $request->type;
        $produit->price = $this->del_espace($request->price);
        $produit->save();

        //Assigner le produit à tous les projets
        $projets = Project::get();
        foreach ($projets as $key => $projet) {
            $check = Bstock::where('pjt_id',$projet->id)->where('pdt_id',$produit->id)->first();
            if(!$check){
                $stock = new Bstock();
                $stock->pjt_id = $projet->id;
                $stock->pdt_id = $produit->id;
                $stock->qte_first = 0;
                $stock->save();
            }
        }

        return redirect()->route('produits')->with('success','Le produit a bien été ajouté');
    }

    public function edit($id)
    {
        $produit= Produit::where('id',$id)->with('categorie')->with('scategorie')->with('type')->first();
        if(isset($produit->id)){

            $cats = Categorie::get();
            $scats = Scategorie::get();
            $types = Typeproduit::get();

            return view('produits.edit',compact('produit','cats','types','scats'));
        }
        return redirect()->back();
    }

    public function update(Request $request)
    {
        //dd($request->all());
        $id = $request->slugid;
        Validator::make($request->all(), [
            'nom' => ['required',Rule::unique('produits')->ignore($id)],
            'code' => ['required',Rule::unique('produits')->ignore($id)],
        ])->validate();

        $produit = Produit::where('id',$id)->first();
        if(!$produit){
            return redirect()->back();
        }

        // if($produit->nom != $request->nom){
        //     $produits = Produit::where('nom',$request->nom)->first();
        //     if($produits){
        //         return redirect()->back()->with('error','Le produit saisie existe déjà');
        //     }
        // }

        //SAVOIR SI LE PRICE A CHANGE
        if($produit->price != $this->del_espace($request->price)){
            $this->saveActionUser("Modification du prix du produit","Le produit ".$produit->nom." a changé de prix. Ancien prix : ".$produit->price." Nouveau prix : ".$this->del_espace($request->price));
        }


        $produit->code = $request->code;
        $produit->nom = $request->nom;
        $produit->categorie_id = $request->cat_id;
        $produit->scategorie_id = $request->scat_id ?: '';
        $produit->type_id = $request->type;
        $produit->price = $this->del_espace($request->price);
        $produit->save();

        $this->saveActionUser("Modification d'un produit","Modification du produit ".$produit->nom);

        return redirect()->route('produits')->with('success','Le produit a bien été modifié');
    }

    public function destroy($id)
    {
        //dd($id);
        $cat = Produit::findOrFail($id);
        $cat->nom = $cat->nom.now();
        $this->saveActionUser("Suppression d'un produit","Suppression du produit ".$cat->nom);
        $cat->save();
        $cat->delete();
        return redirect()->back()->with('success','Vous venez de supprimer un produit');
    }

    public function types()
    {
        $types = Typeproduit::get();
        return view('produits.type',compact('types'));
    }

    public function storeType(Request $request)
    {
        Validator::make($request->all(), [
            'libelle' => ['required'],
        ])->validate();

        $type = new Typeproduit();
        $type->libelle = $request->libelle;
        $type->save();

        return redirect()->route('produits.types')->with('success','✔ Félicitation ! vous venez d\'ajouter un type');
    }

    public function updateType(Request $request)
    {
        Validator::make($request->all(), [
            'libelle' => ['required'],
            'idslug' => ['required'],
        ])->validate();

        $type = Typeproduit::where('id',$request->idslug)->first();
        if($type){
            $type->libelle = $request->libelle;
            $type->save();
        }

        return redirect()->route('produits.types')->with('success','✔ Félicitation ! vous venez de modifier le type');
    }


    public function excel(Request $request) {
        //dd($request->all());
        $file = $request->file('file');
        $ext = $file->getClientOriginalExtension();
        if(!in_array($ext,['xls','xlsx'])) {
            $response = "Ce type de fichier (<b>.{$ext}</b>) n'est pas autorisé !";
        }else{
            try{
                $filedata = Excel::toCollection(new ImportProduitCodif, $file);
                $filedata = $filedata[0]->toArray();
                //dd($filedata);
                //IMPORT STOCK MAGASIN
                if(count($filedata)){
                    foreach ($filedata as $key => $row) {
                        //dump($row);
                        if($key != 0){
                            //dd($row);
                            if($row[1]){
                                $check = Bstock::where('magasin_id',3)->where('pdt_id',$row[1])->first();
                                if($check){
                                    $check->qte = $row[5]??0;
                                    $check->save();
                                }else{
                                    //verifier si le produit existe
                                    $check = Produit::where('id',$row[1])->first();
                                    if(!$check){
                                        $produit = new Produit();
                                        $produit->code = $row[2];
                                        $produit->nom = $row[3];
                                        $produit->categorie_id = 1;
                                        $produit->scategorie_id = 1;
                                        $produit->type_id = 1;
                                        $produit->price = 0;
                                        $produit->save();
                                    }

                                    $stock = new Bstock();
                                    $stock->magasin_id = 3;
                                    $stock->pdt_id = $row[1];
                                    $stock->qte = $row[5]??0;
                                    $stock->save();
                                }
                                // dd($check,$row[1]);
                            }

                        }
                    }
                }

                // if(count($filedata)){
                //     foreach ($filedata as $key => $row) {
                //         if($key != 0){
                //             if($row[0]){
                //                 $check = Produit::where('id',$row[0])->first();
                //                 if($check){
                //                     $check->code = $row[1];
                //                     $check->save();
                //                 }
                //                 // dd($check,$row[1]);
                //             }

                //         }
                //     }
                // }
            $response = "Votre fichier a été importé avec succès !<br>";

            }catch (\Exception $e){
                $response = $e->getMessage();
                dd($response);
            }
        }

        return redirect()->back()->with('success',$response);
    }


}
