<?php

namespace App\Http\Controllers;

use App\Bl;
use App\Bstock;
use App\Categorie;
use App\Produit;
use App\Project;
use App\Typeproduit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ProjectsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $projects = Project::with('produits')->where('etat',0)->orderBy('created_at','desc')->get();
        $cats = Categorie::get();
        if (!in_array('add_pjt', Session::get('permissions'))){
            return redirect()->back()->with('success',"Vous n'avez pas le droit d'accéder à cette page");
        }
        return view('projects.index',compact('projects','cats'));
    }

    public function cloturer(){
        $projects = Project::with('produits')->where('etat',1)->orderBy('created_at','desc')->get();
        $cats = Categorie::get();
        if (!in_array('add_pjt', Session::get('permissions'))){
            return redirect()->back()->with('success',"Vous n'avez pas le droit d'accéder à cette page");
        }
        return view('projects.cloturer',compact('projects','cats'));
    }

    public function create()
    {
        dd('index');
    }

    public function store(Request $request)
    {
        //dd($request->all());
        $valider = Validator::make($request->all(),[
            'libelle' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $cat = new Project();
            $cat->libelle = $request->libelle ;
            $cat->slug= Str::slug($request->libelle);
            $cat->description = $request->description ;
            $cat->save();

            if(count($request->categorie_id)>0){
                foreach ($request->categorie_id as $key => $value) {
                    //select tous les produits de la categorie
                    $produits = Produit::where('categorie_id',$value)->get();

                    foreach ($produits as $key => $produit) {
                        // $check = Bstock::where('pjt_id',$cat->id)->where('pdt_id',$produit->id)->first();
                        // if(!$check){
                            $stock = new Bstock();
                            $stock->pjt_id = $cat->id;
                            $stock->pdt_id = $produit->id;
                            $stock->qte_first = 0;
                            $stock->save();
                        // }
                    }
                }
            }
            return redirect()->route('projects')->with('success','✔ Félicitation ! vous venez d\' ajoute un projet');
        }
    }

    public function show($id)
    {
        //dd($id);
        $project = Project::where('slug',$id)->first();
        if($project){
            $produits = Produit::get();
            $stocks = Bstock::where('pjt_id',$project->id)->with('produit')->get();
            $bls = Bl::where('pjt_id',$project->id)->get();
            //dd($stocks);
            return view('projects.show',compact('project','produits','stocks','bls'));
        }
        return redirect()->back();
    }

    public function update(Request $request)
    {
        //dd($request->all());
        $valider = Validator::make($request->all(),[
            'libelle' =>'required',
            'idslug' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $cat = Project::where('id',$request->idslug)->first();
            if($cat){
                $cat->libelle = $request->libelle ;
                $cat->slug= Str::slug($request->libelle);
                $cat->description = $request->description ;
                $cat->save();
            }

            return redirect()->route('projects')->with('success','✔ Félicitation ! vous venez d\' ajoute un projet');
        }
    }

    public function destroy($id)
    {
        //dd($id);
        $cat = Project::findOrFail($id);
        $cat->delete();
        return redirect()->back()->with('success','Vous venez de supprimer un projet');
    }

    public function clore($id) {
        $cat = Project::findOrFail($id);
        //verifier si le projet est vide
        $check = Bstock::where('pjt_id',$cat->id)->where('qte','<>',0)->has('produit')->first();
        //dd($check);
        if($check){
            return redirect()->back()->with('error','Le projet n\'est pas vide, vous ne pouvez pas le clôturer');
        }
        $cat->etat = 1;
        $cat->save();

        return redirect()->back()->with('success','Vous venez de clôturer un projet');
    }

    //show

    public function storeProject(Request $request)
    {
        //dd($request->all());
        $valider = Validator::make($request->all(),[
            'pdt' =>'required',
            'qte' =>'required',
            'idpjt' =>'required',
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{
            //check
            $check = Bstock::where('pjt_id',$request->idpjt)->where('pdt_id',$request->pdt)->first();
            if($check){
                return redirect()->back()->with('error','Le produit est déjà assigné au projet');
            }

            $stock = new Bstock();
            $stock->pjt_id = $request->idpjt;
            $stock->pdt_id = $request->pdt;
            $stock->qte_first = $request->qte;
            $stock->save();

            $project = Project::find($request->idpjt);

            return redirect()->route('projects.show',$project->slug)->with('success','✔ Félicitation ! vous venez d\' assigné un produit');
        }
    }

    public function destroyProject($id,$idpjt)
    {
        $stock = Bstock::where('id',$id)->where('pjt_id',$idpjt)->first();
        //dd($stock);
        if($stock){
            $stock->delete();
            return redirect()->back()->with('success','Vous venez de supprimer un produit');
        }
        return redirect()->back();
    }

    public function updateProject(Request $request)
    {
        //dd($request->all());
        $valider = Validator::make($request->all(),[
            'pdt' =>'required',
            'qte' =>'required',
            'idpjt' =>'required',
        ]);

        $bstock = Bstock::where('pjt_id',$request->idpjt)->where('id',$request->idslug)->first();
        if($bstock){
            $bstock->qte_first = $request->qte;
            $bstock->save();
        }
        return redirect()->back();
    }
}
