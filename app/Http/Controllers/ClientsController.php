<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ClientsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $clients = Client::get();
        return view('clients.index',compact('clients'));
    }

    public function create()
    {
        return view('clients.create');
    }

    public function store(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(), [
            'nom' => ['required'],
            'adresse' => ['required'],
            'contact' => ['required'],
        ])->validate();

        $client = new Client();
        $client->nom = $request->nom;
        $client->adresse = $request->adresse;
        $client->contact = $request->contact;
        $client->save();

        return redirect()->route('clients')->with('success','Le client a bien été ajouté');
    }

    public function edit($id)
    {
        $client = Client::where('id',$id)->first();
        if($client){
            return view('clients.edit',compact('client'));
        }
        return redirect()->back();
    }

    public function update(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(), [
            'nom' => ['required'],
            'adresse' => ['required'],
            'contact' => ['required'],
            'id' => ['required'],
        ])->validate();

        $client = Client::where('id',$request->id)->first();

        if($client){
            $client->nom = $request->nom;
            $client->adresse = $request->adresse;
            $client->contact = $request->contact;
            $client->save();
        }

        return redirect()->route('clients')->with('success','Le client a bien été modifié');
    }


    public function destroy($id)
    {
        //dd($id);
        $cat = Client::findOrFail($id);
        $cat->delete();
        return redirect()->back()->with('success','Vous venez de supprimer un client');
    }
}
