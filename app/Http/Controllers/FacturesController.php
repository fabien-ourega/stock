<?php

namespace App\Http\Controllers;

use App\Bl;
use App\Blitem;
use App\Client;
use App\Mpaiement;
use App\Project;
use Illuminate\Support\Facades\Validator;
use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class FacturesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $bons = Bl::with('project')->with('modepaie')->where('etat',1)->where('etat_facture',0)->orderBy('id','desc')->get();
        $bons->map(function ($item){
            $item->total = $this->getMontantTotal($item->id,'all');
        });
        $modes = Mpaiement::get();
        $projects = Project::get();
        $clients = Client::get();
        //dd($bons);
        if (!in_array('add_fct', Session::get('permissions'))){
            return redirect()->back()->with('success',"Vous n'avez pas le droit d'accéder à cette page");
        }
        return view('factures.index',compact('bons','modes','projects','clients'));
    }

    public function getFphysik($id)
    {
        $id = decrypt($id);
        $bon = Bl::where('id',$id)->firstOrFail();
        $bon->facturephysique = 1;
        $bon->save();

        return redirect()->route('factures')->with('success','La facture physique a bien été traité.');
    }

    public function indexValid()
    {
        $bons = Bl::with('project')->with('modepaie')->where('etat',1)->where('etat_facture',1)->orderBy('id','desc')->get();
        $bons->map(function ($item){
            $item->total = $this->getMontantTotal($item->id,'all');
        });
        $modes = Mpaiement::get();
        $projects = Project::get();
        $clients = Client::get();
        //dd($bons);
        if (!in_array('add_fct', Session::get('permissions'))){
            return redirect()->back()->with('success',"Vous n'avez pas le droit d'accéder à cette page");
        }

        return view('factures.index_valid',compact('bons','modes','projects','clients'));
    }

    public function show($id)
    {
        $bl = Bl::where('id',$id)->with('project')->with('client')->first();
        $listbls = Blitem::where('bl_id',$id)->with('produits')->get();
        if (!in_array('add_fct', Session::get('permissions'))){
            return redirect()->back()->with('success',"Vous n'avez pas le droit d'accéder à cette page");
        }
        return view('factures.show',compact('bl','listbls'));

    }

    public function validfct(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(), [
            'tidsluge' => ['required'],
            'modepaie' => ['required'],
            //'file' => ['required'],
        ])->validate();

        $bl = Bl::where('id',$request->tidsluge)->where('etat_facture',0)->first();
        if($bl){
            if($request->modepaie==1){
                //cheque
                if(!$request->numchequeone){
                    return redirect()->back()->with('error','Veuillez inserez le numéro de chèque');
                }
                $bl->valeur_facture = $request->numchequeone;
            }elseif ($request->modepaie ==2){
                //virement
                if(!$request->datevirement){
                    return redirect()->back()->with('error','Veuillez inserez la date de virement');
                }
                $bl->valeur_facture = $request->datevirement;
            }else{
                //avance
                //$request->modepaie==2
                if($request->avance == 'espece'){

                    if(!$request->numboncaisse){
                        return redirect()->back()->with('error','Veuillez inserez le numéro de bon de caisse');
                    }
                    $bl->type_facture = 'espece';
                    $bl->valeur_facture = $request->numboncaisse;

                }elseif($request->avance == 'cheque'){

                    if(!$request->numcheque){
                        return redirect()->back()->with('error','Veuillez inserez le numéro du chèque');
                    }
                    $bl->type_facture = 'cheque';
                    $bl->valeur_facture = $request->numcheque;
                }else{
                    return redirect()->back()->with('error','Veuillez inserez tous les champs');
                }
            }

            $bl->etat_facture = 1;
            $bl->mode_facture = $request->modepaie;
            /*if($request->file('file')){
                $file=$request->file('file');
                $extension = $file->getClientOriginalExtension() ?: 'png';
                $folderName = 'bc/';
                $picture = Str::random(10).'.'. $extension;
                $file->move($folderName,$picture);
                $bl->file_bl = $picture;
            }*/

            //verification sur la remise
            if($request->remise != 0){
                //si remise est un nombre et inferieur au montant total
                //dd($request->remise);
                if(!is_numeric($request->remise) || $request->remise > $this->getMontantTotal($bl->id,'ht')){
                    return redirect()->back()->with('error','Veuillez inserez un montant de remise valide');
                }
                $bl->remise = $request->remise;
            }

            $bl->save();

            return redirect()->route('factures')->with('success','La facture a bien été traité.');

        }
        return redirect()->back()->with('error','Une erreur est survenue veuillez réessayer ultérieurement.');
    }

    public function download($id)
    {
        $bl = Bl::where('id',$id)->with('project')->with('client')->first();
        if($bl){
            $listbls = Blitem::where('bl_id',$id)->with('produits')->get();

            $pdf = PDF::loadView('factures.download',compact('bl','listbls'));
            //return view('factures.download',compact('bl','listbls'));
            return $pdf->download('factures_'.$bl->slug.'-'.date("d-m-y").'.pdf');

        }
        return redirect()->back();
    }

    public function getMontantTotal($idfacture , $return)
    {
        $listbls = Blitem::where('bl_id',$idfacture)->with('produits')->get();

        $sstotal = 0;
        foreach($listbls as $k=>$listbl){
            $total = $listbl->qte * $listbl->price ;
            //$total = $listbl->qte * $listbl->produits->price ;
            $sstotal = $sstotal + $total;
        }
        $sstotal = $sstotal - $listbls[0]->bl->remise;
        $mht = $sstotal;
        $tva = 0.18 * $sstotal;
        $mttc = $sstotal + $tva;

        $data['ht'] = $mht;
        $data['ttc'] = $mttc;

        if($return == "all"){
            return $data;
        }elseif ($return == "ht"){
            return $mht;
        }elseif ($return == "ttc"){
            return $mttc;
        }else{
            return $data;
        }

    }
}
