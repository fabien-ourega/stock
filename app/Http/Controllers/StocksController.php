<?php

namespace App\Http\Controllers;

use App\Bstock;
use App\Categorie;
use App\Pa;
use App\Produit;
use App\Project;
use App\StockHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class StocksController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getMontantTotal($idfacture,$type='project')
    {
        if($type == 'project'){
            //get list bls
            $listbls = Bstock::where('pjt_id',$idfacture)->with('produit')->get();
        }else{
            $listbls = Bstock::where('magasin_id',$idfacture)->with('produit')->get();
        }

        $sstotal = 0;
        foreach($listbls as $k=>$listbl){
            if($listbl->produit){
                $total = $listbl->qte * $listbl->produit->price ;
                $sstotal = $sstotal + $total;
            }

        }

        return $sstotal;

    }

    public function index()
    {
        $projects = Project::with('produits')->get();
        $categories = Categorie::orderBy('libelle')->get();
        //dd($projects);
        $produits = Produit::with('categorie')->with('type')->get();
        $produits->map(function ($produit){
            $nbst = Bstock::where('pdt_id',$produit->id)->whereNull('pjt_id')->first();
            $nb =$nbst ? $nbst->qte : '0';
            $produit->mtotal = $produit->price * $nb;
        });
        return view('gstocks.index',compact('projects','produits','categories'));
    }

    public function storePdt(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(), [
            'astock' => ['required'],
            'idslug' => ['required'],
        ])->validate();

        $request->astock =str_replace(' ', '', $request->astock);

        $pdt = Produit::where('id',$request->idslug)->first();
        if($pdt){
            $bstock = Bstock::where('pdt_id',$pdt->id)->whereNull('pjt_id')->first();
            if($bstock){
                $newqte = $bstock->qte + $request->astock ;
                $bstock->qte = $newqte;

            }else{
                $bstock = new Bstock();
                $bstock->qte = $request->astock;
                $bstock->pdt_id = $pdt->id;
            }
            $bstock->save();

            //save historie
            $this->saveStockHistorie('approvisionnement',$pdt->id,null,$request->astock,'interne');

            return redirect()->route('gstock')->with('success',"L'approvisionnement a bien été effectué.");
        }

        return redirect()->back()->with('error','Une erreur est survenue veuillez réessayer ultérieurement');
    }

    public function updatePdt(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(), [
            'astock' => ['required'],
            'idslug' => ['required'],
        ])->validate();

        $request->astock =str_replace(' ', '', $request->astock);

        if($request->astock > $request->dispo){
            return redirect()->back()->with('error','La quantité a réduire doit être inférieur à la quantité disponible');
        }

        $pdt = Produit::where('id',$request->idslug)->first();
        if($pdt){
            $bstock = Bstock::where('pdt_id',$pdt->id)->whereNull('pjt_id')->first();
            if($bstock){
                $newqte = $bstock->qte - $request->astock ;
                $bstock->qte = $newqte;
                $bstock->save();
            }

            //save historie
            $this->saveStockHistorie('reduction',$pdt->id,null,$request->astock,'interne');

            return redirect()->route('gstock')->with('success',"La réduction a bien été effectué.");
        }

        return redirect()->back()->with('error','Une erreur est survenue veuillez réessayer ultérieurement');

    }

    public function createPdt()
    {
        $produits = Produit::with('categorie')->groupBy('categorie_id')->get();
        return view('gstocks.create_pdt',compact('produits'));
    }

    public function getpdt($catid)
    {
        $produits = Produit::where('categorie_id',$catid)->with('categorie')->with('type')->get();
        return $produits;
    }

    public function stockPdt(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(), [
            'datasession' =>['required'],
        ])->validate();

        foreach ($request->datasession as $k=>$data){
            $item = explode(';',$data);
            //dd($item);
            //select Bstock
            $pdt = Produit::where('id',$item[0])->first();
            if($pdt){
                $bstock = Bstock::where('pdt_id',$pdt->id)->whereNull('pjt_id')->first();
                if($bstock){
                    $newqte = $bstock->qte + $item[2] ;
                    $bstock->qte = $newqte;
                }else{
                    $bstock = new Bstock();
                    $bstock->qte = $item[2];
                    $bstock->pdt_id = $pdt->id;
                }
                $bstock->save();

                //save prix d'achat
                $prix = new Pa();
                $prix->produit_id = $item[0];
                $prix->qte = $item[2];
                $prix->prix = $item[3];
                $prix->save();

                if($k==0){
                    //save history
                    $this->saveStockHistorie('entree',null,null,null,'interne');
                }
            }

        }
        return redirect()->route('gstock')->with('success',"L'approvisionnement a bien été effectué.");
    }


    public function project($id)
    {
        $project = Project::find($id);
        //dd($project);
        if($project){
            $stocks = Bstock::where('pjt_id',$project->id)->with('produit')->get();
            $otherprojects = Project::where('id','<>',$id)->get();
            return view('gstocks.project',compact('project','stocks','otherprojects'));
        }
        return redirect()->back()->with('error','Une erreur est survenue veuillez réessayer ultérieurement');
    }

    public function storePjt(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(), [
            'astock' => ['required'],
            'idslug' => ['required'],
            'slugpjt' => ['required'],
        ])->validate();

        $request->astock =str_replace(' ', '', $request->astock);

        $pdt = Produit::where('id',$request->idslug)->first();
        if($pdt){
            $bstock = Bstock::where('pdt_id',$pdt->id)->where('pjt_id',$request->slugpjt)->first();
            if($bstock){
                $newqte = $bstock->qte + $request->astock ;
                $bstock->qte = $newqte;

            }else{
                $bstock = new Bstock();
                $bstock->qte = $request->astock;
                $bstock->pdt_id = $pdt->id;
                $bstock->pjt_id = $request->slugpjt;
            }
            $bstock->save();

            //save historie
            $this->saveStockHistorie('approvisionnement',$request->idslug,$request->slugpjt,$request->astock,'project');

            return redirect()->route('gstock.project',$request->slugpjt)->with('success',"L'approvisionnement a bien été effectué.");
        }

        return redirect()->back()->with('error','Une erreur est survenue veuillez réessayer ultérieurement');
    }

    public function updatePjt(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(), [
            'astock' => ['required'],
            'idslug' => ['required'],
            'slugpjt' => ['required'],
        ])->validate();

        $request->astock =str_replace(' ', '', $request->astock);

        if($request->astock > $request->dispo){
            return redirect()->back()->with('error','La quantité a réduire doit être inférieur à la quantité disponible');
        }

        $pdt = Produit::where('id',$request->idslug)->first();
        if($pdt){
            $bstock = Bstock::where('pdt_id',$pdt->id)->where('pjt_id',$request->slugpjt)->first();
            if($bstock){
                $newqte = $bstock->qte - $request->astock ;
                $bstock->qte = $newqte;
                $bstock->save();
            }

            //save historie
            $this->saveStockHistorie('reduction',$request->idslug,$request->slugpjt,$request->astock,'project');

            return redirect()->route('gstock.project',$request->slugpjt)->with('success',"La réduction a bien été effectué.");
        }

        return redirect()->back()->with('error','Une erreur est survenue veuillez réessayer ultérieurement');
    }

    public function createPjt($id)
    {
        $project = Project::find($id);
        $produits = Bstock::where('pjt_id',$project->id)->with('produit')->get();
        $produitids = Bstock::where('pjt_id',$project->id)->pluck('pdt_id');
        //dd($produitids);
        return view('gstocks.create_pjt',compact('produits','project','produitids'));
    }

    public function getpjt($catid,$pjtid)
    {
        //dd($catid,$pjtid);
        $pdts = Produit::where('categorie_id',$catid)->pluck('id');
        $produits = Bstock::where('pjt_id',$pjtid)->whereIn('pdt_id',$pdts)->with('produit')->get();
        //dd($pdts,$produits);
        return $produits;


    }

    public function stockPjtOld(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(), [
            'projectid' =>['required'],
            'datasession' =>['required'],
        ])->validate();

        foreach ($request->datasession as $k=>$data){
            $item = explode(';',$data);
            //dd($k,$item);
            //select Bstock
            $pdt = Produit::where('id',$item[0])->first();
            if($pdt){
                $bstock = Bstock::where('pdt_id',$pdt->id)->where('pjt_id',$request->projectid)->first();
                if($bstock){
                    $newqte = $bstock->qte + $item[2] ;
                    $bstock->qte = $newqte;

                }else{
                    $bstock = new Bstock();
                    $bstock->qte = $item[2];
                    $bstock->pdt_id = $pdt->id;
                    $bstock->pjt_id = $request->projectid;
                }
                $bstock->save();

                //save prix d'achat
                $prix = new Pa();
                $prix->produit_id = $item[0];
                $prix->project_id = $request->projectid;
                $prix->qte = $item[2];
                $prix->prix = $item[3];
                $prix->save();

                //save history
                if($k==0){
                    $this->saveStockHistorie('entree',null,$request->projectid,null,'project');
                }

            }

        }
        return redirect()->route('gstock.project',$request->projectid)->with('success',"L'approvisionnement a bien été effectué.");
    }

    public function stockPjt(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(), [
            'projectid' =>['required'],
            'datasession' =>['required'],
            'magasin' =>['required'],
        ])->validate();

        foreach ($request->datasession as $k=>$data){
            $item = explode(';',$data);
            //dd($k,$item);
            //select Bstock
            $pdt = Produit::where('id',$item[0])->first();
            if($pdt){
                $bstock = Bstock::where('pdt_id',$pdt->id)->where('magasin_id',$request->magasin)->first();
                if($bstock){
                    $nbstock = Bstock::where('pdt_id',$pdt->id)->where('pjt_id',$request->projectid)->first();
                    if($nbstock){
                        $newqte = $nbstock->qte + $item[2] ;
                        $nbstock->qte = $newqte;

                    }else{
                        $nbstock = new Bstock();
                        $nbstock->qte = $item[2];
                        $nbstock->pdt_id = $pdt->id;
                        $nbstock->pjt_id = $request->projectid;
                    }
                    $nbstock->save();

                    //reduit le produit
                    $bstock->qte = $bstock->qte - $item[2];
                    $bstock->save();

                    //save history
                    // if($k==0){
                        $this->saveStockHistorie('entree',$pdt->id,$request->projectid,$item[2],'project',$request->magasin);
                    // }
                }

                //save prix d'achat
                // $prix = new Pa();
                // $prix->produit_id = $item[0];
                // $prix->project_id = $request->projectid;
                // $prix->qte = $item[2];
                // $prix->prix = $item[3];
                // $prix->save();

            }

        }
        return redirect()->route('gstock.project',$request->projectid)->with('success',"L'approvisionnement a bien été effectué.");
    }

    public function transPjt(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(), [
            'dispo' =>['required'],
            'astock' =>['required','min:1'],
            'pjt' =>['required'],
            'idslug' =>['required'],
            'slugpjt' =>['required'],
        ])->validate();

        //verifier si la quantite est suffisant
        $bstock = Bstock::where('pdt_id',$request->idslug)->where('pjt_id',$request->slugpjt)->first();
        if(!$bstock){
            return redirect()->back()->with('error','Une erreur est survenue veuillez réessayer ultérieurement');
        }
        $request->astock =str_replace(' ', '', $request->astock);

        if($bstock->qte >= $request->astock){

            //send on stock generale

            if($request->pjt == 0){
                $check = Bstock::where('pdt_id',$request->idslug)->whereNull('pjt_id')->first();
                if($check){
                    $newqte = $check->qte + $request->astock ;
                    $check->qte = $newqte;
                }else{
                    $check = new Bstock();
                    $check->qte = $request->astock ;
                    $check->pdt_id = $request->idslug;
                    //$check->pjt_id = $request->pjt;
                }
                $check->save();
            }else{

                //Send à un project

                $check = Bstock::where('pdt_id',$request->idslug)->where('pjt_id',$request->pjt)->first();
                if($check){
                    $newqte = $check->qte + $request->astock ;
                    $check->qte = $newqte;
                }else{
                    $check = new Bstock();
                    $check->qte = $request->astock ;
                    $check->pdt_id = $request->idslug;
                    $check->pjt_id = $request->pjt;
                }
                $check->save();
            }
            //reduit le produit
            $bstock->qte = $bstock->qte - $request->astock;
            $bstock->save();

            //save historie
            $this->saveStockHistorie('transfere',$request->idslug,$request->pjt,$request->astock,'project');

            return redirect()->route('gstock.project',$request->slugpjt)->with('success',"Le transfère a bien été effectué.");

        }
        return redirect()->back()->with('error','La quantité a transféré doit être inférieur à la quantité disponible');

        //dd($bstock);
    }

    public function transPdt(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(), [
            'dispo' =>['required'],
            'astock' =>['required','min:1'],
            'pjt' =>['required'],
            'idslug' =>['required'],
        ])->validate();

        $bstock = Bstock::where('pdt_id',$request->idslug)->whereNull('pjt_id')->first();
        if(!$bstock){
            return redirect()->back()->with('error','Une erreur est survenue veuillez réessayer ultérieurement');
        }
        $request->astock =str_replace(' ', '', $request->astock);

        if($bstock->qte >= $request->astock){
            $check = Bstock::where('pdt_id',$request->idslug)->where('pjt_id',$request->pjt)->first();
            if($check){
                $newqte = $check->qte + $request->astock ;
                $check->qte = $newqte;
            }else{
                $check = new Bstock();
                $check->qte = $request->astock ;
                $check->pdt_id = $request->idslug;
                $check->pjt_id = $request->pjt;
            }
            $check->save();

            //reduit le produit
            $bstock->qte = $bstock->qte - $request->astock;
            $bstock->save();

            //save historie
            $this->saveStockHistorie('transfere',$request->idslug,null,$request->astock,'interne');

            return redirect()->route('gstock')->with('success',"Le transfère a bien été effectué.");
        }
        return redirect()->back()->with('error','La quantité a transféré doit être inférieur à la quantité disponible');

    }

    public function storie()
    {
        //$stories = StockHistory::latest()->limit(100)->with('project')->with('produit')->get();
        $stories = StockHistory::with('project')->with('produit')->orderBy('id','desc')->get();
        //dd($stories);
        return view('gstocks.storie',compact('stories'));
    }

    public function itemStorie($id)
    {
        $story = StockHistory::where('id',$id)->with('project')->with('produit')->first();
        if($story){
            $dateD = $story->created_at->format('Y-m-d').' 00:00:00';
            $dateF = $story->created_at->format('Y-m-d').' 23:59:59';
            $lists = Pa::whereBetween('created_at',[$dateD, $dateF])->with('project')->with('produit')->get();
            //dd($lists);
            return view('gstocks.storie_item',compact('story','lists'));
        }

        return redirect()->back();
    }

}
