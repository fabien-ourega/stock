<?php

namespace App\Http\Controllers;

use App\Bl;
use App\Bstock;
use App\Client;
use App\Produit;
use App\Project;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $projects = Project::where('etat',0)->get();
        $clients = Client::get();
        $produits = Produit::get();
        $bons = Bl::get();
        $lastbons = Bl::with('project')->where('etat','0')->orderBy('id','desc')->take('12')->get();
        $produitfaible = Bstock::whereNull('pjt_id')->where('qte','<=',10)->with('produit')->get();
        //dd($produitfaible);
        $stats =[];
        $months=['01','02','03','04','05','06','07','08','09','10','11','12'];
        foreach($months as $month){
            //dump($month);
            $mtt = 0;
            $cops = Bl::whereYear('updated_at',date('Y'))->whereMonth('updated_at',$month)->where('etat',1)->get();
            foreach ($cops as $cop){
                $va = (new \App\Http\Controllers\FacturesController())->getMontantTotal($cop->id,'ttc');
                $mtt = $mtt +$va;
            }
            $stats[$month] = $this->del_espace($mtt);
        }
        //dd($stats);
        $facturno = Bl::where('etat',1)->where('etat_facture',0)->count();
        $facturtrair = Bl::where('etat',1)->where('etat_facture',1)->count();
        $statfactures['traite'] = $facturtrair;
        $statfactures['ntraite'] = $facturno;

        $bon = Bl::where('etat',0)->count();
        $bonn = Bl::where('etat',1)->count();
        $statbon['traite'] = $bonn;
        $statbon['ntraite'] = $bon;

        return view('home',compact('projects','clients','produits','bons','lastbons','produitfaible','stats','statfactures','statbon'));
    }
}
