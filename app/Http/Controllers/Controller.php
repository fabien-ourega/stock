<?php

namespace App\Http\Controllers;

use App\Historique;
use App\StockHistory;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function del_espace($var){
        $data = str_replace(' ', '', $var);
        return $data;
    }

    public function saveStockHistorie($libell,$idpdt=null,$idpjt=null,$qte=null,$type,$idmagasin=null)
    {
        $histo = new StockHistory();
        $histo->libelle = $libell;
        $histo->type = $type;
        $histo->project_id = $idpjt ?: null;
        $histo->magasin_id = $idmagasin ?: null;
        $histo->produit_id = $idpdt ?: null;
        $histo->qte = $qte ?: null;;
        $histo->save();
    }

    public function saveActionUser($action,$comment=null)
    {
        $histo = new Historique();
        $histo->user_id = Auth::user()->id;
        $histo->action = $action;
        $histo->comments = $comment ?: null;
        $histo->save();
    }
}
