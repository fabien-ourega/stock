<?php

namespace App\Http\Middleware;

use Closure;

class ActionsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $action = [];
        if(auth()->check()) {
            $authUser = auth()->user()->role_id;
            switch ($authUser) {
                case 1:
                    //admin
                    $action = ['show_stock','show_stock_all','add_bc','valid_bc_unik','appro_stock','appro_pjt','valid_bc','add_bl','edit_bl','show_fct_valid','add_fct','edit_fct','print_fct_valid','add_achat','add_pjt','add_pdt','show_pdt','edit_pdt_price','del_wainting',
                            'arch_bc','edit_fiche_bc','edit_fiche_bl','del_brll','del_bc','del_bl','del_fct','del_achat','del_pjt','del_pdt','add_cats','del_cats','del_clt','all_mgs'
                ];
                    break;

                case 2:
                    //Editeur
                    $action = ['show_stock','add_bc','valid_bc_unik'];
                    break;

                case 3:
                    //Superviseur
                    $action = ['show_stock','show_stock_all','appro_stock','valid_bc','add_bl','edit_bl','show_pdt','all_mgs','add_pjt'];
                    break;

                case 4:
                    //Support
                    $action = ['show_stock','show_stock_all','appro_stock','appro_pjt','valid_bc','add_bl','edit_bl','add_fct','print_fct','add_achat','add_four','del_wainting'];
                    break;

                case 5:
                    //Comptable
                    $action = ['show_stock','show_stock_all','appro_stock','show_fct_valid','add_fct','edit_fct','print_fct_valid','add_achat','add_four','add_pjt','show_pdt','add_pdt','edit_pdt_price'];
                    break;

                default:
                    $action = [];
                    break;

            }

            session(['permissions' => $action]);
        }

        return $next($request);
    }
}
