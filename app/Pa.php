<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pa extends Model
{
    protected $table="prix_achats";

    public function project()
    {
        return $this->belongsTo('App\Project','project_id');
    }

    public function produit()
    {
        return $this->belongsTo('App\Produit','produit_id');
    }
}
