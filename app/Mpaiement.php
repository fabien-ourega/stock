<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mpaiement extends Model
{
    use SoftDeletes;
    protected $table="mode_paiements";
}
