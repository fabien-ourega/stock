<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class ImportProduitCodif implements ToCollection
{
    /**
    * @param Collection $collection
    */
    public $message;

    public function collection(Collection $rows)
    {
        return $rows;
    }
}
