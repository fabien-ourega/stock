<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bstock extends Model
{
    use SoftDeletes;
    protected $table="big_stock";

    public function project()
    {
        return $this->belongsTo('App\Project','pjt_id');
    }

    public function produit()
    {
        return $this->belongsTo('App\Produit','pdt_id');
    }

}
