<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AchatItem extends Model
{
    use SoftDeletes;
    protected $table = "achat_item";
}
