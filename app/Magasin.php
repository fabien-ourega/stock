<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Magasin extends Model
{
    use SoftDeletes;


    public function bstocks()
    {
        return $this->hasMany('App\Bstock');
    }

}
