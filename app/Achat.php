<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Achat extends Model
{
    use SoftDeletes;

    public function fournisseur()
    {
        return $this->belongsTo('App\Fournisseur','fournisseur_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function modepaie()
    {
        return $this->belongsTo('App\Mpaiement','mode_id');
    }

    public function items()
    {
        return $this->hasMany('App\AchatItem','achat_id');
    }

    public function magasin() {
        return $this->belongsTo('App\Magasin','magasin_id');
    }

}
