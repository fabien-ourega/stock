<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bl extends Model
{
    use SoftDeletes;
    protected $table="bon_livraisons";

    public function project()
    {
        return $this->belongsTo('App\Project','pjt_id');
    }

    public function client()
    {
        return $this->belongsTo('App\Client','client_id');
    }

    public function modepaie()
    {
        return $this->belongsTo('App\Mpaiement','mode_facture');
    }
}
