<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Blitem extends Model
{
    use SoftDeletes;
    protected $table="bon_livraisons_items";

    public function produits()
    {
        return $this->belongsTo('App\Produit','pdt_id');
    }

    public function bl()
    {
        return $this->belongsTo('App\Bl','bl_id');
    }
}
