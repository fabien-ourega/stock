<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class GstockExport implements FromView
{
    public $data;
    public $idcats;
    public $nbpdt;

    public function __construct($data,$idcats,$nbpdt)
    {
        $this->data = $data;
        $this->idcats = $idcats;
        $this->nbpdt = $nbpdt;
    }

    public function view() : View
    {
        $customerArray = $this->data;
        $selectcatId = $this->idcats;
        $selectnbpdt = $this->nbpdt;

        return view('gstocks.download_etat_excel', [
            'customerArray' => $customerArray,
            'selectcatId' => $selectcatId,
            'selectnbpdt' => $selectnbpdt,
        ]);
    }
}
