<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ProjetExport implements FromView
{
    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function view() : View
    {
        $customerArray = $this->data;
        return view('projects.download_etat_excel', [
            'datas' => $customerArray
        ]);
    }
}
