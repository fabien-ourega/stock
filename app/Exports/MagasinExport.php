<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class MagasinExport implements FromView
{
    public $data;
    public $nbpdt;

    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct($data,$nbpdt)
    {
        $this->data = $data;
        $this->nbpdt = $nbpdt;
    }

    public function view() : View
    {
        $customerArray = $this->data;
        $selectnbpdt = $this->nbpdt;

        return view('magasins.download_etat_excel', [
            'customerArray' => $customerArray,
            'selectnbpdt' => $selectnbpdt,
        ]);
    }
}
