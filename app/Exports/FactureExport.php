<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class FactureExport implements FromView
{
    public $data;
    public $dated;
    public $datef;

    public function __construct($data,$date1,$date2)
    {
        $this->data = $data;
        $this->dated = $date1;
        $this->datef = $date2;
    }

    public function view() : View
    {
        $datas = $this->data;
        $date = $this->dated;
        $date2 = $this->datef;

        return view('factures.download_etat_excel', compact('datas','date','date2'));
    }
}
