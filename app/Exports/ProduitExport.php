<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ProduitExport implements FromView
{
    public $data;
    public $dated;
    public $datef;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function view() : View
    {
        $datas = $this->data;
        return view('produits.download_etat_excel', compact('datas'));
    }
}
