<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActuCategorie extends Model
{
    protected $table = 'actualites_categories';
    public $timestamps = false;
}
