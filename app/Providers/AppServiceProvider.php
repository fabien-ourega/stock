<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //Formatter les prix
        Blade::directive('price', function ($expression) {
            return "<?php echo number_format(str_replace(' ', '', $expression), 0, '', ' '); ?>";
        });

        $memois = [
            '01' => ['Janvier', 'Jan'],
            '02' => ['Février', 'Fév'],
            '03' => ['Mars', 'Mar'],
            '04' => ['Avril', 'Avr'],
            '05' => ['Mai', 'Mai'],
            '06' => ['Juin', 'Juin'],
            '07' => ['Juillet', 'Juil'],
            '08' => ['Août', 'Août'],
            '09' => ['Septembre', 'Sept'],
            '10' => ['Octobre', 'Oct'],
            '11' => ['Novembre', 'Nov'],
            '12' => ['Décembre', 'Déc']
        ];

        View::share('memois', $memois);
    }
}
