<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Scategorie extends Model
{
    use SoftDeletes;
    protected $table = "sub_categories";

    public function categorie(){
        return $this->belongsTo('App\Categorie','categorie_id');
    }
}
