<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('web/index');
//});


Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//ACHAT MODULE
Route::get('/gachats', 'AchatController@index')->name('achats');
Route::get('/gachats/rejeter', 'AchatController@rejeter')->name('achats.rejeter');
Route::get('/gachats/create', 'AchatController@create')->name('achats.create');
Route::get('/gachats/{slug}/show', 'AchatController@show')->name('achats.show');

Route::post('gachats/store', 'AchatController@store')->name('achats.store');
Route::post('gachats/upload/excel', 'AchatController@excel')->name('achats.excel');

Route::get('gachats/{id}/valid', 'AchatController@valid')->name('achat.valider');
Route::post('gachats/rejeter', 'AchatController@reject')->name('achat.reject');


//FOURNISSEURS
Route::get('fournisseur', 'FournisseurController@index')->name('frsseur');
Route::get('fournisseur/create', 'FournisseurController@create')->name('frsseur.create');
Route::get('fournisseur/{id}/edit', 'FournisseurController@edit')->name('frsseur.edit');
Route::get('fournisseur/delete/{id}', 'FournisseurController@delet')->name('frsseur.delete');
Route::get('fournisseur/deletes', 'FournisseurController@allDelet')->name('frsseur.deletes');

Route::post('fournisseur/store', 'FournisseurController@store')->name('frsseur.store');
Route::post('fournisseur/update', 'FournisseurController@update')->name('frsseur.update');


//NEWS
Route::get('actualites', 'NewsController@index')->name('news');
Route::get('actualites/create', 'NewsController@create')->name('news.create');
Route::get('actualites/{id}/edit', 'NewsController@edit')->name('news.edit')->where('id', '[0-9]+');
Route::get('actualites/{id}/retablir', 'NewsController@retablir')->name('news.retablir')->where('id', '[0-9]+');
Route::get('actualites/type/{slug}', 'NewsController@showtype')->name('news.show_type')->where('slug', '[A-Za-z]+');
Route::get('actualites/delete/{id}', 'NewsController@delet')->name('news.delete')->where('id', '[0-9]+');
Route::get('actualites/deletes', 'NewsController@allDelet')->name('news.deletes');
Route::get('actualites/brouillons', 'NewsController@allBrouillons')->name('news.brouillons');
Route::get('actualites/publies', 'NewsController@allPublies')->name('news.publies');
Route::get('actualites/retablirs', 'NewsController@allRetablirs')->name('news.retablirs');
Route::get('actualites/{id}/apercu', 'NewsController@apercu')->name('news.apercu')->where('id', '[0-9]+');
Route::get('actualites/{slug}', 'NewsController@show')->name('news.show');

Route::post('actualites/store', 'NewsController@store')->name('news.store');
Route::post('actualites/update', 'NewsController@update')->name('news.update');
Route::post('actualites/preview/store', 'NewsController@previewStore')->name('news.preview_store');

//CATEGORIE
Route::get('categories', 'CategoriesController@index')->name('categories');
Route::get('categories/{id}/edit', 'CategoriesController@edit')->name('categories.edit')->where('id', '[0-9]+');
Route::get('categories/delete/{id}', 'CategoriesController@delet')->name('categories.delete')->where('id', '[0-9]+');
Route::get('categories/deletes', 'CategoriesController@allDelet')->name('categories.deletes');
Route::get('categories/{id}/show', 'CategoriesController@show')->name('categories.show')->where('id', '[0-9]+');
Route::get('scategories/{id}/edit', 'CategoriesController@sedit')->name('scategories.edit')->where('id', '[0-9]+');
Route::get('scategories/delete/{id}', 'CategoriesController@sdelet')->name('scategories.delete')->where('id', '[0-9]+');

Route::get('get/{id}/scategorie', 'CategoriesController@getscat');

Route::post('categories/store', 'CategoriesController@store')->name('categories.store');
Route::post('categories/update', 'CategoriesController@update')->name('categories.update');
Route::post('scategories/store', 'CategoriesController@sstore')->name('scategories.store');
Route::post('scategories/update', 'CategoriesController@supdate')->name('scategories.update');

//USERS
Route::get('utilisateurs', 'UsersController@index')->name('users');
Route::get('utilisateurs/create', 'UsersController@create')->name('users.create');
Route::get('utilisateurs/{id}/edit', 'UsersController@edit')->name('users.edit')->where('id', '[0-9]+');
Route::get('utilisateurs/delete/{id}', 'UsersController@delet')->name('users.delete')->where('id', '[0-9]+');
Route::get('utilisateurs/deletes', 'UsersController@allDelet')->name('users.deletes');

Route::post('utilisateurs/store', 'UsersController@store')->name('users.store');
Route::post('utilisateurs/update', 'UsersController@update')->name('users.update');

//ROLE
Route::get('roles', 'RolesController@index')->name('roles');
Route::get('roles/{id}/edit', 'RolesController@edit')->name('roles.edit')->where('id', '[0-9]+');
Route::get('roles/delete/{id}', 'RolesController@delet')->name('roles.delete')->where('id', '[0-9]+');
Route::get('roles/deletes', 'RolesController@allDelet')->name('roles.deletes');

Route::post('roles/store', 'RolesController@store')->name('roles.store');
Route::post('roles/update', 'RolesController@update')->name('roles.update');

//PROFIL
Route::get('profile', 'ProfilesController@index')->name('profiles');
Route::post('profile/save', 'ProfilesController@profilupdate')->name('profiles.save');
Route::post('profile/pass', 'ProfilesController@profileditpass')->name('profiles.savepass');
Route::post('profile/avatar', 'ProfilesController@profileditavatar')->name('profiles.saveavatar');

//MEDIATHEQUE

    //ALBUM PHOTO
    Route::get('albums', 'MediasController@album')->name('albums');
    Route::get('albums/delete/{id}', 'MediasController@deletAlbum')->name('albums.delete')->where('id', '[0-9]+');
    Route::get('albums/{id}/edit', 'MediasController@editAlbum')->name('albums.edit')->where('id', '[0-9]+');
    Route::get('albums/deletes', 'MediasController@allDeletAlbum')->name('albums.deletes');

    Route::post('albums/store', 'MediasController@storeAlbum')->name('albums.store');
    Route::post('albums/update', 'MediasController@updateAlbum')->name('albums.update');

    //PHOTO
    Route::get('photos/{id}', 'MediasController@photo')->name('photos')->where('id', '[0-9]+');
    Route::get('photos/{id}/{type}', 'MediasController@updatePhoto')->name('photos.update')->where(['id'=>'[0-9]+','type' => '[a-z]+']);
    Route::get('photos/delete/{id}', 'MediasController@deletPhoto')->name('photos.delete')->where('id', '[0-9]+');
    Route::get('photos/deletes', 'MediasController@allDeletPhoto')->name('photos.deletes');
    Route::get('photos/active', 'MediasController@allActPhoto')->name('photos.actives');
    Route::get('photos/inactive', 'MediasController@allInactPhoto')->name('photos.inactives');

    Route::post('photos/store', 'MediasController@storePhoto')->name('photos.store');

    //VIDEO
    Route::get('videos', 'MediasController@videos')->name('videos');
    Route::get('videos/delete/{id}', 'MediasController@deletVideo')->name('videos.delete')->where('id', '[0-9]+');
    Route::get('videos/deletes', 'MediasController@allDeletVideo')->name('videos.deletes');

    Route::post('videos/store', 'MediasController@storeVideo')->name('videos.store');


//PRODUITS
Route::get('produits', 'ProduitsController@index')->name('produits');
Route::get('produits/creer', 'ProduitsController@create')->name('produits.create');
Route::get('produits/{slug}/modifier', 'ProduitsController@edit')->name('produits.edit')->where('id', '[0-9]+');
Route::get('produits/{slug}/delete', 'ProduitsController@destroy')->name('produits.destroy')->where('id', '[0-9]+');
Route::post('produits/store', 'ProduitsController@store')->name('produits.store');
Route::post('produits/update', 'ProduitsController@update')->name('produits.update');
Route::get('produits/types', 'ProduitsController@types')->name('produits.types');
Route::post('produits/types/store', 'ProduitsController@storeType')->name('produits.types.store');
Route::post('produits/types/update', 'ProduitsController@updateType')->name('produits.types.update');
Route::post('produits/upload/excel', 'ProduitsController@excel')->name('produits.excel');

//PROJECTS
Route::get('projects', 'ProjectsController@index')->name('projects');
Route::get('projects/clotures', 'ProjectsController@cloturer')->name('projects.cloturer');
Route::get('projects/creer', 'ProjectsController@create')->name('projects.create');
Route::post('projects/creer', 'ProjectsController@store')->name('projects.store');
Route::get('projects/{id}/show', 'ProjectsController@show')->name('projects.show');
Route::get('projects/{id}/edit', 'ProjectsController@index')->name('projects.edit')->where('id', '[0-9]+');
Route::get('projects/{id}/delete', 'ProjectsController@destroy')->name('projects.delete')->where('id', '[0-9]+');
Route::get('projects/{id}/cloturer', 'ProjectsController@clore')->name('projects.clore')->where('id', '[0-9]+');
Route::post('projects/update', 'ProjectsController@update')->name('projects.update');

Route::post('projects/item/store', 'ProjectsController@storeProject')->name('projects.storeProject');
Route::post('projects/item/upade', 'ProjectsController@updateProject')->name('projects.updateProject');
Route::get('projects/item/{id}/{pjt}/delete', 'ProjectsController@destroyProject')->name('projects.deleteProject');

//STOCK
Route::get('gstock', 'StocksController@index')->name('gstock');
Route::post('gstock/stock/produit', 'StocksController@storePdt')->name('gstock.store.pdt');
Route::post('gstock/update/produit', 'StocksController@updatePdt')->name('gstock.update.pdt');
Route::get('gstock/entree/produit', 'StocksController@createPdt')->name('gstock.entree.pdt');
Route::post('gstock/entree/produit/store', 'StocksController@stockPdt')->name('gstock.entree.pdt.store');
Route::post('gstock/transfere/produits', 'StocksController@transPdt')->name('gstock.trans.pdt');

Route::get('get/{id}/{pjt}/produits/cat', 'StocksController@getpjt');
Route::get('get/{id}/produits/cat', 'StocksController@getpdt');


Route::get('gstock/{id}/projects', 'StocksController@project')->name('gstock.project')->where('id', '[0-9]+');
Route::post('gstock/stock/projects', 'StocksController@storePjt')->name('gstock.store.project');
Route::post('gstock/update/projects', 'StocksController@updatePjt')->name('gstock.update.project');
Route::get('gstock/entree/{id}/project', 'StocksController@createPjt')->name('gstock.entree.pjt')->where('id', '[0-9]+');
Route::post('gstock/entree/project/store', 'StocksController@stockPjt')->name('gstock.entree.pjt.store');
Route::post('gstock/transfere/projects', 'StocksController@transPjt')->name('gstock.trans.project');
Route::get('gstock/storie', 'StocksController@storie')->name('gstock.storie');
Route::get('gstock/{item}/storie', 'StocksController@itemStorie')->name('gstock.storie.item')->where('id', '[0-9]+');


//BON DE LIVRAISON
Route::get('bonlivraison', 'BlivraisonsController@index')->name('bl');
Route::get('bonlivraison/en_attent_validation', 'BlivraisonsController@indexwaiting')->name('bl.waiting');
Route::get('bonlivraison/rejeter', 'BlivraisonsController@indexrejeter')->name('bl.rejeter');
Route::get('bonlivraison/{slug}/valid', 'BlivraisonsController@validerbl')->name('bl.valider');
Route::post('bonlivraison/rejeter', 'BlivraisonsController@rejetbl')->name('bl.rejetbl');
Route::get('bonlivraison/traites', 'BlivraisonsController@indexvalid')->name('bl.valid');
Route::get('bonlivraison/archives', 'BlivraisonsController@indexarchive')->name('bl.indexarchive');
Route::get('bonlivraison/archives/{slug}', 'BlivraisonsController@indexarchiveitem')->name('bl.indexarchiveitem');
Route::get('bonlivraison/brouillons', 'BlivraisonsController@indexbrouillon')->name('bl.brouillon');
Route::get('bonlivraison/create', 'BlivraisonsController@create')->name('bl.create');
Route::get('bonlivraison/{id}/show', 'BlivraisonsController@show')->name('bl.show')->where('id', '[0-9]+');
Route::get('bonlivraison/{id}/delete/bc', 'BlivraisonsController@destroy')->name('bl.destroy')->where('id', '[0-9]+');
Route::get('bonlivraison/deletes', 'BlivraisonsController@allDelet')->name('bl.deletes');
Route::get('bonlivraison/{id}/archiver/bc', 'BlivraisonsController@archive')->name('bl.archive')->where('id', '[0-9]+');
Route::get('bonlivraison/archiver', 'BlivraisonsController@allArchiv')->name('bl.archive.deletes');

Route::get('bonlivraison/{id}/show/brouillon', 'BlivraisonsController@showBrouillons')->name('bl.show.brouillon')->where('id', '[0-9]+');
Route::get('bonlivraison/{id}/editer/brouillon', 'BlivraisonsController@editBrouillons')->name('bl.edit.brouillon')->where('id', '[0-9]+');
Route::get('bonlivraison/{id}/delete/brouillon', 'BlivraisonsController@destroyBrouillons')->name('bl.destroy.brouillon')->where('id', '[0-9]+');
Route::get('bonlivraison/{id}/download', 'BlivraisonsController@download')->name('bl.download')->where('id', '[0-9]+');
Route::get('boncommande/{id}/download', 'BlivraisonsController@downloadBc')->name('bl.bc.download')->where('id', '[0-9]+');
Route::get('get/{id}/produits', 'BlivraisonsController@getpjt');
Route::get('get/{qte}/{id}/{pjtid}/pdt/dispo', 'BlivraisonsController@getdispopjt');
Route::get('get/{qte}/{id}/{pjtid}/magasin/dispo', 'BlivraisonsController@getdispomagasin');
Route::post('bonlivraison/store', 'BlivraisonsController@store')->name('bl.store');
Route::post('bonlivraison/store/update', 'BlivraisonsController@storeUpdate')->name('bl.store.update');
Route::post('bonlivraison/update/status', 'BlivraisonsController@status')->name('bl.status');

Route::get('bonlivraison/{id}/details', 'BlivraisonsController@blitem')->name('bl.item.bl')->where('id', '[0-9]+');
Route::post('bonlivraison/update/vlid', 'BlivraisonsController@validbl')->name('bl.validbl');
Route::post('bonlivraison/update/editvlid', 'BlivraisonsController@editbl')->name('bl.editbl');
Route::post('bonlivraison/update/bcref', 'BlivraisonsController@editblref')->name('bl.editblref');

//FACTURES
Route::get('factures', 'FacturesController@index')->name('factures');
Route::get('factures/valid', 'FacturesController@indexValid')->name('factures.valid');
Route::get('factures/{id}/detail', 'FacturesController@show')->name('factures.show');
Route::get('factures/{id}/factphys', 'FacturesController@getFphysik')->name('factures.getFphysik');
Route::post('factures/update/vlid', 'FacturesController@validfct')->name('factures.validbl');
Route::get('factures/{id}/download', 'FacturesController@download')->name('factures.download')->where('id', '[0-9]+');

//GESTION DE CLIENTS
Route::get('clients', 'ClientsController@index')->name('clients');
Route::get('clients/create', 'ClientsController@create')->name('clients.create');
Route::post('clients/store', 'ClientsController@store')->name('clients.store');
Route::get('clients/{id}/edit', 'ClientsController@edit')->name('clients.edit')->where('id', '[0-9]+');
Route::post('clients/update', 'ClientsController@update')->name('clients.update');
Route::get('clients/{id}/delete', 'ClientsController@destroy')->name('clients.destroy')->where('id', '[0-9]+');

//MODE DE PAIEMENT
Route::get('modepaiement', 'ModePaiementController@index')->name('modepaie');
Route::post('modepaiement/store', 'ModePaiementController@store')->name('modepaie.store');
Route::get('modepaiement/{id}/edit', 'ModePaiementController@edit')->name('modepaie.edit')->where('id', '[0-9]+');
Route::post('modepaiement/update', 'ModePaiementController@update')->name('modepaie.update');
Route::get('modepaiement/{id}/delete', 'ModePaiementController@destroy')->name('modepaie.destroy')->where('id', '[0-9]+');

//MAGASIN
Route::get('magasins', 'MagasinsController@index')->name('magasins');
Route::get('magasins/create', 'MagasinsController@create')->name('magasins.create');
Route::post('magasins/store', 'MagasinsController@store')->name('magasins.store');
Route::get('magasins/{id}/edit', 'MagasinsController@edit')->name('magasins.edit')->where('id', '[0-9]+');
Route::get('magasins/{id}/gerer', 'MagasinsController@show')->name('magasins.show')->where('id', '[0-9]+');
Route::post('magasins/update', 'MagasinsController@update')->name('magasins.update');
Route::get('magasins/{id}/delete', 'MagasinsController@destroy')->name('magasins.destroy')->where('id', '[0-9]+');

Route::post('magasins/stock/appro', 'MagasinsController@appro')->name('magasins.store.appro');
Route::post('magasins/stock/reduir', 'MagasinsController@reduir')->name('magasins.store.reduir');
Route::post('magasins/stock/transfer', 'MagasinsController@transfer')->name('magasins.store.transfer');

//ETATS
Route::post('etats/gstock', 'EtatsController@gstock')->name('etats.gstock');
Route::post('etats/projets', 'EtatsController@projets')->name('etats.projets');
Route::post('etats/bc', 'EtatsController@bc')->name('etats.bc');
Route::post('etats/produits', 'EtatsController@pdt')->name('etats.pdt');
Route::post('etats/factures', 'EtatsController@factures')->name('etats.factures');
Route::post('etats/magasin', 'EtatsController@magasin')->name('etats.magasin');
