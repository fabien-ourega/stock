@extends('layouts.app')

@section('title')
    Connectez vous
    @parent
@stop

@section('header_styles')
    <style>
        .wrapper-page{
            width: 315px;
        }

    </style>
@endsection

@section('footer_scripts')
@endsection


@section('content')
<div class="account-pages"></div>

<div class="clearfix"></div>

<div class="wrapper-page">
    <div class=" card-box">

        <div class="text-center">
            <div class="text-slate-300">
                <img src="{{asset('assets/images/logo_light.png')}}" alt="" width="150px">
            </div>
            <h5 class="content-group">Connectez-vous à votre compte <br>
                <small class="display-block">
                    Entrez vos informations d’identification ci-dessous
                </small>
            </h5>
        </div>



        <div class="panel-body">
            <form class="form-horizontal m-t-20" method="POST" action="{{ route('login') }}">
                @csrf
                <div class="form-group ">
                    <div class="col-xs-12">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email" autofocus>
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-12">
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required placeholder="Mot de passe" autocomplete="current-password">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group text-center m-t-40">
                    <div class="col-xs-12">
                        <button class="btn btn-info btn-block text-uppercase waves-effect waves-light" type="submit" style="background-color: #0483b8 !important;border: 1px solid #0483b8 !important;">Connexion</button>
                    </div>
                </div>

                {{--<div class="form-group m-t-30 m-b-0">--}}
                    {{--<div class="col-sm-12">--}}
                        {{--<a href="page-recoverpw.html" class="text-dark"><i class="fa fa-lock m-r-5"></i> Forgot your password?</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </form>

        </div>
    </div>

</div>

@endsection
