@extends('layouts.app')

@section('title')
    Projet - {{$project->libelle}}
    @parent
@stop

@section('header_styles')
    <!-- DataTables -->
    <link href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css"/>
    <style>
        .avatar-lg {
            height: 4.5rem;
            width: 4.5rem;
        }
        .rounded-circle {
            border-radius: 50%!important;
        }
        .bg-primary {
            background-color: #6658dd!important;
        }
        .avatar-title {
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            color: #fff;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            height: 100%;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            width: 100%;
        }
        .font-22 {
            font-size: 22px!important;
        }
        .mb-3, .my-3 {
            margin-bottom: 1.5rem!important;
        }

        .dropdown-item {
            float: left;
            padding: 5px;
            margin-right:5px;
        }
    </style>
@endsection

@section('footer_scripts')
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.js') }}"></script>

    <script src="{{ asset('assets/pages/datatables.init.js') }}"></script>

    <!-- Sweet-Alert  -->
    <script src="{{ asset('assets/pages/jquery.sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-select/js/bootstrap-select.min.js') }}"></script>
    {{--<script src="{{ asset('assets/pages/jquery.sweet-alert.init.js') }}"></script>--}}

    <script type="text/javascript">
        $(document).ready(function () {
            $('.select2').select2();

            $('#myTable').dataTable({
                language: {
                    lengthMenu: "_MENU_",
                    search: "_INPUT_",
                    searchPlaceholder: "Recherche",
                    sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                    sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    sInfoPostFix:    "",
                    sLoadingRecords: "Chargement en cours...",
                    sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
                    sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
                    sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    oPaginate: {
                        "sFirst":    "Premier",
                        "sLast":    "Dernier",
                        "sNext":    "Suivant",
                        "sPrevious": "Précédent"
                    },
                }
            });
        });

        $('.actus').on('click', '.delete', function (event) {
            event.preventDefault();
            var href = $(this).attr('href');
            swal({
                title: "Êtes-vous sûr?",
                text: "Voulez vous vraiment supprimer cet produit",
                icon: "warning",
                buttons: true,
                buttons: ["Annuler", "Oui"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    window.location = href;
                }
            });
        });

        $('#check_all').on('click', function(e){
            if($(this).is(':checked',true)){
                $(".role_item").prop('checked', true);
            }else{
                $(".role_item").prop('checked', false);
            }
        });

        $('#sendActionGroup').on('click', function(e) {

            if($('#actionGroup').val()=='del_select'){
                var allVals = [];
                $(".role_item:checked").each(function() {
                    allVals.push($(this).attr('data-id'));
                });
                //console.log(allVals.length);
                // return false;
                if(allVals.length <=0)
                {
                    swal("Oops","Veuillez selectionnez des lignes à supprimer!","error" )
                }
                else {
                    //$("#loading").show();
                    swal({
                        title: "Êtes-vous sûr?",
                        text: "Voulez vous vraiment supprimer ces lignes",
                        icon: "warning",
                        buttons: true,
                        buttons: ["Annuler", "Oui"],
                        dangerMode: true,
                    }).then((willDelete) => {
                        if(willDelete) {
                            var valueIds = allVals.join(",");

                            $.ajax({
                                url:"{{ route('users.deletes') }}",
                                method:"GET",
                                data:{value:valueIds},
                                success:function(res){
                                    console.log(res);
                                    if(res == '1'){
                                        //$("#loading").hide();
                                        location.reload();
                                    }else{
                                        //$("#loading").hide();
                                        location.reload();
                                    }
                                }
                            })
                        }
                    });
                }
            }else{
                return false;
                //swal("Oops","Something went wrong!","error" )
            }

        });


        function showform(){
            $('#formstore').show();
            $('#showform').hide();
            $('#hideform').show();
        }
        function hideform(){
            $('#formstore').hide();
            $('#hideform').hide();
            $('#showform').show();
        }

        function editstock(libel,qte,id){
            $("#formEdit")[0].reset();
            $("#eqte").val(qte);
            $("#elibelle").val(libel);
            $("#idslug").val(id);
        }

    </script>
@endsection


@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="page-title">Projet - {{$project->libelle}}
                            @if($project->etat==0)<button onclick="showform()" id="showform" class="btn btn-default btn-xs"><i class="fa fa-plus-circle"></i> Assigner des produits</button>@endif
                            <button onclick="hideform()" id="hideform" class="btn btn-default btn-xs" style="display: none;"><i class="fa fa-plus-circle"></i> Fermer </button>
                        </h4>
                        <ol class="breadcrumb"> </ol>
                    </div>
                </div>

                <div id="edit-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="edit-modalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="standard-modalLabel">Modifier une assignation
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                </h4>
                            </div>
                            <form id="formEdit" action="{{route('projects.updateProject')}}" class="form-horizontal" method="post" autocomplete="off">
                                @csrf
                                <div class="modal-body">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label class="col-md-12" for="">Produit <span class="text-danger">*</span></label>
                                            <input readonly type="text" id="elibelle" class="form-control col-md-12">
                                        </div>

                                        <div class="col-sm-12">
                                            <label class="col-md-12" for="description">Quantité attendue <span class="text-danger">*</span></label>
                                            <input type="number" min="1" id="eqte" name="qte" class="form-control col-md-12" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <input type="hidden" name="idslug" id="idslug">
                                    <input type="hidden" name="idpjt" value="{{$project->id}}">
                                    <button type="button" class="btn btn-light" data-dismiss="modal">Annuler</button>
                                    <button type="submit" class="btn btn-primary">Modifier</button>
                                </div>
                            </form>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div>


                <div class="modal-dialog" style="display: none;" id="formstore">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="standard-modalLabel">Choisir le produit</h4>
                        </div>
                        <form action="{{route('projects.storeProject')}}" class="form-horizontal" method="post" autocomplete="off">
                            @csrf
                            <div class="modal-body">
                                <div class="form-group">
                                    <div class="col-sm-12 mb-3">
                                        <label class="col-md-12" for="pdt">Produit <span class="text-danger">*</span></label>
                                        <select name="pdt" id="pdt" class="form-control select2">
                                            <option value="" selected>Choisir le produit</option>
                                            @foreach($produits as $produit)
                                                <option value="{{$produit->id}}">{{$produit->nom}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-sm-12">
                                        <label class="col-md-12" for="qte">Quantité attendue <span class="text-danger">*</span></label>
                                        <input type="number" name="qte" class="form-control col-md-12" min="1" required>
                                    </div>

                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="hidden" name="idpjt" value="{{$project->id}}">
                                <button type="button" class="btn btn-light" data-dismiss="modal">Annuler</button>
                                <button type="submit" class="btn btn-primary">Ajouter</button>
                            </div>
                        </form>
                    </div>
                </div>


                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-4 col-xl-4">
                            <div class="widget-rounded-circle card-box">
                                <div class="row">
                                    <div class="">
                                        <h4 class="m-t-0 m-b-5 header-title"><b>{{$project->libelle}}</b></h4>
                                        <p class="text-muted">Projet</p>
                                        <p class="text-dark"><i class="fa fa-clock-o m-r-10"></i><small>{{$project->created_at->format('d-m-Y')}}</small></p>
                                        <p class="text-dark"><i class="fa fa-info m-r-10"></i><small>{{$project->description}}</small></p>
                                    </div>
                                </div> <!-- end row-->
                            </div> <!-- end widget-rounded-circle-->
                        </div>
                        <div class="col-md-4 col-xl-4">
                            <div class="widget-rounded-circle card-box">
                                <div class="row">
                                    <div class="col-6 pull-left">
                                        <div class="avatar-lg rounded-circle bg-primary">
                                            <i class="fa fa-tag font-22 avatar-title text-white"></i>
                                        </div>
                                    </div>
                                    <div class="col-6" style="padding-right: 10px">
                                        <div class="text-right">
                                            <h3 class="text-dark mt-1"><span data-plugin="counterup">{{count($stocks)}}</span></h3>
                                            <p class="text-muted mb-1 text-truncate">Total Produit</p>
                                        </div>
                                    </div>
                                </div> <!-- end row-->
                            </div> <!-- end widget-rounded-circle-->
                        </div>

                        {{--<div class="col-md-4 col-xl-4">
                            <div class="widget-rounded-circle card-box">
                                <div class="row">
                                    <div class="col-6 pull-left">
                                        <div class="avatar-lg rounded-circle bg-warning">
                                            <i class="fa fa-clock-o font-22 avatar-title text-white"></i>
                                        </div>
                                    </div>
                                    <div class="col-6" style="padding-right: 10px">
                                        <div class="text-right">
                                            <h3 class="text-dark mt-1"><span data-plugin="counterup">0</span></h3>
                                            <p class="text-muted mb-1 text-truncate">Produits Créés</p>
                                        </div>
                                    </div>
                                </div> <!-- end row-->
                            </div> <!-- end widget-rounded-circle-->
                        </div>--}}

                        <div class="col-md-4 col-xl-4">
                            <div class="widget-rounded-circle card-box">
                                <div class="row">
                                    <div class="col-6 pull-left">
                                        <div class="avatar-lg rounded-circle bg-success">
                                            <i class="fa fa-file font-22 avatar-title text-white"></i>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="text-right">
                                            <h3 class="text-dark mt-1"><span data-plugin="counterup">{{count($bls)}}</span></h3>
                                            <p class="text-muted mb-1 text-truncate">Bon de commande</p>
                                        </div>
                                    </div>
                                </div> <!-- end row-->
                            </div> <!-- end widget-rounded-circle-->
                        </div>

                        {{--<div class="col-md-3 col-xl-3">
                            <div class="widget-rounded-circle card-box">
                                <div class="row">
                                    <div class="col-6 pull-left">
                                        <div class="avatar-lg rounded-circle bg-success">
                                            <i class="fa fa-check-circle font-22 avatar-title text-white"></i>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="text-right">
                                            <h3 class="text-dark mt-1"><span data-plugin="counterup">3195</span></h3>
                                            <p class="text-muted mb-1 text-truncate">Closed Tickets</p>
                                        </div>
                                    </div>
                                </div> <!-- end row-->
                            </div> <!-- end widget-rounded-circle-->
                        </div>

                        <div class="col-md-3 col-xl-3">
                            <div class="widget-rounded-circle card-box">
                                <div class="row">
                                    <div class="col-6 pull-left">
                                        <div class="avatar-lg rounded-circle bg-danger">
                                            <i class="fa fa-trash font-22 avatar-title text-white"></i>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="text-right">
                                            <h3 class="text-dark mt-1"><span data-plugin="counterup">128</span></h3>
                                            <p class="text-muted mb-1 text-truncate">Deleted Tickets</p>
                                        </div>
                                    </div>
                                </div> <!-- end row-->
                            </div> <!-- end widget-rounded-circle-->
                        </div>--}}
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="page-title">PRODUIT ASSIGNER</h4>
                            <ol class="breadcrumb"> </ol>
                        </div>

                        <div class="col-md-12">
                            <div class="card-box">
                                <table class="table table-hover m-0 table-centered dt-responsive nowrap w-100 mt-4 actus" id="myTable" >
                                    <thead>
                                    <tr>
                                        <th>Num</th>
                                        <th>Produit</th>
                                        <th>Catégorie</th>
                                        {{--<th>Type</th>--}}
                                        <th title="Quantité attendue">Q.Attendue</th>
                                        <th title="Montant attendue">M.Attendue</th>
                                        <th title="Quantité livré">Q.Livré</th>
                                        <th title="Quantité restante">Q.restante</th>
                                        <th title="Taux avancement">Taux</th>
                                        <th title="Quantité en stock">Q.Stock</th>
                                        <th title="Montant en stock">M.Stock</th>
                                        <th>Date</th>
                                        @if($project->etat==0)
                                        <th class="hidden-sm">Action</th>
                                        @endif
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @foreach($stocks as $k=>$stock)
                                    @if($stock->produit)
                                        @php
                                            $cat = \App\Categorie::where('id',$stock->produit->categorie_id)->first();
                                            $scat = \App\Scategorie::where('id',$stock->produit->scategorie_id)->first();
                                            $type = \App\Typeproduit::where('id',$stock->produit->type_id)->first();
                                            $livr = \App\Blitem::where('pjt_id',$stock->pjt_id)->where('pdt_id',$stock->pdt_id)->get();
                                            $taux = $stock->qte_first ? $livr->sum('qte') * 100 / $stock->qte_first : 0;
                                        @endphp
                                        <tr>
                                            <td><b>0{{$k+1}}</b></td>
                                            <td>{{$stock->produit->nom}} <br><span class="badge badge-xs bg-soft-primary text-secondary">@price($stock->produit->price) Fcfa</span><br><span class="badge badge-xs bg-soft-secondary text-secondary">{{$type->libelle}}</span> </td>
                                            <td>{{$cat->libelle}}</td>
                                            {{--<td><span class="badge bg-soft-secondary text-secondary">{{$type->libelle}}</span></td>--}}
                                            <td>@if($stock->qte_first) @price($stock->qte_first) @else {{$stock->qte_first}} @endif </td>
                                            <td><span class="label label-info">@price($stock->qte_first * $stock->produit->price) F</span></td>
                                            <td>@if($livr) @price($livr->sum('qte')) @else 0 @endif</td>
                                            <td>
                                                @if($stock->qte_first)
                                                    @if($livr)
                                                        @if($stock->qte_first - $livr->sum('qte') > 0)
                                                            @price($stock->qte_first - $livr->sum('qte'))
                                                        @else
                                                            0
                                                        @endif
                                                    @else
                                                        @price($stock->qte_first)
                                                    @endif
                                                @else
                                                    0
                                                @endif
                                            </td>
                                            <td>
                                                @if(round($taux) < 33)
                                                    <span class="label label-danger">@price($taux)%</span>
                                                @elseif (round($taux) > 34 and round($taux) < 99)
                                                    <span class="label label-warning">@price($taux)%</span>
                                                @else
                                                    <span class="label label-success">@price($taux)%</span>
                                                @endif

                                            </td>
                                            <td>
                                                @if($stock->qte < 5)
                                                    <span class="label label-danger">@price($stock->qte)</span>
                                                @else
                                                    <span class="label label-success">@price($stock->qte)</span>
                                                @endif
                                            </td>
                                            <td><span class="label label-info">@price($stock->qte * $stock->produit->price) F</span></td>
                                            <td>{{$stock->created_at->format('d/m/Y')}}</td>
                                            @if($project->etat==0)
                                            <td>
                                                <div class="row">
                                                    <a class="dropdown-item btn btn-xs btn-success" href="#" data-toggle="modal" data-target="#edit-modal"  onclick="editstock('{{$stock->produit->nom}}','{{$stock->qte_first}}','{{$stock->id}}')" title="Modifier"><i class="fa fa-edit"></i></a>
                                                    @if($livr->sum('qte') < 1)
                                                    <a class="dropdown-item btn btn-xs btn-danger delete" href="{{route('projects.deleteProject',[$stock->id,$project->id])}}"  title="supprimer"><i class="fa fa-trash"></i></a>
                                                    {{--<a class="dropdown-item btn btn-xs btn-info" href="#"><i class="fa fa-share-square-o"></i> Transferer</a>--}}
                                                    @endif
                                                </div>
                                            </td>
                                            @endif
                                        </tr>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div><!-- end col -->

                        <div class="col-md-12">
                            <h4 class="page-title">
                                BON DE COMMANDE
                                @if($project->etat==0)<a href="{{route('bl.create')}}" class="btn btn-success btn-xs">Créer un bon</a>@endif
                                <ol class="breadcrumb"> </ol>
                            </h4>
                        </div>

                        <div class="col-lg-12">
                            <div class="card-box" style="">
                                <div class="table-responsive">
                                    <table class="table table-actions-bar m-b-0">
                                        <thead>
                                        <tr>
                                            <th>N°</th>
                                            <th>Nom</th>
                                            <th>Date</th>
                                            <th>Status</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($bls as $bon)
                                        <tr>
                                            <td>#0{{$k+1}}</td>
                                            <td>{{$bon->slug}}</td>
                                            <td>{{$bon->created_at->format('d/m/Y')}}</td>
                                            <td>
                                                @if($bon->etat == 0)
                                                    <button type="button" class="btn btn-danger btn-sm waves-effect waves-light">Non traité</button>
                                                @else
                                                    <button type="button" class="btn btn-success btn-sm waves-effect waves-light">Traité</button>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection
