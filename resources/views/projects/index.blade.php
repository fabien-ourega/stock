@extends('layouts.app')

@section('title')
    Mes Projets
    @parent
@stop

@section('header_styles')
    <!-- DataTables -->
    <link href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('footer_scripts')
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.js') }}"></script>

    <script src="{{ asset('assets/pages/datatables.init.js') }}"></script>

    <!-- Sweet-Alert  -->
    <script src="{{ asset('assets/pages/jquery.sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/imask.js') }}"></script>
    {{--<script src="{{ asset('assets/pages/jquery.sweet-alert.init.js') }}"></script>--}}

    <script type="text/javascript">
        $(document).ready(function () {
            $('.select2').select2();

            $('#myTable').dataTable({
                "ordering": false,
                language: {
                    lengthMenu: "_MENU_",
                    search: "_INPUT_",
                    searchPlaceholder: "Recherche",
                    sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                    sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    sInfoPostFix:    "",
                    sLoadingRecords: "Chargement en cours...",
                    sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
                    sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
                    sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    oPaginate: {
                        "sFirst":    "Premier",
                        "sLast":    "Dernier",
                        "sNext":    "Suivant",
                        "sPrevious": "Précédent"
                    },
                }
            });
        });

        $('.actus').on('click', '.delete', function (event) {
            event.preventDefault();
            var href = $(this).attr('href');
            swal({
                title: "Êtes-vous sûr?",
                text: "Voulez vous vraiment supprimer cet projet",
                icon: "warning",
                buttons: true,
                buttons: ["Annuler", "Oui"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    window.location = href;
                }
            });
        });

        $('.actus').on('click', '.clore', function (event) {
            event.preventDefault();
            var href = $(this).attr('href');
            swal({
                title: "Êtes-vous sûr?",
                text: "Voulez vous vraiment clôturer cet projet",
                icon: "warning",
                buttons: true,
                buttons: ["Annuler", "Oui"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    window.location = href;
                }
            });
        });

        $('#check_all').on('click', function(e){
            if($(this).is(':checked',true)){
                $(".role_item").prop('checked', true);
            }else{
                $(".role_item").prop('checked', false);
            }
        });

        $('#sendActionGroup').on('click', function(e) {

            if($('#actionGroup').val()=='del_select'){
                var allVals = [];
                $(".role_item:checked").each(function() {
                    allVals.push($(this).attr('data-id'));
                });
                //console.log(allVals.length);
                // return false;
                if(allVals.length <=0)
                {
                    swal("Oops","Veuillez selectionnez des lignes à supprimer!","error" )
                }
                else {
                    //$("#loading").show();
                    swal({
                        title: "Êtes-vous sûr?",
                        text: "Voulez vous vraiment supprimer ces lignes",
                        icon: "warning",
                        buttons: true,
                        buttons: ["Annuler", "Oui"],
                        dangerMode: true,
                    }).then((willDelete) => {
                        if(willDelete) {
                            var valueIds = allVals.join(",");

                            $.ajax({
                                url:"{{ route('users.deletes') }}",
                                method:"GET",
                                data:{value:valueIds},
                                success:function(res){
                                    console.log(res);
                                    if(res == '1'){
                                        //$("#loading").hide();
                                        location.reload();
                                    }else{
                                        //$("#loading").hide();
                                        location.reload();
                                    }
                                }
                            })
                        }
                    });
                }
            }else{
                return false;
                //swal("Oops","Something went wrong!","error" )
            }

        });


        function editstock(libel,description,id){
            $("#formEdit")[0].reset();
            $("#idslug").val(id);
            $("#elibelle").val(libel);
            $("#edescription").val(description);
        }

        function showform(){
            $('#etats').show();
            $('#showform').hide();
            $('#hideform').show();
        }
        function hideform(){
            $('#etats').hide();
            $('#hideform').hide();
            $('#showform').show();
        }

    </script>
@endsection


@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="page-title">
                            Mes Projets
                            <button type="button" class="btn btn-default btn-xs" data-toggle="modal" data-target="#standard-modal">Ajouter</button>
                        </h4>

                        @if(Auth::user()->role_id!=2)
                        <div class="pull-right">
                            <button onclick="showform()" id="showform"  class="btn btn-default btn-xs"><i class="fa fa-bar-chart"></i> Etat</button>
                            <button onclick="hideform()" id="hideform" class="btn btn-default btn-xs" style="display: none;"><i class="fa fa-bar-chart"></i> Fermer </button>
                        </div>
                        @endif

                        <ol class="breadcrumb"> </ol>
                    </div>
                </div>

                @if(Auth::user()->role_id!=2)
                <div style="display: none;" id="etats" class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="standard-modalLabel">Edition d'un etat <br>
                                <small class="text-success"><span id="lbbe"></span></small>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </h4>
                        </div>
                        <form id="formAdd" action="{{route('etats.projets')}}" class="form-horizontal" method="post" autocomplete="off">
                            @csrf
                            <div class="modal-body">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label class="col-md-12" for="password">Projet</label>
                                        <select name="project[]" id="project" class="form-control select2" multiple required>
                                            <option value="all">TOUS LES PROJETS</option>
                                            @foreach($projects as $project)
                                                <option value="{{$project->id}}">{{$project->libelle}}</option>
                                            @endforeach
                                            {!! count($projects)>0 ? '<option disabled role=separator>':'' !!}
                                            <option value="0">Stock Interne</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                        <div class="col-md-12">
                                        <div class="radio radio-inline">
                                            <input type="radio" id="inlineRadio4" value="pdf" name="typexport" checked="">
                                            <label for="inlineRadio4"> PDF </label>
                                        </div>
                                        <div class="radio radio-inline">
                                            <input type="radio" id="inlineRadio5" value="excel" name="typexport">
                                            <label for="inlineRadio5"> EXCEL </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Etat</button>
                            </div>
                        </form>
                    </div><!-- /.modal-content -->
                </div>
                @endif

                <div id="standard-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="standard-modalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="standard-modalLabel">Ajouter un projet
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                </h4>
                            </div>
                            <form action="{{route('projects.store')}}" class="form-horizontal" method="post" autocomplete="off">
                                @csrf
                                <div class="modal-body">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label class="col-md-12" for="password">Libelle <span class="text-danger">*</span></label>
                                            <input type="text" name="libelle" class="form-control col-md-12" required>
                                        </div>
                                        <div class="col-sm-12 m-t-20">
                                            <label class="col-md-12" for="password">Ajouter les produits des catégories <span class="text-danger">*</span></label>
                                            <select name="categorie_id[]" class="form-control select2" required multiple>
                                                <option disabled>Selectionner une catégorie</option>
                                                @foreach($cats as $categorie)
                                                    <option value="{{$categorie->id}}">{{$categorie->libelle}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="col-sm-12 m-t-20">
                                            <label class="col-md-12" for="description">Description</label>
                                            <textarea class="form-control" name="description" id="description" cols="10" rows="5"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-light" data-dismiss="modal">Annuler</button>
                                    <button type="submit" class="btn btn-primary">Ajouter</button>
                                </div>
                            </form>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div>

                <div id="edit-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="edit-modalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="standard-modalLabel">Modifier un projet
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                </h4>
                            </div>
                            <form id="formEdit" action="{{route('projects.update')}}" class="form-horizontal" method="post" autocomplete="off">
                                @csrf
                                <div class="modal-body">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label class="col-md-12" for="">Libelle <span class="text-danger">*</span></label>
                                            <input type="text" id="elibelle" name="libelle" class="form-control col-md-12" required>
                                        </div>

                                        <div class="col-sm-12">
                                            <label class="col-md-12" for="description">Description</label>
                                            <textarea class="form-control" name="description" id="edescription" cols="10" rows="5"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <input type="hidden" name="idslug" id="idslug">
                                    <button type="button" class="btn btn-light" data-dismiss="modal">Annuler</button>
                                    <button type="submit" class="btn btn-primary">Modifier</button>
                                </div>
                            </form>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div>


                <div class="row">
                    <div class="col-md-12">
                        <div class="card-box table-responsive">
                            <table id="myTable" class="table table-striped table-bordered actus">
                                <thead>
                                <tr>
                                    <th>Ref.No.</th>
                                    {{--<th> <input type="checkbox" id="check_all"></th>--}}
                                    <th>Nom</th>
                                    <th>Description </th>
                                    <th>Nombre de produit </th>
                                    {{--<th>Etat </th>--}}
                                    <th>Date d'ajout</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($projects as $k=>$user)
                                    <tr>
                                        <td>#0{{$k+1}} </td>
                                        {{--<td><input type="checkbox" class="role_item" data-id="{{$user->id}}"></td>--}}
                                        <td style="font-weight: bold">
                                            {{$user->libelle}}
                                            <div class="row-actions-edit">
                                                <span class="edit"> <a href="{{route('projects.show',$user->slug)}}">Voir</a> | </span>
                                                @if (in_array('del_pjt', Session::get('permissions')))
                                                <span class="trash"><a href="{{route('projects.delete',$user->id)}}" class="delete" style="color: red">Supprimer</a> | </span>
                                                @endif
                                                <span class="edit"> <a href="#" data-toggle="modal" data-target="#edit-modal"  onclick="editstock('{{$user->libelle}}','{{$user->description}}','{{$user->id}}')">Modifier</a></span>
                                                @if (in_array('del_pjt', Session::get('permissions')))
                                                | <span class="trash"><a href="{{route('projects.clore',$user->id)}}" class="clore" style="color: red">Clôturer </a></span>
                                                @endif
                                            </div>
                                        </td>
                                        <td>{{$user->description}}</td>
                                        <td>{{$user->produits->count()}}</td>
                                        {{--<td>
                                            @if($user->etat == 0)
                                                <span class="label label-danger">En cours</span>
                                            @else
                                                <span class="label label-success">Terminer</span>
                                            @endif
                                        </td>--}}
                                        <td>{{$user->created_at->format('d/m/Y')}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div> <!-- container -->
        </div> <!-- content -->
    </div>
@endsection
