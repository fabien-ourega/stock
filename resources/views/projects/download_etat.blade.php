<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<style type="text/css" media="all">
    @page { size: auto;  margin: 0mm; }

    body{
        font-size: 10px;
        margin: 1.5px;
    }
    table {
        font-size: 11px;
    }

    .td{
        border: 1px solid #000000;
        padding-left: 5px;
        padding: 10px;
    }
    .tdd{
        padding: 3px;
        border-bottom: 1px solid #e2e2e2;
    }
    .txt-center{
        text-align: center;
    }
    .txt-right{
        text-align: right;
    }
    .pr-20{
        padding-right: 20px;
    }
    .mb-20{
        margin-bottom: 20px;
    }
    #resultscol td{
        border: 1px solid #000000;
        padding: 3px;
    }

    #fin ul li{
        list-style: none;
        text-align: left;
    }

    .custom-control-label::before,
    .custom-control-label::after {
        top: 0.1rem !important;
        left: -2rem !important;
        width: 1.25rem !important;
        height: 1.25rem !important;
    }

    footer {
        position: fixed;
        bottom: 0cm;
        left: 0cm;
        right: 0cm;
        height: 2cm;

        color: #000;
        font-size: 10px;
        text-align: center;
    }

    .breakNow{
        page-break-inside:avoid;
        page-break-after:always;
    }

</style>
<body >
<div class="breakNow" id="gsetat">
    <table border="0" cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td>
                <div class="col">
                    <img src="{{asset('assets/images/logo_dark.png')}}" data-holder-rendered="true" height="50px" />

                </div>
            </td>

        </tr>
    </table>
    <br><br><br>
    <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td align="center">
                <p style="font-size: 18px;font-weight: bold">ETAT DES PROJETS</p>
            </td>
        </tr>

    </table>

    <table style="" class="mb-20" cellspacing="0" cellpadding="0" align="center" width="100%">
        <tr style="background-color: #e1e1e8">
            <td class="td"><strong>PROJET</strong></td>
            <td class="td"><strong>PRODUIT</strong></td>
            <td class="td"><strong>Q.ATTENDUE</strong></td>
            <td class="td"><strong>M.ATTENDUE</strong></td>
            <td class="td"><strong>Q.LIVRE</strong></td>
            <td class="td"><strong>Q.RESTANTE</strong></td>
            <td class="td"><strong>TAUX</strong></td>
            <td class="td"><strong>Q.STOCK</strong></td>
            <td class="td"><strong>M.STOCK</strong></td>
        </tr>

    @foreach($datas as $data)
        @php
            $stocks = \App\Bstock::where('pjt_id',$data->id)->with('produit','project')->get();
        @endphp

        @foreach($stocks as $stock)
            @if($stock->produit)
                @php
                    $livr = \App\Blitem::where('pjt_id',$stock->pjt_id)->where('pdt_id',$stock->pdt_id)->get();
                    $taux = $stock->qte_first ? $livr->sum('qte') * 100 / $stock->qte_first : 0;
                @endphp
            <tr>

                <td>{{$stock->project? $stock->project->libelle : ''}}</td>
                <td>{{$stock->produit->nom}}</td>
                <td>@if($stock->qte_first) @price($stock->qte_first) @else {{$stock->qte_first}} @endif</td>
                <td>@price($stock->qte_first * $stock->produit->price)</td>
                <td>@if($livr) @price($livr->sum('qte')) @else 0 @endif</td>
                <td>
                    @if($stock->qte_first)
                        @if($livr)
                            @if($stock->qte_first - $livr->sum('qte') > 0)
                                @price($stock->qte_first - $livr->sum('qte'))
                            @else
                                0
                            @endif
                        @else
                            @price($stock->qte_first)
                        @endif
                    @else
                        0
                    @endif
                </td>
                <td>{{$taux}}%</td>
                <td>@price($stock->qte)</td>
                <td>@price($stock->qte * $stock->produit->price)</td>
            </tr>
            @endif
        @endforeach
    @endforeach
    </table>
</div>
{{--<footer>
    Cocody Angré, 8ème Tranche, Résidence Pacific Villa 73, 01 BP 473 Abidjan 01 <br>
    Tél. (225)06 32 86 36 / (225) 59 59 09 91, RC : CI-ABJ-2018-B-14915, CC : 18 29 457 U, Capital Social : 10.000.000 FCFA, Régime d'imposition : Réel simplifié
</footer>--}}

<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
<script>
    $(document).ready(function () {
        //alert('demo');
        //window.print();
        var restorepage = document.body.innerHTML;
        var printcontent = document.getElementById("gsetat").innerHTML;
        document.body.innerHTML = printcontent;
        window.print();
        document.body.innerHTML = restorepage;
    });
</script>

</body>
</html>
