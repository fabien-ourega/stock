<html>
<style>
    body{
        font-size: 10px;
        margin: 1.5px;
    }
    table {
        font-size: 11px;
    }

    .td{
        border: 1px solid #000000;
        padding-left: 5px;
        padding: 10px;
    }
    .tdd{
        padding: 3px;
        border-bottom: 1px solid #e2e2e2;
    }
    .txt-center{
        text-align: center;
    }
    .txt-right{
        text-align: right;
    }
    #resultscol td{
        border: 1px solid #000000;
        padding: 3px;
    }

    #fin ul li{
        list-style: none;
        text-align: left;
    }

    .custom-control-label::before,
    .custom-control-label::after {
        top: 0.1rem !important;
        left: -2rem !important;
        width: 1.25rem !important;
        height: 1.25rem !important;
    }

    footer {
        position: fixed;
        bottom: 0cm;
        left: 0cm;
        right: 0cm;
        height: 2cm;

        /** Extra personal styles **/
        color: #000;
        font-size: 10px;
        text-align: center;
    }

</style>
<body>
<div>

    <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td align="center">
                <p style="font-size: 18px;font-weight: bold">ETAT DES PROJETS</p>
            </td>
        </tr>

    </table>
    <br>
    <table style="" cellspacing="0" cellpadding="0" align="center" width="100%">
        <tr style="background-color: #e1e1e8">
            <td class="td"><strong>PROJET</strong></td>
            <td class="td"><strong>PRODUIT</strong></td>
            <td class="td"><strong>Q.ATTENDUE</strong></td>
            <td class="td"><strong>M.ATTENDUE</strong></td>
            <td class="td"><strong>Q.LIVRE</strong></td>
            <td class="td"><strong>Q.RESTANTE</strong></td>
            <td class="td"><strong>TAUX</strong></td>
            <td class="td"><strong>Q.STOCK</strong></td>
            <td class="td"><strong>M.STOCK</strong></td>
        </tr>
        @foreach($datas as $data)
        @php
            $stocks = \App\Bstock::where('pjt_id',$data->id)->with('produit','project')->get();
        @endphp

        @foreach($stocks as $stock)
            @if($stock->produit)
                @php
                    $livr = \App\Blitem::where('pjt_id',$stock->pjt_id)->where('pdt_id',$stock->pdt_id)->get();
                    $taux = $stock->qte_first ? $livr->sum('qte') * 100 / $stock->qte_first : 0;
                @endphp
            <tr>
                <td>{{$stock->project? $stock->project->libelle : ''}}</td>
                <td>{{$stock->produit->nom}}</td>
                <td>@if($stock->qte_first) @price($stock->qte_first) @else {{$stock->qte_first}} @endif</td>
                <td>@price($stock->qte_first * $stock->produit->price)</td>
                <td>@if($livr) @price($livr->sum('qte')) @else 0 @endif</td>
                <td>
                    @if($stock->qte_first)
                        @if($livr)
                            @if($stock->qte_first - $livr->sum('qte') > 0)
                                @price($stock->qte_first - $livr->sum('qte'))
                            @else
                                0
                            @endif
                        @else
                            @price($stock->qte_first)
                        @endif
                    @else
                        0
                    @endif
                </td>
                <td>@price($taux)%</td>
                <td>@price($stock->qte)</td>
                <td>@price($stock->qte * $stock->produit->price)</td>
            </tr>
            @endif
        @endforeach
    @endforeach

    </table>
    <br>
</div>

</body>
</html>
