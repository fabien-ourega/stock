@extends('layouts.app')

@section('title')
    Achat
    @parent
@stop

@section('header_styles')
    <!-- DataTables -->
    <link href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
    <style>
        tr.spacend td{
            padding-bottom: 3em!important;
        }
    </style>
@endsection

@section('footer_scripts')
    <!-- Sweet-Alert  -->
    <script src="{{ asset('assets/pages/jquery.sweetalert.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
        });
    </script>
@endsection


@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="page-title">
                            Achat
                        </h4>
                        <ol class="breadcrumb"> </ol>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">

                            <div class="panel-body">
                                <div class="clearfix">
                                    <div class="pull-left">
                                        <h4 class="text-right"><img src="{{asset('assets/images/logo_dark.png')}}" alt="velonic" width="150"></h4>

                                    </div>
                                    <div class="pull-right">
                                        <h4>Achat <br>
                                            <strong>{{$achat->slug}}</strong>
                                        </h4>
                                        <h5>Statut <br>
                                            @if($achat->status==0)
                                            <span class="label label-danger">Rejeté</span>
                                            @elseif($achat->status==1)
                                            <span class="label label-warning">En attente de validation</span>
                                            @elseif($achat->status==2)
                                            <span class="label label-success">Validé</span>
                                            @endif
                                        </h5>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="pull-left m-t-30">
                                            <address>
                                                <strong>Référence : </strong> {{$achat->reference}}<br>
                                                <strong>Fournisseur : </strong> {{$achat->fournisseur->libelle}}<br>
                                                <strong>Mode paiement : </strong> <span class="label label-success">{{$achat->modepaie->libelle}}</span> <br>
                                            </address>
                                        </div>
                                        <div class="pull-right m-t-30">
                                            <p><strong>Date de paiement: </strong> {{date('d/m/Y',strtotime($achat->datepaiement))}}</p>
                                            <p class="m-t-10"><strong>Montant TTC: </strong> <span class="label label-success">@if(is_int($achat->montant)) @price($achat->montant ?? 0) Fcfa @else {{$achat->montant}} @endif</span></p>
                                            <p class="m-t-10"><strong>Date de creation: </strong> {{date('d/m/Y',strtotime($achat->created_at))}}</p>
                                            <p class="m-t-10"><strong>Magasin: </strong> {{$achat->magasin? $achat->magasin->nom : '-'}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="m-h-50"></div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table m-t-30">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Désignation</th>
                                                    <th>Unité</th>
                                                    <th>Quantités</th>
                                                    <th>Prix unitaire</th>
                                                    <th>Montant total</th>
                                                    <th>Taxe </th>
                                                    <th>Montant TTC</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach ($achat->items as $i=>$item)
                                                @php $pdt = \App\Produit::where('id',$item->produit_id)->with('type')->first() @endphp
                                                    <tr>
                                                        <td>{{$i+1}}</td>
                                                        {{-- <td>{{$pdt->code}}</td> --}}
                                                        <td><b>{{$pdt->nom}}</b></td>
                                                        <td>{{$pdt->type->libelle}}</td>
                                                        <td>{{$item->qte}} </td>
                                                        <td>@price($item->price)</td>
                                                        <td>@price($item->qte * $item->price)</td>
                                                        <td>{{$item->taxe}} %</td>
                                                        <td>@price($item->mtt)</td>
                                                    </tr>
                                                @endforeach

                                                    {{-- <tr><td colspan="7"></td></tr> --}}
                                                    <tr>
                                                        <td colspan="7" class="text-right"><b>MONTANT TOTAL TTC</b></td>
                                                        <td class="font-weight-bold"><span class="label label-success">@if(is_int($achat->montant)) @price($achat->montant ?? 0) Fcfa @else {{$achat->montant}} @endif</span></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <br>

                                <hr>
                                {{-- <div class="hidden-print">
                                    <div class="pull-right">
                                        <a href="{{route('factures.download',$bl->id)}}" class="btn btn-inverse waves-effect waves-light"><i class="fa fa-download"></i> Télécharger</a>
                                    </div>
                                </div> --}}
                            </div>
                        </div>

                    </div>

                </div>

            </div> <!-- container -->
        </div> <!-- content -->
    </div>
@endsection
