@extends('layouts.app')

@section('title')
    Achats rejetés
    @parent
@stop

@section('header_styles')
    <!-- DataTables -->
    <link href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/switchery/css/switchery.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css"/>

@endsection

@section('footer_scripts')
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.js') }}"></script>

    <script src="{{ asset('assets/pages/datatables.init.js') }}"></script>

    <!-- Sweet-Alert  -->
    <script src="{{ asset('assets/pages/jquery.sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/switchery/js/switchery.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.select2').select2();

            $('#myTable').dataTable({
                language: {
                    lengthMenu: "_MENU_",
                    search: "_INPUT_",
                    searchPlaceholder: "Recherche",
                    sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                    sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    sInfoPostFix:    "",
                    sLoadingRecords: "Chargement en cours...",
                    sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
                    sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
                    sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    oPaginate: {
                        "sFirst":    "Premier",
                        "sLast":    "Dernier",
                        "sNext":    "Suivant",
                        "sPrevious": "Précédent"
                    },
                }
            });


        });


    </script>
@endsection


@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="page-title">
                            Liste des achats rejetés
                            <a href="{{route('achats.create')}}" class="btn btn-default btn-xs">Ajouter</a>
                        </h4>
                        <ol class="breadcrumb"> </ol>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card-box table-responsive">
                            <table id="myTable" class="table table-striped table-bordered actus">
                                <thead>
                                <tr>
                                    <th>N°</th>
                                    <th>Code</th>
                                    <th>Référence</th>
                                    <th>Date</th>
                                    <th>Fournisseur</th>
                                    <th>Mode paiement</th>
                                    <th>Montant</th>
                                    <th>Agent</th>
                                    <th>Status</th>
                                    <th>Motif</th>
                                    <th>Date d'ajout</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($achats as $k=>$item)
                                    <tr>
                                        <td>{{$k+1}}</td>
                                        <td>{{$item->slug}}</td>
                                        <td style="font-weight: bold">
                                            {{$item->reference}}
                                            <div class="row-actions-edit">
                                                <span class="edit"> <a href="{{route('achats.show',$item->id)}}" target="_blank">Voir</a></span>
                                            </div>
                                        </td>
                                        <td>{{date('d/m/Y',strtotime($item->datepaiement))}}</td>
                                        <td>{{$item->fournisseur->libelle}}</td>
                                        <td>{{$item->modepaie->libelle}}</td>
                                        <td>
                                            <p title="Montant TTC"><span class="label label-table label-success">@price($item->montant) Fcfa</span></p>
                                        </td>
                                        <td>{{$item->user->name}}</td>
                                        <td>
                                            <span class="label label-table label-danger">Rejeter</span>
                                        </td>
                                        <td>{!! $item->motif !!}</td>
                                       <td>{{$item->created_at->format('d/m/Y')}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div> <!-- container -->
        </div> <!-- content -->
    </div>
@endsection
