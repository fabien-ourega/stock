@extends('layouts.app')

@section('title')
    Ajouter un achat
    @parent
@stop

@section('header_styles')
    {{--<link href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>--}}
    <link href="{{ asset('assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css"/>
    <style>
        .metable {
            margin: 30px 0;
            border: 1px solid green;
        }
    </style>
@endsection

@section('footer_scripts')
    <script src="{{ asset('assets/js/imask.js') }}"></script>
    <!-- Sweet-Alert  -->
    <script src="{{ asset('assets/pages/jquery.sweetalert.min.js') }}"></script>
    {{--<script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>--}}
    <!--form validation init-->
    <script src="{{ asset('assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js') }}" type="text/javascript"></script>

    <script src="{{ asset('assets/plugins/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-select/js/bootstrap-select.min.js') }}"></script>

    <script>
         var valueSelect = '',
            inputs = [],
            arr = [],
            valuemontantt = 0,
            x = 0;

        $(document).ready(function () {
            $('.select2').select2();

            IMask(document.getElementById('price'), {
                mask: Number,
                min: 1,
                max: 100000000000,
                thousandsSeparator: ' '
            });
        });


        function add_element_to_array(){
            var qtiterest = 0;
            var qtite = $("#qte").val();
            var priceold = $("#price").val();
            var remise = $("#taxe").val();
            var price = parseFloat(priceold.replace(/\s/g,'').replace(',', '.'));
            var produits = $("#pdt").val();
            // console.log(produits);

            if(qtite == "" || qtite<1){
                swal("Oups!", "Veuillez renseigner la quantité", "error");
                return false ;
            }else if(produits == "" || produits== null){
                swal("Oups!", "Veuillez choisir un produit ", "error");
                return false ;
            }else if(price == ""){
                swal("Oups!", "Veuillez renseigner le prix unitaire ", "error");
                return false ;
            }else if(parseInt(remise) > 100){
                swal("Oups!", "Veuillez renseigner une remise en dessous ou égale à 100", "error");
                return false ;
            } else{
                var valueSelect = $('#pdt');
                var selectId = valueSelect.val(),
                    pjtid = $('#pjt').val(),
                    selectLibellType = valueSelect.find(':selected').attr('data-typepdt'),
                    selectLibell = valueSelect.find(':selected').attr('data-libelle');

                if(remise=='' || remise==0){
                    remise = 0;
                }

                var checkexit = inputs.filter(element => {
                    return element.id == selectId
                    // return element.selectId == selectId && element.proprios == proprios
                })

                if(checkexit.length==0){
                    var pxht = price + ((remise /100) * price),
                        mttble = pxht * qtite;
                        valuemontantt = valuemontantt + parseFloat(mttble);

                    arr = {id:selectId , libelletype:selectLibellType, libelle:selectLibell ,qtecmde:qtite, price:price, remise:remise, mttt:mttble};
                    inputs.push(arr);
                    //console.log(arr);
                    showhtml();
                    $('#mttotal').html(Number(valuemontantt).toLocaleString('fr-FR') +' Fcfa');
                }else{
                    swal("Oups!", "Cette ligne existe déjà.", "error");
                    return false ;
                }
            }
        }

        function showhtml() {
            $("#data_session").empty();
            var table = "<tbody>";
            //console.log(inputs);
            table += '<tr class="bg-primary text-white">';
            table += '<td class="border-b">NUM</td>';
            table += '<td class="border-b text-lg font-medium">PRODUIT</td>';
            table += '<td class="border-b text-lg font-medium">TYPE</td>';
            table += '<td class="border-b text-lg font-medium">QUANTITES</td>';
            table += '<td class="border-b text-lg font-medium">P.U</td>';
            table += '<td class="border-b text-lg font-medium">MONTANT HT</td>';
            table += '<td class="border-b text-lg font-medium">TAXE (%)</td>';
            table += '<td class="border-b text-lg font-medium">MONTANT TTC</td>';
            table += '<td class="border-b">ACTION</td>';
            table += '</tr>';

            for (var i = 0; i < inputs.length; i++) {
                var y = i + 1;

                var pxht = inputs[i].price + ((inputs[i].remise /100) * inputs[i].price),
                    mttble = pxht * inputs[i].qtecmde,
                    pxhtformat = Number(inputs[i].qtecmde * inputs[i].price).toLocaleString('fr-FR'),
                    mttbleformat = Number(mttble).toLocaleString('fr-FR'),
                    priceformat = Number(inputs[i].price).toLocaleString('fr-FR');

                table += '<tr data-row-id='+i+'>';
                table += '<td class="border-b">' + y + '</td>';
                table += '<td class="border-b">' + inputs[i].libelle + '</td>';
                table += '<td class="border-b">' + inputs[i].libelletype + '</td>';
                table += '<td class="border-b">' + inputs[i].qtecmde + '</td>';
                table += '<td class="border-b">' + priceformat + '</td>';
                table += '<td class="border-b">' + pxhtformat + '</td>';
                table += '<td class="border-b">' + inputs[i].remise + '</td>';
                table += '<td class="border-b">' + mttbleformat + '</td>';
                table += '<td class="border-b"><a style="cursor: pointer" onclick="deleteSession('+i+','+inputs[i].mttt+')" class="inline-block text-1xl text-orange-500 mx-2" title="Supprimer la ligne"><i class="fa fa-trash text-danger" class="w-4 h-4 mr-2"></i></a></div>';
                table +='<input type="hidden" value="' + inputs[i].id + ';' + inputs[i].libelle + ';' + inputs[i].libelletype + ';' + inputs[i].qtecmde + ';' + inputs[i].price +';' + inputs[i].remise +'" name="datasession[]">';
                table += '</tr>';
            };

            table += '<tr class="mt-3">';
            table += '<td class="border-b"></td>';
            table += '<td class="border-b"></td>';
            table += '<td class="border-b"></td>';
            table += '<td class="border-b"></td>';
            table += '<td class="border-b"></td>';
            table += '<td class="border-b"></td>';
            table += '<td class="border-b text-lg font-medium bg-primary text-white">Montant Total TTC</td>';
            table += '<td class="border-b text-lg font-medium" id="mttotal"></td>';
            table += '<td class="border-b"></td>';
            table += '</tr>';
            table += '</tbody>';

            table += '</tbody>';

            $("#data_session").append(table);
        }

        function deleteSession(id,mtt) {
            valuemontantt = valuemontantt - mtt;
            inputs.splice(id,1);
            showhtml();
            $('#mttotal').empty();
            // $('#mttotal').html(valuemontantt.toLocaleString()+' Fcfa');
            $('#mttotal').html(Number(valuemontantt).toLocaleString('fr-FR') +' Fcfa');
        }

        function submitForm() {
            event.preventDefault();
            if($('#libell').val()==''){
                swal("Oups!", "Veuillez renseigner le champ Nom.");
            }else if($("#reference").val() == ""){
                swal("Oups!", "Veuillez renseigner la référence ", "error");
                return false ;
            }else if($("#fournid").val() == ""){
                swal("Oups!", "Veuillez choisir le fournisseur ", "error");
                return false ;
            }else if($("#magasin").val() == ""){
                swal("Oups!", "Veuillez choisir le magasin", "error");
                return false ;
            }else if($("#mode").val() == ""){
                swal("Oups!", "Veuillez renseigner le mode de reglement ", "error");
                return false ;
            }else if($('#file').get(0).files.length == 0){
                swal("Oups!", "Veuillez renseigner le champ Bon physique.");
            }else{
                var href = $(this).attr('href');
                swal({
                    title: "Êtes-vous sûr?",
                    text: "Vous êtes sur le point de faire un achat ! Cette action est irréversible ? ",
                    icon: "warning",
                    buttons: true,
                    buttons: ["Annuler", "Oui"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $('#loader').show();
                        document.getElementById('addbtn').disabled = true;
                        $('#addbtn').hide();
                        document.getElementById('addappelfonds').submit();
                    }
                });
            }

        }

    </script>
@endsection


@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">

                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Ajouter un achat</h4>
                        <ol class="breadcrumb"> </ol>
                    </div>
                </div>

                <div class="row">
                    <form enctype="multipart/form-data" id="addappelfonds" class="form-horizontal" method="post" action="{{route('achats.store')}}" role="form">
                        @csrf
                        <div class="col-md-12">
                            <div class="card-box">
                                <h4 class="card-title mb-3 text-primary">IDENTIFIANT ACHAT</h4>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group m-t-10">
                                            <div class="col-sm-12">
                                                <label>Numéro facture <span class="text-danger">*</span></label>
                                                <input type="text" name="reference" value="{{old('reference')}}" id="reference" class="form-control" required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label>Magasin <span class="text-danger">*</span></label>
                                                <select name="magasin" id="magasin" class="form-control" required>
                                                    <option selected disabled>Choisir le magasin</option>
                                                    @foreach ($magasins as $item)
                                                        <option value="{{$item->id}}">{{$item->nom}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label>Agent <span class="text-danger">*</span></label>
                                                <input type="text" readonly disabled value="{{Auth::user()->name}}" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card-box">
                                <h4 class="card-title mb-3 text-primary">FOURNISSEUR & MODE DE PAIEMENT</h4>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group m-t-10">
                                            <div class="col-sm-12">
                                                <label>Fournisseur <span class="text-danger">*</span></label>
                                                <select name="fournid" id="fournid" class="form-control select2" required>
                                                    <option value="" selected>Selectionnez le fournisseur</option>
                                                    @foreach($fournisseurs as $item)
                                                        <option value="{{$item->id}}" {{old('fournid') == $item->id ? 'selected' : ''}}>{{$item->libelle}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label>Date de l'achât <span class="text-danger">*</span></label>
                                                <input name="dateachat" id="dateachat" class="form-control" type="date" required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label>Mode de reglement <span class="text-danger">*</span></label>
                                                <select name="mode" id="mode" class="form-control" required>
                                                    <option selected disabled>Choisir le mode</option>
                                                    @foreach ($modes as $item)
                                                        @if($item->libelle != 'Virement')
                                                        <option value="{{$item->id}}" {{old('mode') == $item->id ? 'selected' : ''}}>{{$item->libelle}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label>Bon de livraison <span class="text-danger">*</span></label>
                                                <input type="file" name="file" id="file" class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label>Date de règlement </label>
                                                <input name="datepaiement" id="datepaiement" class="form-control" type="date">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card-box">
                                <h4 class="card-title mb-3 text-primary">COMMANDE</h4>

                                <div class="row">
                                    <div class="">
                                        <div class="row m-t-10">
                                            <div class="col-md-4">
                                                <div class="col-sm-12">
                                                    <label>Produits <span class="text-danger">*</span></label>
                                                    <select name="pdtid" id="pdt" class="form-control select2">
                                                        <option value="" selected disabled>Choisir un produit</option>
                                                        @foreach($produits as $item)
                                                            <option value="{{$item->id}}" {{old('pdtid') == $item->id ? 'selected' : ''}} data-typepdt="{{$item->type->libelle}}" data-libelle="{{$item->nom}}">{{$item->nom}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="col-md-12">
                                                    <label for="qte">Quantités <span class="text-danger">*</span></label>
                                                    <input type="number" min="1" id="qte" name="qte" class="form-control col-md-12">
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="col-md-12">
                                                    <label for="price">Prix U.HT<span class="text-danger">*</span></label>
                                                    <input type="text" min="1" id="price" name="price" class="form-control col-md-12">
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="col-md-12">
                                                    <label for="taxe">Taxe(%)</label>
                                                    <input type="number" min="1" value="0" id="taxe" name="taxe" class="form-control col-md-12">
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="col-span-12">
                                                    <label class="col-md-12" style="opacity:0;">Quantité</label>
                                                    <a onclick="add_element_to_array()" class="btn btn-primary btn-md waves-effect waves-light"><i class="fa fa-plus"></i> AJOUTER</a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="m-t-20">
                                            <input type="hidden" id="varieMontant">
                                            <table class="table" id="data_session">
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="text-right mt-5">
                                    <div id="loader" style="display: none;">
                                        <img src="{{asset('assets/images/preloader.gif')}}" style="width: 51px;" alt="loader">
                                    </div>
                                    <button id="addbtn" type="button" onclick="submitForm()" class="btn btn-success text-white" name="bouton" value="save"><i class="fa fa-save"></i> Enregistrer</button>
                                </div>
                            </div>

                        </div>

                    </form>
                </div>
            </div> <!-- container -->
        </div> <!-- content -->
    </div>
@endsection
