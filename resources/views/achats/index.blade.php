@extends('layouts.app')

@section('title')
    Achats
    @parent
@stop

@section('header_styles')
    <!-- DataTables -->
    <link href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/switchery/css/switchery.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css"/>

@endsection

@section('footer_scripts')
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.js') }}"></script>

    <script src="{{ asset('assets/pages/datatables.init.js') }}"></script>

    <!-- Sweet-Alert  -->
    <script src="{{ asset('assets/pages/jquery.sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/switchery/js/switchery.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.select2').select2();

            $('#myTable').dataTable({
                language: {
                    lengthMenu: "_MENU_",
                    search: "_INPUT_",
                    searchPlaceholder: "Recherche",
                    sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                    sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    sInfoPostFix:    "",
                    sLoadingRecords: "Chargement en cours...",
                    sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
                    sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
                    sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    oPaginate: {
                        "sFirst":    "Premier",
                        "sLast":    "Dernier",
                        "sNext":    "Suivant",
                        "sPrevious": "Précédent"
                    },
                }
            });

            $("#modepaie").change(function() {
                var valeur = $('#modepaie').val();
                //console.log(valeur);
                $('#avance_espece').hide();
                $('#avance_caisse').hide();

                if(valeur == 1){
                    //Chèque
                    $('#cheque').show();
                    $('#virement').hide();
                    $('#avance').hide();

                }else if(valeur==2){
                    //Virement
                    $('#cheque').hide();
                    $('#virement').show();
                    $('#avance').hide();

                }else if(valeur==3){
                    //Avance
                    $('#cheque').hide();
                    $('#virement').hide();
                    $('#avance').show();

                }else {
                    $('#cheque').hide();
                    $('#virement').hide();
                    $('#avance').hide();
                }
            });

            $("#avance").change(function() {
                var valeur = $('#avance_select').val();
                //console.log(valeur);

                if(valeur == 'espece'){
                    //espece
                    $('#avance_espece').hide();
                    $('#avance_caisse').show();

                }else if(valeur == 'cheque'){
                    $('#avance_espece').show();
                    $('#avance_caisse').hide();
                }else {
                    $('#avance_espece').hide();
                    $('#avance_caisse').hide();
                }
            });

        });

        $('.actus').on('click', '.delete', function (event) {
            event.preventDefault();
            var href = $(this).attr('href');
            swal({
                title: "Êtes-vous sûr?",
                text: "Voulez vous vraiment supprimer cet article",
                icon: "warning",
                buttons: true,
                buttons: ["Annuler", "Oui"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    window.location = href;
                }
            });
        });

        $('#check_all').on('click', function(e){
            if($(this).is(':checked',true)){
                $(".role_item").prop('checked', true);
            }else{
                $(".role_item").prop('checked', false);
            }
        });

        $('#sendActionGroup').on('click', function(e) {

            if($('#actionGroup').val()=='del_select'){
                var allVals = [];
                $(".role_item:checked").each(function() {
                    allVals.push($(this).attr('data-id'));
                });
                //console.log(allVals.length);
                // return false;
                if(allVals.length <=0)
                {
                    swal("Oops","Veuillez selectionnez des lignes à supprimer!","error" )
                }
                else {
                    //$("#loading").show();
                    swal({
                        title: "Êtes-vous sûr?",
                        text: "Voulez vous vraiment supprimer ces lignes",
                        icon: "warning",
                        buttons: true,
                        buttons: ["Annuler", "Oui"],
                        dangerMode: true,
                    }).then((willDelete) => {
                        if(willDelete) {
                            var valueIds = allVals.join(",");

                            $.ajax({
                                url:"{{ route('news.deletes') }}",
                                method:"GET",
                                data:{value:valueIds},
                                success:function(res){
                                    console.log(res);
                                    if(res == '1'){
                                        //$("#loading").hide();
                                        location.reload();
                                    }else{
                                        //$("#loading").hide();
                                        location.reload();
                                    }
                                }
                            })
                        }
                    });
                }
            }else{
                return false;
                //swal("Oops","Something went wrong!","error" )
            }

        });

        function update_status(el,ref){
            //console.log(el,ref);
            $('#myModal').modal('show');
            var id = $(el).attr('data-id');
            $("#tidsluge").val(id);
            $("#tlbbe").html(ref);
        }

        function showform(){
            $('#etats').show();
            $('#showform').hide();
            $('#hideform').show();
        }
        function hideform(){
            $('#etats').hide();
            $('#hideform').hide();
            $('#showform').show();
        }

        $('.actus').on('click', '.valider', function (event) {
            event.preventDefault();
            var href = $(this).attr('href');
            swal({
                title: "Êtes-vous sûr?",
                text: "Voulez vous vraiment valider cet achat",
                icon: "warning",
                buttons: true,
                buttons: ["Annuler", "Oui"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    //disable button
                    $(this).attr("disabled", true);
                    //set loading
                    $(this).html('<i class="fa fa-spinner fa-spin"></i> Chargement...');
                    window.location = href;
                }
            });
        });

    </script>
@endsection


@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="page-title">
                            Liste des achats
                            <a href="{{route('achats.create')}}" class="btn btn-default btn-xs">Ajouter</a>
                            {{-- <a href="javascript:void(0);" data-toggle="modal" data-target="#myModal" class="btn btn-default btn-xs">Upload</a> --}}
                        </h4>
                        {{-- <div class="pull-right">
                            <button onclick="showform()" id="showform"  class="btn btn-default btn-xs"><i class="fa fa-bar-chart"></i> Etat</button>
                            <button onclick="hideform()" id="hideform" class="btn btn-default btn-xs" style="display: none;"><i class="fa fa-bar-chart"></i> Fermer </button>
                        </div> --}}
                        <ol class="breadcrumb"> </ol>
                    </div>
                </div>

                <div style="display: none;" id="etats" class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="standard-modalLabel">Edition d'un etat <br>
                                <small class="text-success"><span id="lbbe"></span></small>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </h4>
                        </div>
                        <form id="formAdd" action="{{route('etats.factures')}}" class="form-horizontal" method="post" autocomplete="off">
                            @csrf
                            <div class="modal-body">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label class="col-md-12" for="password">Projet <span class="text-danger">*</span></label>
                                        <select name="project" id="project" class="form-control select2" required>
                                            <option value="0">Tous les projets</option>
                                            {{-- @foreach($projects as $project)
                                                <option value="{{$project->id}}">{{$project->libelle}}</option>
                                            @endforeach --}}
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label class="col-md-12" for="clt">Client </label>
                                        <select name="client" id="client" class="form-control select2">
                                            <option selected disabled>Selectionnez un client</option>
                                            {{-- @foreach($clients as $client)
                                                <option value="{{$client->id}}">{{$client->nom}}</option>
                                            @endforeach --}}
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label class="col-md-12" for="password">Status</label>
                                        <select name="status" id="status" class="form-control" required>
                                            <option selected disabled>Le status</option>
                                            <option value="1">Traité</option>
                                            <option value="0">En cours de traitement</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label class="col-md-12" for="password">Mode de paiement</label>
                                        <select name="mode[]" id="mode" class="form-control select2" multiple>
                                            <option disabled>Le mode de paiement</option>
                                            {{-- @foreach($modes as $mode)
                                            <option value="{{$mode->id}}">{{$mode->libelle}}</option>
                                            @endforeach --}}
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <label class="col-md-12" for="password">Date debut</label>
                                        <input type="date" name="ddebut" class="form-control">
                                    </div>

                                    <div class="col-sm-6">
                                        <label class="col-md-12" for="password">Date fin</label>
                                        <input type="date" name="dfin" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-12">
                                        <div class="radio radio-inline">
                                            <input type="radio" id="inlineRadio4" value="pdf" name="typexport" checked="">
                                            <label for="inlineRadio4"> PDF </label>
                                        </div>
                                        <div class="radio radio-inline">
                                            <input type="radio" id="inlineRadio5" value="excel" name="typexport">
                                            <label for="inlineRadio5"> EXCEL </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Etat</button>
                            </div>
                        </form>
                    </div><!-- /.modal-content -->
                </div>

                <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="standard-modalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="standard-modalLabel">Motif du rejet<br>
                                    <small class="text-success"><span id="tlbbe"></span></small>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                </h4>
                            </div>
                            <form action="{{route('achat.reject')}}" class="form-horizontal" method="post" autocomplete="off">
                                @csrf
                                <input type="hidden" id="tidsluge" name="tidsluge">
                                <div class="modal-body">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label class="col-md-12" for="password">Motif <span class="text-danger">*</span></label>
                                            <textarea name="motif" id="motif" cols="30" rows="5" class="form-control" style="resize: none"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-light" data-dismiss="modal">Annuler</button>
                                    <button type="submit" class="btn btn-primary">Rejeter</button>
                                </div>
                            </form>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div>


                <div class="row">
                    <div class="col-md-12">
                        <div class="card-box table-responsive">
                            <table id="myTable" class="table table-striped table-bordered actus">
                                <thead>
                                <tr>
                                    <th>N°</th>
                                    <th>Code</th>
                                    <th>Référence</th>
                                    <th>Date</th>
                                    <th>Fournisseur</th>
                                    <th>Mode paiement</th>
                                    <th>Montant</th>
                                    <th>Agent</th>
                                    <th>Magasin</th>
                                    <th>Status</th>
                                    <th>Date d'ajout</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($achats as $k=>$item)
                                    <tr>
                                        <td>{{$k+1}}</td>
                                        <td>{{$item->slug}}</td>
                                        <td style="font-weight: bold">
                                            {{$item->reference}}
                                            <div class="row-actions-edit">
                                                <span class="edit"> <a href="{{route('achats.show',$item->id)}}" target="_blank">Voir</a> | </span>
                                                {{-- <span class="trash"><a href="{{route('factures.download',$bon->id)}}" style="color: red">Télécharger</a></span> --}}
                                            </div>
                                        </td>
                                        <td>{{date('d/m/Y',strtotime($item->datepaiement))}}</td>
                                        <td>{{$item->fournisseur->libelle}}</td>
                                        <td>{{$item->modepaie->libelle}}</td>
                                        <td>
                                            <p title="Montant TTC"><span class="label label-table label-success">@if(is_int($item->montant)) @price($item->montant ?? 0) Fcfa @else {{$item->montant}} @endif</span></p>
                                        </td>
                                        <td>{{$item->user->name}}</td>
                                        <td>{{$item->magasin? $item->magasin->nom : '-'}}</td>
                                        <td>
                                            @if($item->status == 1)
                                                <span class="label label-table label-warning">En attente de validation</span>
                                                @if($item->user_id != Auth::user()->id)
                                                <div class="row-actions-edit">
                                                    {{-- <span class="edit"> <a href="{{route('achats.show',$item->id)}}" target="_blank">Voir</a> | </span> --}}
                                                    <a href="{{route('achat.valider',encrypt($item->id))}}" class="btn btn-success btn-xs waves-effect waves-light valider"><i class="fa fa-check-circle"></i> Valider</a>
                                                    <a href="javascript:;" data-id="{{encrypt($item->id)}}" onclick="update_status(this,`{{$item->reference}}`)" value="{{$item->id}}" class="btn btn-danger btn-xs waves-effect waves-light"><i class="fa fa-times-circle"></i> Rejeter</a>
                                                </div>
                                                @endif
                                            @else
                                                <span class="label label-table label-success">Validé</span>
                                            @endif
                                        </td>
                                       <td>{{$item->created_at->format('d/m/Y')}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div> <!-- container -->
        </div> <!-- content -->
    </div>
@endsection
