@extends('layouts.app')

@section('title')
    Bon de livraisons archivés
    @parent
@stop

@section('header_styles')
    <!-- DataTables -->
    <link href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/switchery/css/switchery.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css"/>
    <style>
        .actff{
            border-color: #99caf1;
        }
    </style>
@endsection

@section('footer_scripts')
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.js') }}"></script>

    <script src="{{ asset('assets/pages/datatables.init.js') }}"></script>

    <!-- Sweet-Alert  -->
    <script src="{{ asset('assets/pages/jquery.sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/switchery/js/switchery.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.select2').select2();

            $('#myTable').dataTable({
                language: {
                    lengthMenu: "_MENU_",
                    search: "_INPUT_",
                    searchPlaceholder: "Recherche",
                    sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                    sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    sInfoPostFix:    "",
                    sLoadingRecords: "Chargement en cours...",
                    sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
                    sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
                    sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    oPaginate: {
                        "sFirst":    "Premier",
                        "sLast":    "Dernier",
                        "sNext":    "Suivant",
                        "sPrevious": "Précédent"
                    },
                }
            });

        });

        $('.actus').on('click', '.delete', function (event) {
            event.preventDefault();
            var href = $(this).attr('href');
            swal({
                title: "Êtes-vous sûr?",
                text: "Voulez-vous vraiment supprimer ce bon",
                icon: "warning",
                buttons: true,
                buttons: ["Annuler", "Oui"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    window.location = href;
                }
            });
        });

        $('.actus').on('click', '.archive', function (event) {
            event.preventDefault();
            var href = $(this).attr('href');
            swal({
                title: "Êtes-vous sûr?",
                text: "Voulez-vous vraiment archiver ce bon",
                icon: "warning",
                buttons: true,
                buttons: ["Annuler", "Oui"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    window.location = href;
                }
            });
        });

        $('#check_all').on('click', function(e){
            if($(this).is(':checked',true)){
                $(".role_item").prop('checked', true);
            }else{
                $(".role_item").prop('checked', false);
            }
        });

        $('#sendActionGroup').on('click', function(e) {

            if($('#actionGroup').val()=='del_select'){
                var allVals = [];
                $(".role_item:checked").each(function() {
                    allVals.push($(this).attr('data-id'));
                });
                //console.log(allVals.length);
                // return false;
                if(allVals.length <=0)
                {
                    swal("Oops","Veuillez selectionnez des lignes à supprimer!","error" )
                }
                else {
                    //$("#loading").show();
                    swal({
                        title: "Êtes-vous sûr?",
                        text: "Voulez vous vraiment supprimer ces lignes",
                        icon: "warning",
                        buttons: true,
                        buttons: ["Annuler", "Oui"],
                        dangerMode: true,
                    }).then((willDelete) => {
                        if(willDelete) {
                            var valueIds = allVals.join(",");

                            $.ajax({
                                url:"{{ route('news.deletes') }}",
                                method:"GET",
                                data:{value:valueIds},
                                success:function(res){
                                    console.log(res);
                                    if(res == '1'){
                                        //$("#loading").hide();
                                        location.reload();
                                    }else{
                                        //$("#loading").hide();
                                        location.reload();
                                    }
                                }
                            })
                        }
                    });
                }
            }else{
                return false;
                //swal("Oops","Something went wrong!","error" )
            }

        });

        function update_status(el,ref){
            if(el.checked){
                var status = 1;
            }else{
                var status = 0;
            }
            //console.log(el,ref);
            $('#myModal').modal('show');

            var id = el.value;
            $("#tidsluge").val(id);
            $("#tlbbe").html(ref);
            /*var token =$("meta[property='csrf-token']").attr('content');

            $.post("{route('bl.status')}}", {_token:token, id:el.value, status:status}, function(data){
                if(data == 1){
                    document.getElementById('redaonly').readOnly = true;
                    swal("Succès!","Le bon de livraison a bien été traité","success");
                    location.reload();
                }else{
                    swal("Erreur!","Un problème est survenu","error");
                }
            });*/
        }

        function updatebc(id,ref){
            $('#myModalEdit').modal('show');
            $("#tidslugedit").val(id);
            $("#tlbbedit").html(ref);
        }

        function updatebl(id,ref){
            $('#myModalEditBL').modal('show');
            $("#tidslugeditbl").val(id);
            $("#tlbbeditbl").html(ref);
        }

        function updateref(id,ref){
            $('#myModalEditRef').modal('show');
            $("#tidslugeditref").val(id);
            $("#tlbbeditblref").html(ref);
        }

        function showform(){
            $('#etats').show();
            $('#showform').hide();
            $('#hideform').show();
        }
        function hideform(){
            $('#etats').hide();
            $('#hideform').hide();
            $('#showform').show();
        }

    </script>
@endsection


@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="page-title">
                            Bon de commande archivés
                        </h4>
                        <ol class="breadcrumb"> </ol>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <h4 class="header-title mb-3">ARCHIVES</h4>
                        @foreach ($bons as $bon)
                            <div class="card-box mb-1 {{$q==$bon->updated_at->format('Y') ?'actff' : ''}}">
                                <div class="card-body">
                                    <a href="{{route('bl.indexarchiveitem',$bon->updated_at->format('Y'))}}">
                                        <div class="p-3">
                                            <h4 class="mt-0 mb-2 font-16 text-center">{{$bon->updated_at->format('Y')}}</h4>
                                        </div>
                                    </a>
                                </div>
                            </div>

                        @endforeach

                    </div>
                    <div class="col-md-10">
                        <h4 class="header-title mb-3">Liste des archives <span class="text-primary">{{$q? $q : ''}}</span></h4>
                        <div class="card-box table-responsive">
                            <table id="myTable" class="table table-striped table-bordered actus">
                                <thead>
                                <tr>
                                    <th>Ref.No.</th>
                                    <th>Nom</th>
                                    <th>Projet</th>
                                    <th>Type</th>
                                    <th>Status</th>
                                    <th>Fiche</th>
                                    <th>Date d'ajout</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($listbons as $k=>$bon)
                                    <tr>
                                        <td>{{$bon->reference ?? 'N/A'}}
                                            {{-- <div class="row-actions-edit">
                                                <span class="edit"> <a href="#" onclick="updateref('{{$bon->id}}','{{$bon->slug}}')"><i class="fa fa-edit"></i></a></span>
                                            </div> --}}
                                        </td>
                                        <td style="font-weight: bold">
                                            {{$bon->slug}}
                                            <div class="row-actions-edit">
                                                <span class="edit"> <a href="{{route('bl.show',$bon->id)}}">Voir</a></span>
                                                @if($bon->etat ==0)
                                                    | <span class="trash"><a href="{{route('bl.item.bl',$bon->id)}}" style="color: red">Bon de livraison</a></span>
                                                @endif
                                            </div>
                                        </td>
                                        <td>{{$bon->project->libelle}}</td>
                                        <td>{{$bon->type==1 ? 'Electricité' : 'Plomberie'}}</td>
                                        <td>
                                            @if($bon->etat == 0)
                                                {{--<span class="label label-danger">En cours</span>--}}
                                                <button type="button" class="btn btn-danger btn-sm waves-effect waves-light" onclick="update_status(this,'{{$bon->slug}}')" value="{{$bon->id}}"><i class="fa fa-edit"></i> Non traité</button>
                                                {{--<input id="redaonly" type="checkbox"  data-plugin="switchery" data-color="#81c868" data-size="small" title="En cours de traitement" />--}}
                                            @else
                                                {{--<span class="label label-success">Terminer</span>
                                                <input type="checkbox" checked data-plugin="switchery" data-color="#81c868" data-size="small" readonly/>--}}
                                                <button type="button" class="btn btn-success btn-sm waves-effect waves-light">Traité</button>
                                            @endif

                                        </td>
                                        <td>
                                            <a title="Fiche du bon de commande" href="{{asset('bc/'.$bon->fichier)}}" target="_blank"><i class="fa fa-file-pdf-o"></i> Fiche BC</a>
                                            {{-- <span class="row-actions-edit"><a href="#" onclick="updatebc('{{$bon->id}}','{{$bon->slug}}')" style="color: red"><i class="fa fa-trash"></i></a></span> --}}

                                            @if($bon->etat ==1)
                                                <br>
                                                <a title="Fiche du bon de livraison" href="{{asset('bc/'.$bon->file_bl)}}" target="_blank" class="text-success" style="padding-top: 6px"><i class="fa fa-file-pdf-o"></i> Fiche BL</a>
                                                {{-- <span class="row-actions-edit"><a href="#" onclick="updatebl('{{$bon->id}}','{{$bon->slug}}')" style="color: red"><i class="fa fa-trash"></i></a></span> --}}
                                            @endif
                                        </td>
                                        <td>{{$bon->created_at->format('d/m/Y')}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div> <!-- container -->
        </div> <!-- content -->
    </div>
@endsection
