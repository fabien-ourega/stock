<html>
<style>
    body{
        font-size: 10px;
        margin: 1.5px;
    }
    table {
        font-size: 11px;
    }

    .td{
        border: 1px solid #000000;
        padding-left: 5px;
    }
    .txt-center{
        text-align: center;
    }
    .txt-right{
        text-align: right;
    }
    #resultscol td{
        border: 1px solid #000000;
        padding: 3px;
    }

    #fin ul li{
        list-style: none;
        text-align: left;
    }

    .custom-control-label::before,
    .custom-control-label::after {
        top: 0.1rem !important;
        left: -2rem !important;
        width: 1.25rem !important;
        height: 1.25rem !important;
    }

    footer {
        position: fixed;
        bottom: 0cm;
        left: 0cm;
        right: 0cm;
        height: 2cm;

        /** Extra personal styles **/
        color: #000;
        font-size: 10px;
        text-align: center;
    }

</style>
<body>
<div>
        <table border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td>
                    <div class="col">
                        <img src="{{asset('assets/images/logo_dark.png')}}" data-holder-rendered="true" height="50px" />

                    </div>
                </td>

            </tr>
        </table>
    <br>
    <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td align="center">
                <p style="font-size: 18px;font-weight: bold">BON DE LIVRAISON</p>
            </td>
        </tr>

    </table>
    <br>
        <table border="0" cellspacing="0" cellpadding="0" align="center" width="100%">
            <tr>

                <td width="60%" style="font-size: 11px">
                    <div class="address">
                        <div style="float: left; margin-right: 10px">
                            <p style="font-size: 14px;">
                                Date: {{$bl->created_at->format('d/m/Y')}}<br><br>
                                REF : <strong>{{$bl->reference ? $bl->reference : ''}}</strong><br><br>
                                N°BL :  {{$bl->slug}}<br><br>
                            </p>
                        </div>
                    </div>
                </td>
                <td style="text-align: right">
                    <div class="col company-details">
                        <p style="font-size: 14px;">
                            <strong>{{$bl->client ? $bl->client->nom : ''}}</strong><br>
                            {{$bl->client ? $bl->client->adresse : ''}}<br>
                            {{$bl->client ? 'Contact: '.$bl->client->contact : ''}}
                        </p>
                    </div>
                </td>
            </tr>
        </table>
        <br>
        <br>
        <table style="border: 1px solid #000" cellspacing="0" cellpadding="0" align="center" width="100%">
            <tr style="background-color: #e1e1e8">
                <td width="25%" class="td"><strong>DESIGNATION</strong></td>
                <td width="10%" class="td"><strong>UNITE</strong></td>
                <td width="15%" class="td"><strong>QUANTITES COMMANDEES</strong></td>
                <td width="15%" class="td"><strong>QUANTITES LIVREES</strong></td>
                <td width="15%" class="td"><strong>QUANTITES RESTANTES</strong></td>
                <td width="30%" class="td"><strong>OBSERVATION</strong></td>
            </tr>

            @foreach($listbls as $k=>$listbl)
                <tr class="">
                    <td class="td">{{$listbl->produits->nom}}</td>
                    @php $unit =\App\Typeproduit::find($listbl->produits->type_id) @endphp
                    <td class="td">{{$unit ? $unit->libelle :''}}</td>
                    <td class="td">{{$listbl->qtecmd}}</td>
                    <td class="td">{{$listbl->qte}}</td>
                    <td class="td">{{$listbl->qterestant ? $listbl->qterestant : 0}}</td>
                    <td class="td"></td>
                </tr>
            @endforeach

        </table>

    <br>
    <table id="fin" style="border: 1px solid #000;" cellspacing="0" cellpadding="0" width="100%">
        <tr style="text-align: center">
            <td class="td"><strong>Visa du Client et observations éventuelles</strong></td>
            <td class="td"><strong>Visa du Fournisseur</strong></td>
        </tr>
        <tr>
            <td class="td" style="text-align: center" width="75%">
                <table id="fin" style="" cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                        <td style="text-align: center;border-right: 1px solid #000000;height: 100px;" width="50%">
                            Reçu le ........................................................
                        </td>
                        <td style="text-align: center" width="50%">
                            Réserves sur la livraison
                        </td>
                    </tr>
                </table>
            </td>
            <td class="td" style="text-align: center" width="25%">
                <table id="fin" style="" cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                        <td style="text-align: center" width="100%">
                            Livré le ........................................................
                        </td>
                    </tr>
                </table>
            </td>

        </tr>
    </table>
</div>
<footer>
    Cocody Angré, 8ème Tranche, Résidence Pacific Villa 73, 01 BP 473 Abidjan 01 <br>
    Tél. (225)06 32 86 36 / (225) 59 59 09 91, RC : CI-ABJ-2018-B-14915, CC : 18 29 457 U, Capital Social : 10.000.000 FCFA, Régime d'imposition : Réel simplifié
</footer>
</body>
</html>
