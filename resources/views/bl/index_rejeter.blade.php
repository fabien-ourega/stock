@extends('layouts.app')

@section('title')
    Bon de commande rejeter
    @parent
@stop

@section('header_styles')
    <!-- DataTables -->
    <link href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/switchery/css/switchery.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('footer_scripts')
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.js') }}"></script>

    <script src="{{ asset('assets/pages/datatables.init.js') }}"></script>

    <!-- Sweet-Alert  -->
    <script src="{{ asset('assets/pages/jquery.sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/switchery/js/switchery.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.select2').select2();

            $('#myTable').dataTable({
                language: {
                    lengthMenu: "_MENU_",
                    search: "_INPUT_",
                    searchPlaceholder: "Recherche",
                    sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                    sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    sInfoPostFix:    "",
                    sLoadingRecords: "Chargement en cours...",
                    sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
                    sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
                    sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    oPaginate: {
                        "sFirst":    "Premier",
                        "sLast":    "Dernier",
                        "sNext":    "Suivant",
                        "sPrevious": "Précédent"
                    },
                }
            });

        });

        $('.actus').on('click', '.delete', function (event) {
            event.preventDefault();
            var href = $(this).attr('href');
            swal({
                title: "Êtes-vous sûr?",
                text: "Voulez vous vraiment supprimer ce bon",
                icon: "warning",
                buttons: true,
                buttons: ["Annuler", "Oui"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    window.location = href;
                }
            });
        });

        $('.actus').on('click', '.valider', function (event) {
            event.preventDefault();
            var href = $(this).attr('href');
            swal({
                title: "Êtes-vous sûr?",
                text: "Voulez vous vraiment valider ce bon",
                icon: "warning",
                buttons: true,
                buttons: ["Annuler", "Oui"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    //disable button
                    $(this).attr("disabled", true);
                    //set loading
                    $(this).html('<i class="fa fa-spinner fa-spin"></i> Chargement...');
                    window.location = href;
                }
            });
        });



        $('#check_all').on('click', function(e){
            if($(this).is(':checked',true)){
                $(".role_item").prop('checked', true);
            }else{
                $(".role_item").prop('checked', false);
            }
        });

        $('#sendActionGroup').on('click', function(e) {

            if($('#actionGroup').val()=='del_select'){
                var allVals = [];
                $(".role_item:checked").each(function() {
                    allVals.push($(this).attr('data-id'));
                });
                //console.log(allVals.length);
                // return false;
                if(allVals.length <=0)
                {
                    swal("Oops","Veuillez selectionnez des lignes à supprimer!","error" )
                }
                else {
                    //$("#loading").show();
                    swal({
                        title: "Êtes-vous sûr?",
                        text: "Voulez vous vraiment supprimer ces lignes",
                        icon: "warning",
                        buttons: true,
                        buttons: ["Annuler", "Oui"],
                        dangerMode: true,
                    }).then((willDelete) => {
                        if(willDelete) {
                            var valueIds = allVals.join(",");

                            $.ajax({
                                url:"{{ route('news.deletes') }}",
                                method:"GET",
                                data:{value:valueIds},
                                success:function(res){
                                    console.log(res);
                                    if(res == '1'){
                                        //$("#loading").hide();
                                        location.reload();
                                    }else{
                                        //$("#loading").hide();
                                        location.reload();
                                    }
                                }
                            })
                        }
                    });
                }
            }else{
                return false;
                //swal("Oops","Something went wrong!","error" )
            }

        });

        function updatebc(id,ref){
            $('#myModalEdit').modal('show');
            $("#tidslugedit").val(id);
            $("#tlbbedit").html(ref);
        }

        function updatebl(id,ref){
            $('#myModalEditBL').modal('show');
            $("#tidslugeditbl").val(id);
            $("#tlbbeditbl").html(ref);
        }

        function updateref(id,ref){
            $('#myModalEditRef').modal('show');
            $("#tidslugeditref").val(id);
            $("#tlbbeditblref").html(ref);
        }

        function update_status(el,ref){
            //console.log(el,ref);
            $('#myModal').modal('show');
            var id = $(el).attr('data-id');
            $("#tidsluge").val(id);
            $("#tlbbe").html(ref);
        }

        function showform(){
            $('#etats').show();
            $('#showform').hide();
            $('#hideform').show();
        }
        function hideform(){
            $('#etats').hide();
            $('#hideform').hide();
            $('#showform').show();
        }

    </script>
@endsection


@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="page-title">
                            Bon de commande rejeter
                            <a href="{{route('bl.create')}}" class="btn btn-default btn-xs">Ajouter</a>
                        </h4>
                        <ol class="breadcrumb"> </ol>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card-box table-responsive">
                            <table id="myTable" class="table table-striped table-bordered actus">
                                <thead>
                                <tr>
                                    <th>Ref.No.</th>
                                    <th>Nom</th>
                                    <th>Projet</th>
                                    <th>Type</th>
                                    <th>Status</th>
                                    <th>Fiche</th>
                                    <th>Date d'ajout</th>
                                    <th>Motif</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($bons as $k=>$bon)
                                    <tr>
                                        <td>{{$bon->reference}} </td>
                                        <td style="font-weight: bold">
                                            {{$bon->slug}}
                                            <div class="row-actions-edit">
                                                <span class="edit"> <a href="{{route('bl.show',$bon->id)}}">Voir</a></span>
                                                @if (in_array('del_bc', Session::get('permissions')))
                                                    | <span class="trash"><a href="{{route('bl.destroy',$bon->id)}}" class="text-danger delete">Supprimer</a></span>
                                                @endif
                                            </div>
                                        </td>
                                        <td>{{$bon->project->libelle}}</td>
                                        <td>{{$bon->type==1 ? 'Electricité' : 'Plomberie'}}</td>
                                        <td>
                                            <button type="button" class="btn btn-danger btn-sm waves-effect waves-light">Rejeter</button>
                                        </td>
                                        <td>
                                            <a title="Fiche du bon de commande" href="{{asset('bc/'.$bon->fichier)}}" target="_blank"><i class="fa fa-file-pdf-o"></i> Fiche BC</a>
                                        </td>
                                        <td>{{$bon->created_at->format('d/m/Y')}}</td>
                                        <td>
                                            {{$bon->motif}}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div> <!-- container -->
        </div> <!-- content -->
    </div>
@endsection
