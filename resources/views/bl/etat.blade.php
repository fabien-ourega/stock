<div style="display: none;" id="etats" class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="standard-modalLabel">Edition d'un etat <br>
                <small class="text-success"><span id="lbbe"></span></small>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </h4>
        </div>
        <form id="formAdd" action="{{route('etats.bc')}}" class="form-horizontal" method="post" autocomplete="off">
            @csrf
            <div class="modal-body">
                {{--<div class="form-group">
                    <div class="col-sm-12">
                        <label class="col-md-12" for="password">Bon</label>
                        <select name="bon" id="bon" class="form-control" required>
                            <option selected disabled>Le Bon</option>
                            <option value="cmd">Bon de commande</option>
                            <option value="lvd">Bon de livraison</option>
                        </select>
                    </div>
                </div>--}}

                <div class="form-group">
                    <div class="col-sm-12">
                        <label class="col-md-12" for="password">Projet</label>
                        <select name="project" id="project" class="form-control select2" required>
                            <option value="0">Tous les projets</option>
                            @foreach($projects as $project)
                                <option value="{{$project->id}}">{{$project->libelle}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-12">
                        <label class="col-md-12">Type </label>
                        <select name="type" id="type" class="form-control">
                            <option disabled selected>Choisir un type</option>
                            <option value="1">Electricité</option>
                            <option value="2">Plomberie</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-12">
                        <label class="col-md-12" for="password">Status</label>
                        <select name="status" id="status" class="form-control" required>
                            <option selected disabled>Le status</option>
                            <option value="1">Traité</option>
                            <option value="0">Non traité</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-6">
                        <label class="col-md-12" for="password">Date debut</label>
                        <input type="date" name="ddebut" class="form-control">
                    </div>

                    <div class="col-sm-6">
                        <label class="col-md-12" for="password">Date fin</label>
                        <input type="date" name="dfin" class="form-control">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-12">
                        <label class="col-md-12" for="password">Impression</label>
                        <select name="imprim" id="imprim" class="form-control" required>
                            <option value="1" selected>Imprimer l'État</option>
                            <option value="2">Imprimer les factures</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <div class="radio radio-inline">
                            <input type="radio" id="inlineRadio4" value="pdf" name="typexport" checked="">
                            <label for="inlineRadio4"> PDF </label>
                        </div>
                        <div class="radio radio-inline" id="iputexel">
                            <input type="radio" id="inlineRadio5" value="excel" name="typexport">
                            <label for="inlineRadio5"> EXCEL </label>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Etat</button>
            </div>
        </form>
    </div><!-- /.modal-content -->
</div>


<script src="{{ url('assets/js/jquery.min.js') }}"></script>
<script>
    $(document).ready(function () {
        //On change le type de bon
        $('#imprim').change(function () {
            var bon = $(this).val();
            // console.log(bon);
            if (bon == '2') {
                $('#iputexel').hide();
            } else {
                $('#iputexel').show();
            }
        });
    });
</script>
