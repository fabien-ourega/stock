<html>
<style>
    body{
        font-size: 10px;
        margin: 1.5px;
    }
    table {
        font-size: 11px;
    }

    .td{
        border: 1px solid #000000;
        padding-left: 5px;
    }
    .txt-center{
        text-align: center;
    }
    .txt-right{
        text-align: right;
    }
    #resultscol td{
        border: 1px solid #000000;
        padding: 3px;
    }

    #fin ul li{
        list-style: none;
        text-align: left;
    }

    .custom-control-label::before,
    .custom-control-label::after {
        top: 0.1rem !important;
        left: -2rem !important;
        width: 1.25rem !important;
        height: 1.25rem !important;
    }

    footer {
        position: fixed;
        bottom: 0cm;
        left: 0cm;
        right: 0cm;
        height: 2cm;

        /** Extra personal styles **/
        color: #000;
        font-size: 10px;
        text-align: center;
    }

</style>
<body>
<div>
        <table border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td>
                    <div class="col">
                        <img src="{{asset('assets/images/logo_dark.png')}}" data-holder-rendered="true" height="50px" />

                    </div>
                </td>

            </tr>
        </table>
    <br>
    <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td align="center">
                <p style="font-size: 18px;font-weight: bold">BON DE COMMANDE</p>
            </td>
        </tr>

    </table>
    <br>
        <table border="0" cellspacing="0" cellpadding="0" align="center" width="100%" >
            <tr>

                <td width="75%">
                    <div class="col company-details">
                        <strong>{{$bl->client ? $bl->client->nom : ''}}</strong><br>
                        {{$bl->client ? $bl->client->adresse : ''}}<br>
                        {{$bl->client ? 'Contact: '.$bl->client->contact : ''}}
                    </div>
                </td>
                <td>
                    <div class="address">
                        Date: {{$bl->created_at->format('d/m/Y')}}<br>
                        N°BL :  <strong>{{$bl->slug}}</strong><br>
                        REF :  <strong>{{$bl->reference}}</strong>
                    </div>
                </td>
            </tr>
        </table>
        <br/>
        <br>

        <table style="border: 1px solid #000" cellspacing="0" cellpadding="0" align="center" width="100%">
            <tr style="background-color: #e1e1e8">
                <td width="20%" class="td"><strong>DESIGNATION</strong></td>
                <td width="20%" class="td"><strong>UNITE</strong></td>
                <td width="20%" class="td"><strong>QUANTITES COMMANDEES</strong></td>
                <td width="20%" class="td"><strong>QUANTITES LIVREES</strong></td>
                <td width="20%" class="td"><strong>QUANTITES RESTANTES A LIVRER</strong></td>
            </tr>

            @foreach($listbls as $k=>$listbl)
                <tr class="">
                    <td class="td">{{$listbl->produits->nom}}</td>
                    @php $unit =\App\Typeproduit::find($listbl->produits->type_id) @endphp
                    <td class="td">{{$unit ? $unit->libelle :''}}</td>
                    <td class="td">{{$listbl->qtecmd}}</td>
                    <td class="td">{{$listbl->qte}}</td>
                    <td class="td">{{$listbl->qterestant ? $listbl->qterestant : 0}}</td>
                </tr>
            @endforeach

        </table>
    <br>
</div>
<footer>
    Cocody Angré, 8ème Tranche, Résidence Pacific Villa 73, 01 BP 473 Abidjan 01 <br>
    Tél. (225)06 32 86 36 / (225) 59 59 09 91, RC : CI-ABJ-2018-B-14915, CC : 18 29 457 U, Capital Social : 10.000.000 FCFA, Régime d'imposition : Réel simplifié
</footer>
</body>
</html>
