@extends('layouts.app')

@section('title')
    Bon de livraisons
    @parent
@stop

@section('header_styles')
    <!-- DataTables -->
    <link href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
    <style>
        tr.spacend td{
            padding-bottom: 3em!important;
        }
    </style>
@endsection

@section('footer_scripts')
    <!-- Sweet-Alert  -->
    <script src="{{ asset('assets/pages/jquery.sweetalert.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
        });
    </script>
@endsection


@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="page-title">
                            Bon de livraison
                        </h4>
                        <ol class="breadcrumb"> </ol>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <!-- <div class="panel-heading">
                                <h4>Invoice</h4>
                            </div> -->
                            <div class="panel-body">
                                <div class="clearfix">
                                    <div class="pull-left">
                                        <h4 class="text-right"><img src="{{asset('assets/images/logo_dark.png')}}" alt="velonic" class="img-responsive" width="20%"></h4>

                                    </div>
                                    <div class="pull-right">
                                        <h4>BL :   <br>
                                            <strong>{{$bl->slug}}</strong><br>
                                            {{$bl->reference ? $bl->reference : ''}}
                                        </h4>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <p class="text-center" style="font-size: 1.3em;font-weight: bold;margin-bottom: 40px">BON DE LIVRAISON</p>

                                    <div class="col-md-12">

                                        <div class="pull-left m-t-30">
                                            <address>
                                                <strong>{{$bl->client ? $bl->client->nom : ''}}</strong><br>
                                                {{$bl->client ? $bl->client->adresse : ''}}<br>
                                                {{$bl->client ? 'Contact: '.$bl->client->contact : ''}}
                                            </address>
                                        </div>
                                        <div class="pull-right m-t-30">
                                            <p><strong>Date: </strong> {{$bl->created_at->format('d/m/Y')}}</p>
                                            <p class="m-t-10"><strong>Référence: </strong> {{$bl->slug}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="m-h-50"></div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive table-bordered">
                                            <table class="table m-t-30">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Désignation</th>
                                                        <th>Unité</th>
                                                        <th>Qte commandée</th>
                                                        <th>Qte livrée</th>
                                                        <th>Qte à livrer</th>
                                                        <th>Observations</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($listbls as $k=>$listbl)
                                                    {{--<tr class="{{count($listbls) == $k+1 ? 'spacend' : ''}}">--}}
                                                <tr>
                                                    <td>{{$k+1}}</td>
                                                    <td>{{$listbl->produits->nom}}</td>
                                                    @php $unit =\App\Typeproduit::find($listbl->produits->type_id) @endphp
                                                    <td>{{$unit ? $unit->libelle :''}}</td>
                                                    <td>{{$listbl->qtecmd}}</td>
                                                    <td>{{$listbl->qte}}</td>
                                                    <td>{{$listbl->qterestant ? $listbl->qterestant : 0}}</td>
                                                    <td></td>
                                                </tr>
                                                @endforeach
                                                </tbody>
                                            </table>

                                            <table class="table m-t-30 table-bordered">
                                                <tbody>
                                                    <tr style="text-align: center">
                                                        <td colspan="3">
                                                            Visa du Client et observations éventuelles
                                                        </td>
                                                        <td colspan="2">
                                                            Visa du Fournisseur
                                                        </td>

                                                    </tr>
                                                    <tr style="text-align: center;height: 120px">
                                                        <td colspan="">
                                                            Reçu le .....................................
                                                        </td>
                                                        <td colspan="2">
                                                            Réserves sur la livraison
                                                        </td>
                                                        <td colspan="2">
                                                            Livré le .....................................
                                                        </td>

                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <hr>
                                <div class="hidden-print">
                                    <div class="pull-right">
                                        <a href="{{route('bl.download',$bl->id)}}" class="btn btn-inverse waves-effect waves-light"><i class="fa fa-download"></i> Télécharger</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </div> <!-- container -->
        </div> <!-- content -->
    </div>
@endsection
