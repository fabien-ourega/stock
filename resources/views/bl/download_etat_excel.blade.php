<html>
<style>
    body{
        font-size: 10px;
        margin: 1.5px;
    }
    table {
        font-size: 11px;
    }

    .td{
        border: 1px solid #000000;
        padding-left: 5px;
        padding: 10px;
    }
    .tdd{
        padding: 3px;
        border-bottom: 1px solid #e2e2e2;
    }
    .txt-center{
        text-align: center;
    }
    .txt-right{
        text-align: right;
    }
    #resultscol td{
        border: 1px solid #000000;
        padding: 3px;
    }

    #fin ul li{
        list-style: none;
        text-align: left;
    }

    .custom-control-label::before,
    .custom-control-label::after {
        top: 0.1rem !important;
        left: -2rem !important;
        width: 1.25rem !important;
        height: 1.25rem !important;
    }

    footer {
        position: fixed;
        bottom: 0cm;
        left: 0cm;
        right: 0cm;
        height: 2cm;

        /** Extra personal styles **/
        color: #000;
        font-size: 10px;
        text-align: center;
    }

</style>
<body>
<div>

    <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td align="center">
                <p style="font-size: 18px;font-weight: bold">ETAT DES BONS DE COMMANDE</p>
                @if(isset($date) and isset($date2))
                    <span style="font-size: 14px">Du {{date('d/m/Y',strtotime($date))}} au {{date('d/m/Y',strtotime($date2))}}</span>
                @endif
            </td>
        </tr>

    </table>
    <br>
    <table style="" cellspacing="0" cellpadding="0" align="center" width="100%">
        <tr style="background-color: #e1e1e8">
            <td class="td"><strong>NUM</strong></td>
            <td class="td"><strong>REF</strong></td>
            <td class="td"><strong>NOM</strong></td>
            <td class="td"><strong>PROJET</strong></td>
            <td class="td"><strong>TYPE</strong></td>
            <td class="td"><strong>STATUT</strong></td>
            {{--<td class="td"><strong>FICHE</strong></td>--}}
            <td class="td"><strong>DATE D AJOUT</strong></td>
            <td class="td"><strong>MONTANT HT</strong></td>
            <td class="td"><strong>MONTANT TTC</strong></td>
        </tr>
        @php $sstotal = 0 @endphp
        @foreach($datas as $k=>$data)
            @php $listbls = \App\Blitem::where('bl_id',$data->id)->with('produits')->get();
                $totals = (new \App\Http\Controllers\FacturesController())->getMontantTotal($data->id,'all');
            @endphp
            <tr class="">
                <td class="td">{{$k+1}}</td>
                <td class="td">{{$data->reference}}</td>
                <td class="td"><p><strong>{{$data->slug}}</strong></p>
                    @foreach($listbls as $k=>$listbl)
                    <li>Code: {{$listbl->produits ? $listbl->produits->code : '-'}}  || Nom: {{$listbl->produits ? $listbl->produits->nom : '-'}}  || Qte: {{$listbl->qte}} || Prix: @price($listbl->price)</li>
                    @endforeach
                </td>
                <td class="td">{{$data->project->libelle}}</td>
                <td class="td">{{$data->type==1 ? 'Electricité' : 'Plomberie'}}</td>
                <td class="td">{{$data->etat==1 ? "TRAITE" : "NON TRAITE"}}</td>
                {{--<td class="td">{{asset('bc/'.$data->fichier)}}</td>--}}
                <td class="td">{{$data->created_at->format('d-m-Y')}}</td>
                <td class="td">@price($totals['ht'])</td>
                <td class="td">@price($totals['ttc'])</td>
            </tr>
        @endforeach

    </table>
    <br>
</div>

</body>
</html>
