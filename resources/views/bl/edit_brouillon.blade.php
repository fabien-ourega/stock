@extends('layouts.app')

@section('title')
    Editer un bon de commande
    @parent
@stop

@section('header_styles')
    {{--<link href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>--}}
    <link href="{{ asset('assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css"/>
    <style>
        .metable {
            margin: 30px 0;
            border: 1px solid green;
        }
    </style>
@endsection

@section('footer_scripts')
    <!-- Sweet-Alert  -->
    <script src="{{ asset('assets/pages/jquery.sweetalert.min.js') }}"></script>
    {{--<script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>--}}
    <!--form validation init-->
    <script src="{{ asset('assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js') }}" type="text/javascript"></script>

    <script src="{{ asset('assets/plugins/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-select/js/bootstrap-select.min.js') }}"></script>

    <script>
        var valueSelect = '',
            inputs = [],
            arr = [],
            x = 0;

        $(document).ready(function () {
            $('.select2').select2();

            getPdtbyPjt();

            @foreach($dats as $data)
                @php $listbl = explode(';',$data);
                    $lign = \App\Bstock::where('id',$listbl[0])->with(['produit'=>function($item){$item->with('type');}])->has('produit')->first();
                @endphp
                arr = {id:'{{$listbl[0]}}' , typdt:'{{$lign?$lign->produit->type->libelle:'-'}}', libelle:'{{$listbl[1]}}' ,qtecmde:'{{$listbl[2]}}', qte:'{{$listbl[3]}}',qtereste:'{{$listbl[4]}}'};
                inputs.push(arr);
            @endforeach

            showhtml();
        });

        $('#pjt').change(function() {
            $('#data_session').empty();
            $('#qte').val("");
            inputs = [];
            arr = [],
            x = 0;

            getPdtbyPjt();

        });

        function getPdtbyPjt() {
            var idarrond = $('#pjt :selected').val();
            var url ="{{ url('get/'.'id'.'/produits')}}";
            url=url.replace('id',idarrond);
            $.get(url, function (data) {
                //console.log(data);
                optionData ='';
                optionData+='<option value="">Choisir un produit</option>';
                for (var i = 0; i < data.length; i++){
                    optionData+='<option value="'+data[i].id+'" data-typepdt="'+data[i].produit.type.libelle+'" data-libelle="'+data[i].produit.nom+'">'+data[i].produit.nom+'</option>';
                }
                $('#pdt').html(optionData);
            });
        }

        function getQteDispo(quantite,idbstock,idpjt) {

            var url ="{{ url('get/'.'valmontant'.'/'.'attribut'.'/'.'projt'.'/pdt/dispo')}}";
            url=url.replace('valmontant',quantite);
            url=url.replace('attribut',idbstock);
            url=url.replace('projt',idpjt);

            return $.ajax({url:url, type:'GET'});
        }

        function add_element_to_array(){
            var qtite = $("#qte").val();
            var qtiterest = 0;
            var produits = $("#pdt").val();
            //console.log(produits);

            if(qtite == "" || qtite<1){
                swal("Oups!", "Veuillez renseigner la quantité", "error");
                return false ;
            }else if(produits == ""){
                swal("Oups!", "Veuillez choisir un produit ", "error");
                return false ;
            } else{
                var valueSelect = $('#pdt');
                var selectId = valueSelect.val(),
                    pjtid = $('#pjt').val(),
                    selectTypeLibell = valueSelect.find(':selected').attr('data-typepdt'),
                    selectLibell = valueSelect.find(':selected').attr('data-libelle');

                //console.log(selectId,selectLibell);
                var request = getQteDispo(qtite,selectId,pjtid);

                $.when(request).done(function(data) {
                    //console.log(data);
                    if(data == "error"){
                        swal("Oups!", "Une erreur s'est produit, veuillez recommencez ", "error");
                        return false ;
                    }else if (data == 'ok'){
                        arr = {id:selectId , typdt:selectTypeLibell ,libelle:selectLibell ,qtecmde:qtite, qte:qtite,qtereste:qtiterest};
                        inputs.push(arr);
                        //console.log(arr);
                        showhtml();
                    }else{
                        //il manque
                        swal("Oups!", "Quantité disponible : "+data , "error");
                        var reste = qtite - data ;
                        //console.log(qtite,reste, data);
                        if(reste>0){
                            arr = {id:selectId , typdt:selectTypeLibell ,libelle:selectLibell ,qtecmde:qtite, qte:data,qtereste:reste};
                            inputs.push(arr);
                            showhtml();
                        }else{
                            return false ;
                        }
                    }
                });
            }
        }

        function showhtml() {
            $("#data_session").empty();
            var table = "<tbody>";
            //console.log(inputs);
            table += '<tr>';
            table += '<td class="border-b">NUM</td>';
            table += '<td class="border-b text-lg font-medium">PRODUIT</td>';
            table += '<td class="border-b text-lg font-medium">TYPE</td>';
            table += '<td class="border-b text-lg font-medium">QUANTITES COMMANDEES</td>';
            table += '<td class="border-b text-lg font-medium">QUANTITES A LIVRER</td>';
            table += '<td class="border-b text-lg font-medium">QUANTITES RESTANTE</td>';
            table += '<td class="border-b">ACTION</td>';
            table += '</tr>';

            for (var i = 0; i < inputs.length; i++) {
                var y = i + 1;

                table += '<tr data-row-id='+i+'>';
                table += '<td class="border-b">' + y + '</td>';
                table += '<td class="border-b">' + inputs[i].libelle + '</td>';
                table += '<td class="border-b">' + inputs[i].typdt + '</td>';
                table += '<td class="border-b">' + inputs[i].qtecmde + '</td>';
                table += '<td class="border-b">' + inputs[i].qte + '</td>';
                table += '<td class="border-b">' + inputs[i].qtereste + '</td>';
                table += '<td class="border-b"><a href="#" onclick="deleteSession('+i+')" class="inline-block text-1xl text-orange-500 mx-2" title="Supprimer la ligne"><i data-feather="trash" class="w-4 h-4 mr-2"></i> Supprimer</a></div>';
                table +='<input type="hidden" value="' + inputs[i].id + ';' + inputs[i].libelle + ';' + inputs[i].qtecmde + ';' + inputs[i].qte +';' + inputs[i].qtereste +'" name="datasession[]">';
                table += '</tr>';
            };

            /*table += '<tr>';
            table += '<td class="border-b"></td>';
            table += '<td class="border-b text-lg font-medium">Montant Total</td>';
            table += '<td class="border-b text-lg font-medium" id="mttotal"></td>';
            table += '<td class="border-b"></td>';
            table += '</tr>';*/

            table += '</tbody>';

            $("#data_session").append(table);
        }

        function deleteSession(id) {
            inputs.splice(id,1);
            showhtml();
        }

        function submitForm() {
            event.preventDefault();
            if($('#libell').val()==''){
                swal("Oups!", "Veuillez renseigner le champ Nom.");
            }else if($('#pjt').val()==''){
                swal("Oups!", "Veuillez renseigner le champ Projet.");
            }else{
                var href = $(this).attr('href');
                swal({
                    title: "Êtes-vous sûr?",
                    text: "Vous êtes sur le point de faire un bon de livraison ! Cette action est irréversible ? ",
                    icon: "warning",
                    buttons: true,
                    buttons: ["Annuler", "Oui"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        document.getElementById('addbtn').disabled = true;
                        document.getElementById('addappelfonds').submit();
                    }
                });
            }

        }

    </script>
@endsection


@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">

                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Editer un bon de commande</h4>
                        <ol class="breadcrumb"> </ol>
                    </div>
                </div>

                <div class="row">
                    <form enctype="multipart/form-data" id="addappelfonds" class="form-horizontal" method="post" action="{{route('bl.store')}}" role="form">
                        @csrf
                        <div class="col-md-12">
                            <div class="card-box">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="col-md-12">Reference <span class="text-danger">*</span></label>
                                                <input type="text" value="{{$bl->reference}}" name="reference" id="reference" class="form-control" required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="col-md-12">Client <span class="text-danger">*</span></label>
                                                <select name="clientid" id="clientid" class="form-control select2" required>
                                                    <option value="" selected>Choisir le client</option>
                                                    @foreach($clients as $client)
                                                        <option value="{{$client->id}}" {{$bl->client_id == $client->id ? "selected" : ""}}>{{$client->nom}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="col-md-12">Projet <span class="text-danger">*</span></label>
                                                <select name="pjt" id="pjt" class="form-control select2" required>
                                                    <option value="" selected>Choisir le projet</option>
                                                    @foreach($projects as $project)
                                                        <option value="{{$project->id}}" {{$bl->pjt_id == $project->id ? "selected" : ""}}>{{$project->libelle}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="col-md-12">Type <span class="text-danger">*</span></label>
                                                <select name="type" id="type" class="form-control" required>
                                                    <option value="" disabled>Choisir un type</option>
                                                    <option value="1" {{$bl->type == 1 ? "selected" : ""}}>Electricité</option>
                                                    <option value="2" {{$bl->type == 2 ? "selected" : ""}}>Plomberie</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="col-md-12">Fichier <span class="text-danger">*</span></label>
                                                <input type="file" name="file" id="file" class="form-control" value="">
                                                <p class="text-helper col-md-12">Voir le fichier <a href="{{asset('bc/'.$bl->fichier)}}" target="_blank"><i class="fa fa-file"></i> </a></p>
                                            </div>
                                        </div>

                                        <hr>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="col-sm-12">
                                                    <label class="col-md-12">Produits <span class="text-danger">*</span></label>
                                                    <select name="pdt" id="pdt" class="form-control select2">

                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="col-md-12">
                                                    <label class="col-md-12" for="qte">Quantités commandées <span class="text-danger">*</span></label>
                                                    <input type="number" min="1" id="qte" name="qte" class="form-control col-md-12">
                                                </div>
                                            </div>
                                            {{--<div class="col-md-3">
                                                <div class="col-md-12">
                                                    <label class="col-md-12" for="qte">Quantités restantes <span class="text-danger">*</span></label>
                                                    <input type="number" min="1" value="0" id="qterest" name="qterest" class="form-control col-md-12" required>
                                                </div>
                                            </div>--}}

                                            <div class="col-md-3">
                                                <div class="col-span-12">
                                                    <label class="col-md-12" style="opacity:0;">Quantité</label>
                                                    <a onclick="add_element_to_array()" class="btn btn-primary btn-md waves-effect waves-light"><i class="fa fa-plus"></i> AJOUTER</a>
                                                </div>
                                            </div>
                                        </div>

                                        <input type="hidden" value="{{$bl->id}}" name="idoldbrouillon">

                                        <div class="w-full px-3 m-3 py-2 border border-solid border-red-200 rounded-sm">
                                            <input type="hidden" id="varieMontant">
                                            <table class="table metable" id="data_session">
                                            </table>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="text-right mt-5">
                                                <button id="addbtnbrouillon" type="submit" class="btn btn-warning text-white" name="bouton" value="brouillon"><i class="fa fa-edit"></i> Enregistrer en brouillon</button>
                                                <button id="addbtn" type="button" onclick="submitForm()" class="btn btn-success text-white" name="bouton" value="save"><i class="fa fa-save"></i> Enregistrer</button>
                                                <a href="{{route('bl')}}" class="btn btn-default text-gray-700 mr-1">Annuler</a>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div> <!-- container -->
        </div> <!-- content -->
    </div>
@endsection
