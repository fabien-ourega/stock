@extends('layouts.app')

@section('title')
    Bon de commande brouillon
    @parent
@stop

@section('header_styles')
    <!-- DataTables -->
    <link href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
    <style>
        tr.spacend td{
            padding-bottom: 3em!important;
        }
    </style>
@endsection

@section('footer_scripts')
    <!-- Sweet-Alert  -->
    <script src="{{ asset('assets/pages/jquery.sweetalert.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
        });
    </script>
@endsection


@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="page-title">
                            Bon de commande brouillon
                        </h4>
                        <ol class="breadcrumb"></ol>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <!-- <div class="panel-heading">
                                <h4>Invoice</h4>
                            </div> -->
                            <div class="panel-body">
                                <div class="clearfix">
                                    <div class="pull-left">
                                        <h4 class="text-right"><img src="{{asset('assets/images/logo_dark.png')}}" alt="velonic" class="img-responsive" width="20%"></h4>

                                    </div>
                                    <div class="pull-right">
                                        <h4>BC :  <br>
                                            <strong>{{$bl->slug}}</strong> <br>
                                            {{$bl->reference ? $bl->reference : ''}}
                                        </h4>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <p class="text-center text-danger" style="font-size: 1.3em;font-weight: bold;margin-bottom: 40px">BON DE COMMANDE (BROUILLON)</p>

                                    <div class="col-md-12">
                                        <div class="pull-left m-t-30">
                                            <address>
                                                <strong>{{$bl->client ? $bl->client->nom : ''}}</strong><br>
                                                {{$bl->client ? $bl->client->adresse : ''}}<br>
                                                {{$bl->client ? 'Contact: '.$bl->client->contact : ''}}
                                            </address>
                                        </div>
                                        <div class="pull-right m-t-30">
                                            <p><strong>Date: </strong> {{$bl->created_at->format('d/m/Y')}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="m-h-50"></div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table m-t-30">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Désignation</th>
                                                        <th>Catégorie</th>
                                                        <th>P.U</th>
                                                        <th>Quantités commandées</th>
                                                        <th>Quantités à livrer</th>
                                                        <th>Quantités restantes à livrer</th>
                                                        {{--<th>Observation</th>--}}
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @php $sstotal = 0 @endphp
                                                @foreach(json_decode($bl->donnes) as $k=>$data)
                                                <tr>
                                                    @php
                                                        $listbl = explode(';',$data);
                                                        $bstock = \App\Bstock::where('id',$listbl[0])->has('produit')->with('produit')->first();
                                                        //$cat = \App\Produit::where('id',$listbl[0])->has('categorie')->with('categorie')->first();
                                                        $pu = $bstock ? $bstock->produit->price:0;
                                                        $total = $listbl[2] * $pu ;
                                                        $sstotal = $sstotal + $total;
                                                    @endphp
                                                    @if($bstock)
                                                    <td>{{$k+1}}</td>
                                                    <td>{{$listbl[1]}}</td>
                                                    <td> {{$bstock ? $bstock->produit->categorie ? $bstock->produit->categorie->libelle : '' : ''}}</td>
                                                    <td>@price($pu)</td>
                                                    <td>{{$listbl[2]}}</td>
                                                    <td>{{$listbl[3]}}</td>
                                                    <td>{{$listbl[4]}}</td>
                                                    @endif
                                                    {{--<td></td>--}}
                                                </tr>
                                                @endforeach
                                                </tbody>
                                            </table>

                                            <br>
                                            <div class="col-md-12">
                                                <p class="text-right"><b>Total HT: </b>@price($sstotal) Fcfa</p>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <hr>
                            </div>
                        </div>

                    </div>

                </div>

            </div> <!-- container -->
        </div> <!-- content -->
    </div>
@endsection
