<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Admin blog">
    <meta name="author" content="Admin">

    <!-- CSRF Token -->
    <meta property="csrf-token" id="csrf-token" name="csrf-token" content="{{ csrf_token() }}">

    <title>@section('title') - Stock @show</title>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- App favicon -->
    <link rel="shortcut icon" href="{{asset('assets/images/logo_light.png')}}">

    <!-- Styles -->
    {{--<link href="{{ asset('assets/css/app.css') }}" rel="stylesheet">--}}
    <link href="{{ asset('assets/css/mystyle.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/core.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/components.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/icons.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/pages.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet" type="text/css" />

    <script src="{{ asset('assets/js/modernizr.min.js') }}"></script>

    @yield('header_styles')

</head>
<body>
    <div id="app">

        {{--@if(!Request::is('login'))--}}
        @auth
            <!-- Begin page -->
            <div id="wrapper">

                <!-- Top Bar Start -->
                <div class="topbar">

                    <!-- LOGO -->
                    <div class="topbar-left">
                        <div class="text-center">

                            <!-- Image Logo here -->
                            <a href="{{route('home')}}" class="logo">
                                <i class="icon-c-logo"> <img src="{{asset('assets/images/logo_sm.png')}}" height="42"/> </i>
                                <span><img src="{{asset('assets/images/logo_light.png')}}" height="50"/></span>
                            </a>
                        </div>
                    </div>

                    <!-- Button mobile view to collapse sidebar menu -->
                    <div class="navbar navbar-default" role="navigation">
                        <div class="container">
                            <div class="">
                                <div class="pull-left">
                                    <button class="button-menu-mobile open-left waves-effect waves-light">
                                        <i class="md md-menu"></i>
                                    </button>
                                    <span class="clearfix"></span>
                                </div>

                                {{--<ul class="nav navbar-nav hidden-xs">--}}
                                    {{--<li><a href="#" class="waves-effect waves-light">Files</a></li>--}}
                                    {{--<li class="dropdown">--}}
                                        {{--<a href="#" class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown"--}}
                                           {{--role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span--}}
                                                    {{--class="caret"></span></a>--}}
                                        {{--<ul class="dropdown-menu">--}}
                                            {{--<li><a href="#">Action</a></li>--}}
                                            {{--<li><a href="#">Another action</a></li>--}}
                                            {{--<li><a href="#">Something else here</a></li>--}}
                                            {{--<li><a href="#">Separated link</a></li>--}}
                                        {{--</ul>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}

                                {{--<form role="search" class="navbar-left app-search pull-left hidden-xs">--}}
                                    {{--<input type="text" placeholder="Search..." class="form-control">--}}
                                    {{--<a href=""><i class="fa fa-search"></i></a>--}}
                                {{--</form>--}}


                                <ul class="nav navbar-nav navbar-right pull-right">
                                    {{--<li class="dropdown top-menu-item-xs">--}}
                                        {{--<a href="#" data-target="#" class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="true">--}}
                                            {{--<i class="icon-bell"></i> <span class="badge badge-xs badge-danger">3</span>--}}
                                        {{--</a>--}}
                                        {{--<ul class="dropdown-menu dropdown-menu-lg">--}}
                                            {{--<li class="notifi-title"><span class="label label-default pull-right">New 3</span>Notification</li>--}}
                                            {{--<li class="list-group slimscroll-noti notification-list">--}}
                                                {{--<!-- list item-->--}}
                                                {{--<a href="javascript:void(0);" class="list-group-item">--}}
                                                    {{--<div class="media">--}}
                                                        {{--<div class="pull-left p-r-10">--}}
                                                            {{--<em class="fa fa-diamond noti-primary"></em>--}}
                                                        {{--</div>--}}
                                                        {{--<div class="media-body">--}}
                                                            {{--<h5 class="media-heading">A new order has been placed A new order has been placed</h5>--}}
                                                            {{--<p class="m-0">--}}
                                                                {{--<small>There are new settings available</small>--}}
                                                            {{--</p>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--</a>--}}

                                                {{--<!-- list item-->--}}
                                                {{--<a href="javascript:void(0);" class="list-group-item">--}}
                                                    {{--<div class="media">--}}
                                                        {{--<div class="pull-left p-r-10">--}}
                                                            {{--<em class="fa fa-cog noti-warning"></em>--}}
                                                        {{--</div>--}}
                                                        {{--<div class="media-body">--}}
                                                            {{--<h5 class="media-heading">New settings</h5>--}}
                                                            {{--<p class="m-0">--}}
                                                                {{--<small>There are new settings available</small>--}}
                                                            {{--</p>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--</a>--}}

                                                {{--<!-- list item-->--}}
                                                {{--<a href="javascript:void(0);" class="list-group-item">--}}
                                                    {{--<div class="media">--}}
                                                        {{--<div class="pull-left p-r-10">--}}
                                                            {{--<em class="fa fa-bell-o noti-custom"></em>--}}
                                                        {{--</div>--}}
                                                        {{--<div class="media-body">--}}
                                                            {{--<h5 class="media-heading">Updates</h5>--}}
                                                            {{--<p class="m-0">--}}
                                                                {{--<small>There are <span class="text-primary font-600">2</span> new updates available</small>--}}
                                                            {{--</p>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--</a>--}}

                                                {{--<!-- list item-->--}}
                                                {{--<a href="javascript:void(0);" class="list-group-item">--}}
                                                    {{--<div class="media">--}}
                                                        {{--<div class="pull-left p-r-10">--}}
                                                            {{--<em class="fa fa-user-plus noti-pink"></em>--}}
                                                        {{--</div>--}}
                                                        {{--<div class="media-body">--}}
                                                            {{--<h5 class="media-heading">New user registered</h5>--}}
                                                            {{--<p class="m-0">--}}
                                                                {{--<small>You have 10 unread messages</small>--}}
                                                            {{--</p>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--</a>--}}

                                                {{--<!-- list item-->--}}
                                                {{--<a href="javascript:void(0);" class="list-group-item">--}}
                                                    {{--<div class="media">--}}
                                                        {{--<div class="pull-left p-r-10">--}}
                                                            {{--<em class="fa fa-diamond noti-primary"></em>--}}
                                                        {{--</div>--}}
                                                        {{--<div class="media-body">--}}
                                                            {{--<h5 class="media-heading">A new order has been placed A new order has been placed</h5>--}}
                                                            {{--<p class="m-0">--}}
                                                                {{--<small>There are new settings available</small>--}}
                                                            {{--</p>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--</a>--}}

                                                {{--<!-- list item-->--}}
                                                {{--<a href="javascript:void(0);" class="list-group-item">--}}
                                                    {{--<div class="media">--}}
                                                        {{--<div class="pull-left p-r-10">--}}
                                                            {{--<em class="fa fa-cog noti-warning"></em>--}}
                                                        {{--</div>--}}
                                                        {{--<div class="media-body">--}}
                                                            {{--<h5 class="media-heading">New settings</h5>--}}
                                                            {{--<p class="m-0">--}}
                                                                {{--<small>There are new settings available</small>--}}
                                                            {{--</p>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--</a>--}}
                                            {{--</li>--}}
                                            {{--<li>--}}
                                                {{--<a href="javascript:void(0);" class="list-group-item text-right">--}}
                                                    {{--<small class="font-600">See all notifications</small>--}}
                                                {{--</a>--}}
                                            {{--</li>--}}
                                        {{--</ul>--}}
                                    {{--</li>--}}
                                    <li class="hidden-xs">
                                        <a href="#" id="btn-fullscreen" class="waves-effect waves-light"><i class="icon-size-fullscreen"></i></a>
                                    </li>
                                    {{--<li class="hidden-xs">--}}
                                        {{--<a href="#" class="right-bar-toggle waves-effect waves-light"><i class="icon-settings"></i></a>--}}
                                    {{--</li>--}}
                                    <li class="dropdown top-menu-item-xs">
                                        <a href="" class="dropdown-toggle profile waves-effect waves-light" data-toggle="dropdown" aria-expanded="true"><img src="{{Auth::user()->img ? asset('assets/userAvatar/'.Auth::user()->img.'')  : asset('assets/images/users/user.jpeg')}}" alt="user-img" class="img-circle"> </a>
                                        <ul class="dropdown-menu">
                                            <li><a href="{{route('profiles')}}"><i class="ti-user m-r-10 text-custom"></i> Profile</a></li>
                                            <li class="divider"></li>
                                            <li>
                                                <a href="{{ route('logout') }}"
                                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                    <i class="ti-power-off m-r-10 text-danger"></i> Déconnexion
                                                </a>

                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    @csrf
                                                </form>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <!--/.nav-collapse -->
                        </div>
                    </div>
                </div>
                <!-- Top Bar End -->


                @include('includes/nav')
        @endauth
        {{--@endif--}}

        <main class="py- 4">
            @include('includes/successOrError')

            @yield('content')
        </main>


        {{--@if(!Request::is('login'))--}}
        @auth
                <!-- Right Sidebar -->
                <div class="side-bar right-bar nicescroll">
                        <h4 class="text-center">Chat</h4>
                        <div class="contact-list nicescroll">
                            <ul class="list-group contacts-list">
                                <li class="list-group-item">
                                    <a href="#">
                                        <div class="avatar">
                                            <img src="{{asset('assets/images/users/avatar-1.jpg')}}" alt="">
                                        </div>
                                        <span class="name">Chadengle</span>
                                        <i class="fa fa-circle online"></i>
                                    </a>
                                    <span class="clearfix"></span>
                                </li>
                                <li class="list-group-item">
                                    <a href="#">
                                        <div class="avatar">
                                            <img src="{{asset('assets/images/users/avatar-2.jpg')}}" alt="">
                                        </div>
                                        <span class="name">Tomaslau</span>
                                        <i class="fa fa-circle online"></i>
                                    </a>
                                    <span class="clearfix"></span>
                                </li>
                                <li class="list-group-item">
                                    <a href="#">
                                        <div class="avatar">
                                            <img src="{{asset('assets/images/users/avatar-3.jpg')}}" alt="">
                                        </div>
                                        <span class="name">Stillnotdavid</span>
                                        <i class="fa fa-circle online"></i>
                                    </a>
                                    <span class="clearfix"></span>
                                </li>
                                <li class="list-group-item">
                                    <a href="#">
                                        <div class="avatar">
                                            <img src="{{asset('assets/images/users/avatar-4.jpg')}}" alt="">
                                        </div>
                                        <span class="name">Kurafire</span>
                                        <i class="fa fa-circle online"></i>
                                    </a>
                                    <span class="clearfix"></span>
                                </li>
                                <li class="list-group-item">
                                    <a href="#">
                                        <div class="avatar">
                                            <img src="{{asset('assets/images/users/avatar-5.jpg')}}" alt="">
                                        </div>
                                        <span class="name">Shahedk</span>
                                        <i class="fa fa-circle away"></i>
                                    </a>
                                    <span class="clearfix"></span>
                                </li>
                                <li class="list-group-item">
                                    <a href="#">
                                        <div class="avatar">
                                            <img src="{{asset('assets/images/users/avatar-6.jpg')}}" alt="">
                                        </div>
                                        <span class="name">Adhamdannaway</span>
                                        <i class="fa fa-circle away"></i>
                                    </a>
                                    <span class="clearfix"></span>
                                </li>
                                <li class="list-group-item">
                                    <a href="#">
                                        <div class="avatar">
                                            <img src="{{asset('assets/images/users/avatar-7.jpg')}}" alt="">
                                        </div>
                                        <span class="name">Ok</span>
                                        <i class="fa fa-circle away"></i>
                                    </a>
                                    <span class="clearfix"></span>
                                </li>
                                <li class="list-group-item">
                                    <a href="#">
                                        <div class="avatar">
                                            <img src="{{asset('assets/images/users/avatar-8.jpg')}}" alt="">
                                        </div>
                                        <span class="name">Arashasghari</span>
                                        <i class="fa fa-circle offline"></i>
                                    </a>
                                    <span class="clearfix"></span>
                                </li>
                                <li class="list-group-item">
                                    <a href="#">
                                        <div class="avatar">
                                            <img src="{{asset('assets/images/users/avatar-9.jpg')}}" alt="">
                                        </div>
                                        <span class="name">Joshaustin</span>
                                        <i class="fa fa-circle offline"></i>
                                    </a>
                                    <span class="clearfix"></span>
                                </li>
                                <li class="list-group-item">
                                    <a href="#">
                                        <div class="avatar">
                                            <img src="{{asset('assets/images/users/avatar-10.jpg')}}" alt="">
                                        </div>
                                        <span class="name">Sortino</span>
                                        <i class="fa fa-circle offline"></i>
                                    </a>
                                    <span class="clearfix"></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                <!-- /Right-bar -->
            </div>
            <!-- END wrapper -->
        {{--@endif--}}
        @endauth
    </div>

    <script>
        var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="{{ url('assets/js/jquery.min.js') }}"></script>
    <script src="{{ url('assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ url('assets/js/detect.js') }}"></script>
    <script src="{{ url('assets/js/fastclick.js') }}"></script>
    <script src="{{ url('assets/js/jquery.slimscroll.js') }}"></script>
    <script src="{{ url('assets/js/jquery.blockUI.js') }}"></script>
    <script src="{{ url('assets/js/waves.js') }}"></script>
    <script src="{{ url('assets/js/wow.min.js') }}"></script>
    <script src="{{ url('assets/js/jquery.nicescroll.js') }}"></script>
    <script src="{{ url('assets/js/jquery.scrollTo.min.js') }}"></script>

    <!-- Scripts -->
    {{--<script src="{{ asset('assets/js/app.js') }}" defer></script>--}}

    @yield('footer_scripts')

    <script src="{{ url('assets/js/jquery.core.js') }}"></script>
    <script src="{{ url('assets/js/jquery.app.js') }}"></script>
</body>
</html>
