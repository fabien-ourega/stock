@extends('layouts.app')

@section('title')
    Admin
    @parent
@stop

@section('header_styles')
    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="{{ url('assets/plugins/morris/morris.css') }}">
    <style>
        .morris-hover.morris-default-style{
            background: #000;
        }
    </style>
@endsection

@section('footer_scripts')
    <script src="{{ url('assets/plugins/peity/jquery.peity.min.js') }}"></script>
    <!-- jQuery  -->
    <script src="{{ url('assets/plugins/waypoints/lib/jquery.waypoints.js') }}"></script>
    <script src="{{ url('assets/plugins/counterup/jquery.counterup.min.js') }}"></script>

    <script src="{{ url('assets/plugins/morris/morris.min.js') }}"></script>
    <script src="{{ url('assets/plugins/raphael/raphael-min.js') }}"></script>

    <script src="{{ url('assets/plugins/jquery-knob/jquery.knob.js') }}"></script>

    <script src="{{ url('assets/pages/jquery.dashboard.js') }}"></script>
    <script src="{{ url('assets/pages/jquery.dashboard_2.js') }}"></script>

    {{--<script src="{{ url('assets/plugins/flot-chart/jquery.flot.min.js') }}"></script>
    <script src="{{ url('assets/plugins/flot-chart/jquery.flot.time.js') }}"></script>
    <script src="{{ url('assets/plugins/flot-chart/jquery.flot.tooltip.min.js') }}"></script>
    <script src="{{ url('assets/plugins/flot-chart/jquery.flot.resize.js') }}"></script>
    <script src="{{ url('assets/plugins/flot-chart/jquery.flot.pie.js') }}"></script>
    <script src="{{ url('assets/plugins/flot-chart/jquery.flot.selection.js') }}"></script>
    <script src="{{ url('assets/plugins/flot-chart/jquery.flot.stack.js') }}"></script>
    <script src="{{ url('assets/plugins/flot-chart/jquery.flot.orderBars.min.js') }}"></script>
    <script src="{{ url('assets/plugins/flot-chart/jquery.flot.crosshair.js') }}"></script>--}}

{{--    <script src="{{ url('assets/pages/morris.init.js') }}"></script>--}}
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            new Morris.Bar({
                element: 'myfirstchart',
                data: [
                    @foreach($stats as $key=>$stat)
                    { y: '{{$memois[$key][0]}}', a: '{{$stat}}'},
                    @endforeach
                ],
                xkey: 'y',
                ykeys: ['a'],
                labels: ['Facture TTC'],
                hideHover: 'auto',
                behaveLikeLine: true,
                resize: true,
                pointFillColors:['#000'],
                pointStrokeColors: ['black'],
                barColors:['#5d9cec']
            });

            new Morris.Donut({
                element: 'donut-example',
                data: [
                    {label: "Factures traitées", value: '{{$statfactures['traite']}}'},
                    {label: "Factures non traitées", value: '{{$statfactures['ntraite']}}'},
                ],
                colors: ['#4bff00', '#ff0000']
            });

            new Morris.Donut({
                element: 'donut-bon',
                data: [
                    {label: "Bons traités", value: '{{$statbon['traite']}}'},
                    {label: "Bon en attente", value: '{{$statbon['ntraite']}}'},
                ],
                colors: ['#4bff00', '#ff0000']
            });


            $('.counter').counterUp({
                delay: 100,
                time: 1200
            });

            $(".knob").knob();

        });

        var resizefunc = [];
    </script>
@endsection


@section('content')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Tableau de bord</h4>
                        <p class="text-muted page-title-alt">Bienvenue sur votre panel d'administration !</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 col-lg-3">
                        <div class="widget-bg-color-icon card-box fadeInDown animated">
                            <div class="bg-icon bg-icon-info pull-left">
                                <i class="fa fa-tasks text-info"></i>
                            </div>
                            <div class="text-right">
                                <h3 class="text-dark"><b class="counter">{{count($projects)}}</b></h3>
                                <p class="text-muted">Total projet</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-3">
                        <div class="widget-bg-color-icon card-box">
                            <div class="bg-icon bg-icon-pink pull-left">
                                <i class="fa fa-users text-pink"></i>
                            </div>
                            <div class="text-right">
                                <h3 class="text-dark"><b class="counter">{{count($clients)}}</b></h3>
                                <p class="text-muted">Total client</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-3">
                        <div class="widget-bg-color-icon card-box">
                            <div class="bg-icon bg-icon-purple pull-left">
                                <i class="md md-add-shopping-cart text-purple"></i>
                            </div>
                            <div class="text-right">
                                <h3 class="text-dark"><b class="counter">{{count($produits)}}</b></h3>
                                <p class="text-muted">Total produit</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-3">
                        <div class="widget-bg-color-icon card-box">
                            <div class="bg-icon bg-icon-success pull-left">
                                <i class="fa fa-file text-success"></i>
                            </div>
                            <div class="text-right">
                                <h3 class="text-dark"><b class="counter">{{count($bons)}}</b></h3>
                                <p class="text-muted">Total BC</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    @if(count($produitfaible)>0)
                    <div class="col-lg-4">
                        <div class="card-box">
                            <h4 class="m-t-0 header-title"><b>Quantité de Produits Faible</b></h4>
                            <p class="text-success">Stock Interne</p>

                            <div class="nicescroll mx-box" tabindex="5000" style="overflow: hidden; outline: none;">
                                <ul class="list-unstyled transaction-list m-r-5">
                                    @foreach($produitfaible as $item)
                                    @if($item->produit)
                                        @php $typ = \App\Typeproduit::where('id',$item->produit->type_id)->first() @endphp
                                        <li>
                                            {{--<i class="ti-download text-success"></i>--}}
                                            <span class="tran-text">{{$item->produit->nom}}</span>
                                            <span class="pull-right tran-price"><span class="text-danger">{{$item->qte}}</span> <span>{{$typ->libelle}}</span></span>
                                            @if(Auth::user()->id==1)
                                            <span class="pull-right text-muted">@price($item->produit->price) F</span>
                                            @endif
                                            <span class="clearfix"></span>
                                        </li>
                                    @endif
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    @endif

                    @foreach($projects as $project)
                    @php $listbypjt = \App\Bstock::where('pjt_id',$project->id)->where('qte','<=',10)->with('produit')->get(); @endphp
                    @if(count($listbypjt)>0)
                        <div class="col-lg-4">
                            <div class="card-box">
                                <h4 class="m-t-0 header-title"><b>Quantité de Produits Faible</b></h4>
                                <p class="text-success">{{$project->libelle}}</p>

                                <div class="nicescroll mx-box" tabindex="5000" style="overflow: hidden; outline: none;">
                                    <ul class="list-unstyled transaction-list m-r-5">
                                        @foreach($listbypjt as $item)
                                         @if($item->produit)
                                            @php $typ = \App\Typeproduit::where('id',$item->produit->type_id)->first() @endphp
                                            <li title="{{$item->produit->nom}}">
                                                <span class="tran-text">{{$item->produit->nom}}</span>
                                                <span class="pull-right tran-price"><span class="text-danger">{{$item->qte}}</span> <span>{{$typ->libelle}}</span></span>
                                                @if(Auth::user()->id==1)
                                                <span class="pull-right text-muted">@price($item->produit->price) F</span>
                                                @endif
                                                <span class="clearfix"></span>
                                            </li>
                                         @endif
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endif
                    @endforeach

                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="portlet">
                            <div class="portlet-heading">
                                <h3 class="portlet-title text-dark">Etat des Factures</h3>
                                <div class="clearfix"></div>
                            </div>

                            <div id="portlet3" class="panel-collapse collapse in">
                                <div class="portlet-body">
                                    <div id="donut-example" style="height: 300px;"></div>

                                    <div class="text-center">
                                        <ul class="list-inline chart-detail-list">
                                            <li>
                                                <h5><i class="fa fa-circle m-r-5" style="color: #4bff00;"></i>Factures traitées <span class="label label-success">{{$statfactures['traite']}}</span></h5>
                                            </li>
                                            <li>
                                                <h5><i class="fa fa-circle m-r-5" style="color: #ff0000;"></i>Factures non traitées <span class="label label-danger">{{$statfactures['ntraite']}}</span></h5>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /Portlet -->
                    </div>

                    <div class="col-lg-6">
                        <div class="portlet">
                            <div class="portlet-heading">
                                <h3 class="portlet-title text-dark">Etat des Bons de commande</h3>
                                <div class="clearfix"></div>
                            </div>

                            <div id="portlet3" class="panel-collapse collapse in">
                                <div class="portlet-body">
                                    <div id="donut-bon" style="height: 300px;"></div>

                                    <div class="text-center">
                                        <ul class="list-inline chart-detail-list">
                                            <li>
                                                <h5><i class="fa fa-circle m-r-5" style="color: #4bff00;"></i>Bons traités <span class="label label-success">{{$statbon['traite']}}</span></h5>
                                            </li>
                                            <li>
                                                <h5><i class="fa fa-circle m-r-5" style="color: #ff0000;"></i>Bon en attente <span class="label label-danger">{{$statbon['ntraite']}}</span></h5>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /Portlet -->
                    </div>
                    @if(Auth::user()->role_id!=2)

                    <div class="col-lg-12">
                        <div class="portlet">
                            <div class="portlet-heading">
                                <h3 class="portlet-title text-dark"> MONTANT DES FACTURES PAR MOIS</h3>
                                <div class="clearfix"></div>
                            </div>
                            <div id="portlet3" class="panel-collapse collapse in">
                                <div class="portlet-body">
                                    <div id="myfirstchart" style="height: 300px;"></div>
                                </div>
                            </div>
                        </div>
                        <!-- /Portlet -->
                    </div>
                        @endif
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="card-box">
                            <h4 class="text-dark header-title m-t-0">Evolution des projets</h4>
                            <br>

                            <div class="row">
                                <div class="col-md-12">
                                    @foreach($projects as $project)
                                        @php
                                            $totalprevu = \App\Bstock::where('pjt_id',$project->id)->get();
                                            $totalprevu = $totalprevu->sum('qte_first');
                                            $totallivre = \App\Blitem::where('pjt_id',$project->id)->get();
                                            $totallivre = $totallivre->sum('qte');
                                            $multip = $totallivre * 100 ;

                                            if($totalprevu ==0){
                                                $pcent = 0;
                                            }else{
                                                $pcent = $multip!=0 ? $multip / $totalprevu : 0;
                                            }
                                        @endphp
                                    <p class="font-600">{{$project->libelle}} <span class="text-primary pull-right">{{round($pcent,2)}}%</span></p>
                                    <div class="progress m-b-30">
                                        <div class="progress-bar progress-bar-primary progress-animated wow animated" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: {{$pcent}}%">
                                        </div>
                                    </div>
                                    @endforeach


                                </div>



                            </div>

                            <!-- end row -->

                        </div>

                    </div>

                    <div class="col-lg-6">
                        <div class="card-box">
                            <a href="{{route('bl')}}" class="pull-right btn btn-default btn-sm waves-effect waves-light"><i class="fa fa-plus"></i> Voir plus</a>
                            <h4 class="text-dark header-title m-t-0">Dernier Bon de commande</h4>
                            <p class="text-muted m-b-30 font-13">Les bons de commande récemment ajoutés</p>

                            <div class="table-responsive">
                                <table class="table table-actions-bar m-b-0">
                                    <thead>
                                    <tr>
                                        <th>Nom</th>
                                        <th>Projet</th>
                                        <th>Date</th>
                                        <th style="min-width: 80px;">Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($lastbons as $lastbon)
                                        <tr>
                                            <td>{{$lastbon->slug}} </td>
                                            <td>{{$lastbon->project->libelle}} </td>
                                            <td>{{$lastbon->created_at->format('d/m/y')}} </td>
                                            <td>
                                                @if($lastbon->status == 0)
                                                    <span class="text-danger">Non traité</span>
                                                @else
                                                    <span class="text-custom">Traité</span>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach


                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>

                </div>
                <!-- end row -->


            </div> <!-- container -->

        </div> <!-- content -->

    </div>
@endsection
