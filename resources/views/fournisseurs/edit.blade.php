@extends('layouts.app')

@section('title')
    Editer un fournisseur
    @parent
@endsection

@section('header_styles')
    {{--<link href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>--}}


@endsection

@section('footer_scripts')
    {{--<script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>--}}
    <!--form validation init-->
    <script src="{{ asset('assets/plugins/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js') }}" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            if($("#elm1").length > 0){
                tinymce.init({
                    selector: "textarea#elm1",
                    theme: "modern",
                    menubar:false,
                    statusbar: false,
                    height:300,
                    plugins: [
                        "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                        "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                        "save table contextmenu directionality emoticons template paste textcolor"
                    ],
                    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link  | print preview media fullpage | forecolor backcolor",
                    style_formats: [
                        {title: 'Bold text', inline: 'b'},
                        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                        {title: 'Example 1', inline: 'span', classes: 'example1'},
                        {title: 'Example 2', inline: 'span', classes: 'example2'},
                        {title: 'Table styles'},
                        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
                    ]
                });
            }
        });


    </script>
@endsection


@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">

                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Editer un fournisseur</h4>
                        <ol class="breadcrumb"> </ol>
                    </div>
                </div>

                <div class="row">
                    <form class="form-horizontal" method="post" action="{{route('frsseur.update')}}" role="form">
                        @csrf
                        <div class="col-md-8">
                            <div class="card-box">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="col-md-12" for="nom">Entreprise <span class="text-danger">*</span></label>
                                                <input type="text" value="{{$item->libelle}}" name="libelle" class="form-control col-md-12" required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="col-md-12" for="contact">Contact <span class="text-danger">*</span></label>
                                                <input type="text" value="{{$item->contact}}" name="contact" class="form-control col-md-12" required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="col-md-12" for="email">Email</label>
                                                <input type="email" value="{{$item->email}}" name="email" class="form-control col-md-12">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="col-md-12" for="adress">Adresse </label>
                                                <input type="text" value="{{$item->adresse}}" name="adresse" class="form-control col-md-12">
                                                <input type="hidden" value="{{$item->id}}" name="idfourn">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">

                            <div class="col-md-12">
                                <div class="card-box">
                                    <div class="row">
                                        <button type="submit" class="btn btn-primary btn-md waves-effect waves-light" ><i class="fa fa-check-square"></i> Modifier</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div> <!-- container -->
        </div> <!-- content -->
    </div>
@endsection
