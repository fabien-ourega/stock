<html>
<style type="text/css" media="all">
    body{
        font-size: 10px;
        margin: 1.5px;
    }
    table {
        font-size: 11px;
    }

    .td{
        border: 1px solid #000000;
        padding-left: 5px;
        padding: 10px;
    }
    .tdd{
        padding: 3px;
        border-bottom: 1px solid #e2e2e2;
    }
    .txt-center{
        text-align: center;
    }
    .txt-right{
        text-align: right;
    }
    .pr-20{
        padding-right: 20px;
    }
    .mb-20{
        margin-bottom: 20px;
    }
    #resultscol td{
        border: 1px solid #000000;
        padding: 3px;
    }

    #fin ul li{
        list-style: none;
        text-align: left;
    }

    .custom-control-label::before,
    .custom-control-label::after {
        top: 0.1rem !important;
        left: -2rem !important;
        width: 1.25rem !important;
        height: 1.25rem !important;
    }

    footer {
        position: fixed;
        bottom: 0cm;
        left: 0cm;
        right: 0cm;
        height: 2cm;

        color: #000;
        font-size: 10px;
        text-align: center;
    }
</style>
<body>
<div>
    @foreach($customerArray as $data)
        @php $magasin = \App\Magasin::find($data);
            $stocks = \App\Bstock::where('magasin_id',$data)->has('produit')->with('produit')->get();
            if($selectnbpdt==1){
                $stocks = \App\Bstock::where('magasin_id',$data)->where('qte','<>',0)->has('produit')->with('produit')->get();
            }
        @endphp

        @if($magasin)
            <table cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td align="center">
                        <p style="font-size: 18px;font-weight: bold;margin-bottom: 10px">ETAT DES MAGASINS {{$magasin->nom}}</p>
                    </td>
                </tr>
            </table>

            <table>
                <tr>
                    <td><strong>No</strong></th>
                    <td><strong>Nom</strong></th>
                    <td><strong>Unité</strong></th>
                    <td><strong>Catégorie </strong></th>
                    <td><strong>P.U </strong></th>
                    <td><strong>Disponible</strong></th>
                    <td><strong>Montant</strong></th>
                </tr>

                @foreach($stocks as $k=>$produit)
                    @if($produit->produit)
                        @php
                            $cat = \App\Categorie::where('id',$produit->produit->categorie_id)->first();
                            $type = \App\Typeproduit::where('id',$produit->produit->type_id)->first();
                        @endphp

                        <tr>
                            <td>{{$k+1}} </td>
                            <td>{{$produit->produit->nom}}</td>
                            <td>{{$type->libelle}}</td>
                            <td>{{$cat->libelle}}</td>
                            <td>{{$produit->produit->price}}</td>
                            <td>{{$produit->qte}}</td>
                            <td>{{$produit->qte * $produit->produit->price}} </td>
                        </tr>
                    @endif
                @endforeach
            </table>
        @endif
    @endforeach
</div>


</body>
</html>
