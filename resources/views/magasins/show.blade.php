@extends('layouts.app')

@section('title')
    Magasin {{$magasin->nom}}
    @parent
@stop

@section('header_styles')
    <!-- DataTables -->
    <link href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('footer_scripts')
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.js') }}"></script>

    <script src="{{ asset('assets/pages/datatables.init.js') }}"></script>

    <!-- Sweet-Alert  -->
    <script src="{{ asset('assets/pages/jquery.sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/js/imask.js') }}"></script>
    {{--<script src="{{ asset('assets/pages/jquery.sweet-alert.init.js') }}"></script>--}}

    <script type="text/javascript">
        $(document).ready(function () {
            IMask(document.getElementById('estock'), {
                mask: Number,
                min: 1,
                max: 100000000000,
                thousandsSeparator: ' '
            });

            IMask(document.getElementById('astock'), {
                mask: Number,
                min: 1,
                max: 100000000000,
                thousandsSeparator: ' '
            });

            IMask(document.getElementById('tstock'), {
                mask: Number,
                min: 1,
                max: 100000000000,
                thousandsSeparator: ' '
            });

            $('#myTable').dataTable({
                language: {
                    lengthMenu: "_MENU_",
                    search: "_INPUT_",
                    searchPlaceholder: "Recherche",
                    sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                    sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    sInfoPostFix:    "",
                    sLoadingRecords: "Chargement en cours...",
                    sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
                    sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
                    sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    oPaginate: {
                        "sFirst":    "Premier",
                        "sLast":    "Dernier",
                        "sNext":    "Suivant",
                        "sPrevious": "Précédent"
                    },
                }
            });
        });


        function astock(nb,description,slug){
            $("#formAdd")[0].reset();
            $("#idslug").val(slug);
            $("#lbbe").html(description);
            $("#dispo").val(nb);
        }

        function estock(nb,description,slug){
            $("#formEdit")[0].reset();
            $("#idsluge").val(slug);
            $("#elbbe").html(description);
            $("#edispo").val(nb);
        }

        function tstock(nb,description,slug){
            $("#formTrans")[0].reset();
            $("#tidsluge").val(slug);
            $("#tlbbe").html(description);
            $("#tdispo").val(nb);
        }

    </script>
@endsection


@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="page-title">Magasin <span class="text-success text-uppercase">{{$magasin->nom}}</span></h4>
                        <ol class="breadcrumb"> </ol>
                    </div>
                </div>

                @if(Auth::user()->role_id==1)
                <div id="standard-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="standard-modalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="standard-modalLabel">Approvisionné <br>
                                    <small class="text-success"><span id="lbbe"></span></small>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                </h4>
                            </div>
                            <form id="formAdd" action="{{route('magasins.store.appro')}}" class="form-horizontal" method="post" autocomplete="off">
                                @csrf
                                <div class="modal-body">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label class="col-md-12" for="password">Quantité en stock </label>
                                            <input type="text" name="dispo" id="dispo" class="form-control col-md-12" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label class="col-md-12" for="password">Quantité a approvisionner <span class="text-danger">*</span></label>
                                            <input type="text" name="astock" id="astock" class="form-control col-md-12" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <input type="hidden" name="idslug" id="idslug">
                                    <input type="hidden" name="slugpjt" value="{{$magasin->id}}">
                                    <button type="button" class="btn btn-light" data-dismiss="modal">Annuler</button>
                                    <button type="submit" class="btn btn-primary">Ajouter</button>
                                </div>
                            </form>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div>

                <div id="edit-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="edit-modalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="standard-modalLabel">Réduire <br>
                                    <small class="text-success"><span id="elbbe"></span></small>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                </h4>
                            </div>
                            <form id="formEdit" action="{{route('magasins.store.reduir')}}" class="form-horizontal" method="post" autocomplete="off">
                                @csrf
                                <div class="modal-body">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label class="col-md-12" for="password">Quantité en stock </label>
                                            <input type="text" name="dispo" id="edispo" class="form-control col-md-12" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label class="col-md-12" for="password">Quantité a réduire <span class="text-danger">*</span></label>
                                            <input type="text" name="astock" id="estock" class="form-control col-md-12" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <input type="hidden" name="idslug" id="idsluge">
                                    <input type="hidden" name="slugpjt" value="{{$magasin->id}}">
                                    <button type="button" class="btn btn-light" data-dismiss="modal">Annuler</button>
                                    <button type="submit" class="btn btn-primary">Modifier</button>
                                </div>
                            </form>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div>

                <div id="transfere-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="edit-modalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="standard-modalLabel">Transférer <br>
                                    <small class="text-success"><span id="tlbbe"></span></small>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                </h4>
                            </div>
                            <form id="formTrans" action="{{route('magasins.store.transfer')}}" class="form-horizontal" method="post" autocomplete="off">
                                @csrf
                                <div class="modal-body">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label class="col-md-12" for="password">Quantité en stock </label>
                                            <input type="text" name="dispo" id="tdispo" class="form-control col-md-12" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label class="col-md-12" for="password">Quantité a tranféré <span class="text-danger">*</span></label>
                                            <input type="text" name="astock" id="tstock" class="form-control col-md-12" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label class="col-md-12">Magasin <span class="text-danger">*</span></label>
                                            <select name="pjt" id="pjt" class="form-control select2" required>
                                                <option value="" selected>Choisir le magasin</option>
                                                    @foreach($othermagasin as $pro)
                                                    <option value="{{$pro->id}}">{{$pro->nom}}</option>
                                                    @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <input type="hidden" name="idslug" id="tidsluge">
                                    <input type="hidden" name="slugpjt" value="{{$magasin->id}}">
                                    <button type="button" class="btn btn-light" data-dismiss="modal">Annuler</button>
                                    <button type="submit" class="btn btn-primary">Transféré</button>
                                </div>
                            </form>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div>

                @endif

                <div class="row">
                    <div class="col-md-12">
                        <div class="card-box table-responsive">
                            <table id="myTable" class="table table-striped table-bordered actus">
                                <thead>
                                <tr>
                                    <th>Ref.No.</th>
                                    <th>Nom</th>
                                    <th>Catégorie </th>
                                    <th>P.U </th>
                                    <th>Disponible</th>
                                    <th>Montant</th>
                                    @if(1==2)<th>Actions </th>@endif
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($stocks as $k=>$produit)
                                    @if($produit->produit)
                                    @php
                                        $cat = \App\Categorie::where('id',$produit->produit->categorie_id)->first();
                                        $type = \App\Typeproduit::where('id',$produit->produit->type_id)->first();
                                    @endphp

                                    <tr>
                                        <td>#0{{$k+1}} </td>
                                        <td style="font-weight: bold">
                                            {{$produit->produit->nom}} <br>
                                            <small class="text-success">{{$type->libelle}}</small>
                                        </td>
                                        <td>{{$cat->libelle}}</td>

                                        <td>@price($produit->produit->price) Fcfa</td>

                                        @php $nb = $produit->qte @endphp
                                        <td>
                                            @if($nb < 5)
                                                <span class="label label-danger">@price($nb)</span>
                                            @else
                                                <span class="label label-success">@price($nb)</span>
                                            @endif
                                        </td>
                                        <td>
                                            @php $total = $nb * $produit->produit->price; @endphp
                                            <span class="label label-info">@price($total) Fcfa</span>
                                        </td>

                                        @if(1==2)<td>
                                            <button data-toggle="modal" data-target="#standard-modal" class="btn btn-default btn-sm waves-effect waves-light" onclick='astock("{{$nb}}","{{$produit->produit->nom}}","{{$produit->produit->id}}")'> <i class="fa fa-plus m-r-5"></i> <span>Approvisionner</span> </button>
                                            @if($nb>0)
                                                <button data-toggle="modal" data-target="#edit-modal" class="btn btn-danger btn-sm waves-effect waves-light" onclick='estock("{{$nb}}","{{$produit->produit->nom}}","{{$produit->produit->id}}")'> <i class="fa fa-minus m-r-5"></i> <span>Réduire</span> </button>
                                                <button data-toggle="modal" data-target="#transfere-modal" class="btn btn-primary btn-sm waves-effect waves-light" onclick='tstock("{{$nb}}","{{$produit->produit->nom}}","{{$produit->produit->id}}")'> <i class="fa fa-reply m-r-5"></i> <span>Transférer</span> </button>
                                            @endif
                                        </td>@endif
                                    </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div> <!-- container -->
        </div> <!-- content -->
    </div>
@endsection
