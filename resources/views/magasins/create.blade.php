@extends('layouts.app')

@section('title')
    Ajouter un magasin
    @parent
@endsection

@section('header_styles')
    {{--<link href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>--}}


@endsection

@section('footer_scripts')
    {{--<script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>--}}
    <!--form validation init-->
    <script src="{{ asset('assets/plugins/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/imask.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            IMask(document.getElementById('price'), {
                mask: Number,
                min: 1,
                max: 100000000000,
                thousandsSeparator: ' '
            });

            if($("#elm1").length > 0){
                tinymce.init({
                    selector: "textarea#elm1",
                    theme: "modern",
                    menubar:false,
                    statusbar: false,
                    height:300,
                    plugins: [
                        "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                        "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                        "save table contextmenu directionality emoticons template paste textcolor"
                    ],
                    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link  | print preview media fullpage | forecolor backcolor",
                    style_formats: [
                        {title: 'Bold text', inline: 'b'},
                        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                        {title: 'Example 1', inline: 'span', classes: 'example1'},
                        {title: 'Example 2', inline: 'span', classes: 'example2'},
                        {title: 'Table styles'},
                        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
                    ]
                });
            }

            if($("#type_article").val()=='text'){
                $("#vdAvant").hide();
                $("#imgAvant").show();
            }else{
                $("#imgAvant").hide();
                $("#vdAvant").show();
                loadVideoImg();
            }
        });

        var loadFile = function(event) {
            $("#output").show();
            var reader = new FileReader();
            reader.onload = function(){
                var output = document.getElementById('output');
                output.src = reader.result;
            };
            reader.readAsDataURL(event.target.files[0]);
        };

        $('#cat_id').change(function() {
            var idarrond = $('#cat_id :selected').val();
            var url ="{{ url('get/'.'id'.'/scategorie')}}";
            url=url.replace('id',idarrond);
            $.get(url, function (data) {
                //console.log(data);
                optionData ='';
                optionData+='<option value="">Choisir une sous catégorie</option>';
                for (var i = 0; i < data.length; i++){
                    optionData+='<option value="'+data[i].id+'">'+data[i].libelle+'</option>';
                }
                $('#scat_id').html(optionData);
            });

        });

    </script>
@endsection


@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">

                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Ajouter un magasin</h4>
                        <ol class="breadcrumb"> </ol>
                    </div>
                </div>

                <div class="row">
                    <form class="form-horizontal" method="post" action="{{route('magasins.store')}}" role="form">
                        @csrf
                        <div class="col-md-8">
                            <div class="card-box">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="col-md-12" for="nom">Nom <span class="text-danger">*</span></label>
                                                <input type="text" name="nom" class="form-control col-md-12" value="{{old('nom')}}" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="col-md-12" for="adresse">Adresse <span class="text-danger">*</span></label>
                                                <input type="text" name="adresse" class="form-control col-md-12" value="{{old('adresse')}}" required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="col-md-12" for="password">Description</label>
                                                <textarea name="description" id="password" class="form-control col-md-12">{{old('description')}}</textarea>
                                            </div>
                                        </div>

                                        <div class="card text-white bg-danger text-xs-center">
                                            <div class="card-body">
                                                <blockquote class="card-bodyquote mb-0 text-center">
                                                    <p>La création du magasin entraînera l'assignation des produits au magasin.</p>
                                                </blockquote>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">

                            <div class="col-md-12">
                                <div class="card-box">
                                    {{--<h4 class="m-t-0 m-b-20 header-title"><b>Publier</b></h4>
                                    <hr>--}}
                                    <div class="row">
                                        <button type="submit" class="btn btn-primary btn-md waves-effect waves-light" name="btn" value="add"><i class="fa fa-check-square"></i> Enregistrer</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div> <!-- container -->
        </div> <!-- content -->
    </div>
@endsection
