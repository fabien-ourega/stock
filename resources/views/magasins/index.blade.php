@extends('layouts.app')

@section('title')
    Magasins
    @parent
@stop

@section('header_styles')
    <!-- DataTables -->
    <link href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('footer_scripts')
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.js') }}"></script>

    <script src="{{ asset('assets/pages/datatables.init.js') }}"></script>

    <!-- Sweet-Alert  -->
    <script src="{{ asset('assets/pages/jquery.sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/select2/js/select2.min.js') }}"></script>
    {{--<script src="{{ asset('assets/pages/jquery.sweet-alert.init.js') }}"></script>--}}

    <script type="text/javascript">
        $(document).ready(function () {
            $('.select2').select2();

            $('#myTable').dataTable({
                language: {
                    lengthMenu: "_MENU_",
                    search: "_INPUT_",
                    searchPlaceholder: "Recherche",
                    sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                    sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    sInfoPostFix:    "",
                    sLoadingRecords: "Chargement en cours...",
                    sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
                    sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
                    sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    oPaginate: {
                        "sFirst":    "Premier",
                        "sLast":    "Dernier",
                        "sNext":    "Suivant",
                        "sPrevious": "Précédent"
                    },
                }
            });

        });

        $('.actus').on('click', '.delete', function (event) {
            event.preventDefault();
            var href = $(this).attr('href');
            swal({
                title: "Êtes-vous sûr?",
                text: "Voulez vous vraiment supprimer cet produit",
                icon: "warning",
                buttons: true,
                buttons: ["Annuler", "Oui"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    window.location = href;
                }
            });
        });

        $('#check_all').on('click', function(e){
            if($(this).is(':checked',true)){
                $(".role_item").prop('checked', true);
            }else{
                $(".role_item").prop('checked', false);
            }
        });

        $('#sendActionGroup').on('click', function(e) {

            if($('#actionGroup').val()=='del_select'){
                var allVals = [];
                $(".role_item:checked").each(function() {
                    allVals.push($(this).attr('data-id'));
                });
                //console.log(allVals.length);
                // return false;
                if(allVals.length <=0)
                {
                    swal("Oops","Veuillez selectionnez des lignes à supprimer!","error" )
                }
                else {
                    //$("#loading").show();
                    swal({
                        title: "Êtes-vous sûr?",
                        text: "Voulez vous vraiment supprimer ces lignes",
                        icon: "warning",
                        buttons: true,
                        buttons: ["Annuler", "Oui"],
                        dangerMode: true,
                    }).then((willDelete) => {
                        if(willDelete) {
                            var valueIds = allVals.join(",");

                            $.ajax({
                                url:"{{ route('news.deletes') }}",
                                method:"GET",
                                data:{value:valueIds},
                                success:function(res){
                                    console.log(res);
                                    if(res == '1'){
                                        //$("#loading").hide();
                                        location.reload();
                                    }else{
                                        //$("#loading").hide();
                                        location.reload();
                                    }
                                }
                            })
                        }
                    });
                }
            }else{
                return false;
                //swal("Oops","Something went wrong!","error" )
            }

        });

        function showform(){
            $('#etats').show();
            $('#showform').hide();
            $('#hideform').show();
        }
        function hideform(){
            $('#etats').hide();
            $('#hideform').hide();
            $('#showform').show();
        }

    </script>
@endsection


@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="page-title">
                            Magasins
                            <a href="{{route('magasins.create')}}" class="btn btn-default btn-xs">Ajouter</a>
                            <a href="javascript:void(0);" data-toggle="modal" data-target="#myModal" class="btn btn-default btn-xs">Upload</a>
                        </h4>
                        @if(Auth::user()->role_id!=2)
                            <div class="pull-right">
                                <button onclick="showform()" id="showform"  class="btn btn-default btn-xs"><i class="fa fa-bar-chart"></i> Etat</button>
                                <button onclick="hideform()" id="hideform" class="btn btn-default btn-xs" style="display: none;"><i class="fa fa-bar-chart"></i> Fermer </button>
                            </div>
                        @endif
                        <ol class="breadcrumb"> </ol>
                    </div>
                </div>

                <div style="display: none;" id="etats" class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="standard-modalLabel">Edition d'un etat <br>
                                <small class="text-success"><span id="lbbe"></span></small>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </h4>
                        </div>
                        <form id="formAdd" action="{{route('etats.magasin')}}" class="form-horizontal" method="post" autocomplete="off">
                            @csrf
                            <div class="modal-body">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label class="col-md-12" for="password">Magasin</label>
                                        <select name="magasin[]" id="magasin" class="form-control select2" multiple required>
                                            <option value="all">TOUS LES MAGASINS</option>
                                            @foreach($magasins as $item)
                                                <option value="{{$item->id}}">{{$item->nom}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group m-b-20">
                                    <div class="col-md-12">
                                        <div class="radio radio-inline">
                                            <input type="radio" id="inlineRadio" value="0" name="nbpdtd" checked="">
                                            <label for="inlineRadio"> Tous les produits </label>
                                        </div>
                                        <div class="radio radio-inline">
                                            <input type="radio" id="inlineRadi" value="1" name="nbpdtd">
                                            <label for="inlineRadi"> Produits disponible </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                        <div class="col-md-12">
                                        <div class="radio radio-inline">
                                            <input type="radio" id="inlineRadio4" value="pdf" name="typexport" checked="">
                                            <label for="inlineRadio4"> PDF </label>
                                        </div>
                                        <div class="radio radio-inline">
                                            <input type="radio" id="inlineRadio5" value="excel" name="typexport">
                                            <label for="inlineRadio5"> EXCEL </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Etat</button>
                            </div>
                        </form>
                    </div><!-- /.modal-content -->
                </div>

                <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="standard-modalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="standard-modalLabel">Fichier pdt<br>
                                    <small class="text-success"><span id="tlbbe"></span></small>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                </h4>
                            </div>
                            <form enctype="multipart/form-data" action="{{route('produits.excel')}}" class="form-horizontal" method="post" autocomplete="off">
                                @csrf
                                <input type="hidden" id="tidsluge" name="tidsluge">
                                <div class="modal-body">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label class="col-md-12" for="password">Fichier <span class="text-danger">*</span></label>
                                            <input type="file" name="file" class="form-control col-md-12" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-light" data-dismiss="modal">Annuler</button>
                                    <button type="submit" class="btn btn-primary">Ajouter</button>
                                </div>
                            </form>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div>


                <div class="row">
                    <div class="col-md-12">
                        <div class="card-box table-responsive">
                            <table id="myTable" class="table table-striped table-bordered actus">
                                <thead>
                                <tr>
                                    <th>Ref.No.</th>
                                    <th>Nom</th>
                                    <th>Adresse</th>
                                    <th>Nombre de produits</th>
                                    <th>Montant</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($magasins as $k=>$item)
                                @php $mtt = (new \App\Http\Controllers\StocksController())->getMontantTotal($item->id,'magasin'); @endphp
                                    <tr>
                                        <td>0{{$k+1}} </td>
                                        <td style="font-weight: bold">
                                            {{$item->nom}}
                                            <div class="row-actions-edit">
                                                @if (in_array('del_pdt', Session::get('permissions')))
                                                <span class="edit"> <a href="{{route('magasins.edit',$item->id)}}">Modifier</a> | </span>
                                                <span class="trash"><a href="{{route('magasins.destroy',$item->id)}}" class="delete" style="color: red">Supprimer</a> </span>
                                                @endif
                                                {{--<span class="view"><a href="" rel="afficher">Afficher</a></span>--}}
                                            </div>
                                        </td>
                                        <td>{{$item->adresse}}</td>
                                        <td>{{$item->qte}}</td>
                                        <td>@price($mtt) Fcfa</td>
                                        <td>
                                            <a href="{{route('magasins.show',$item->id)}}" class="btn btn-primary btn-sm"><i class="fa fa-cogs"></i> GERER</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div> <!-- container -->
        </div> <!-- content -->
    </div>
@endsection
