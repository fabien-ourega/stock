<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<style type="text/css" media="all">
    @page { size: auto;  margin: 0mm; }

    body{
        font-size: 10px;
        margin: 1.5px;
    }
    table {
        font-size: 11px;
    }

    .td{
        border: 1px solid #000000;
        padding-left: 5px;
        padding: 10px;
    }
    .tdd{
        padding: 3px;
        border-bottom: 1px solid #e2e2e2;
    }
    .txt-center{
        text-align: center;
    }
    .txt-right{
        text-align: right;
    }
    .pr-20{
        padding-right: 20px;
    }
    .mb-20{
        margin-bottom: 20px;
    }
    #resultscol td{
        border: 1px solid #000000;
        padding: 3px;
    }

    #fin ul li{
        list-style: none;
        text-align: left;
    }

    .custom-control-label::before,
    .custom-control-label::after {
        top: 0.1rem !important;
        left: -2rem !important;
        width: 1.25rem !important;
        height: 1.25rem !important;
    }

    footer {
        position: fixed;
        bottom: 0cm;
        left: 0cm;
        right: 0cm;
        height: 2cm;

        color: #000;
        font-size: 10px;
        text-align: center;
    }

    .breakNow{
        page-break-inside:avoid;
        page-break-after:always;
    }

</style>
<body >
<div class="breakNow" id="gsetat">
    <table border="0" cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td>
                <div class="col">
                    <img src="{{asset('assets/images/logo_dark.png')}}" data-holder-rendered="true" height="50px" />

                </div>
            </td>

        </tr>
    </table>
    <br><br><br>

    @foreach($customerArray as $data)
        @php $magasin = \App\Magasin::find($data);
            $stocks = \App\Bstock::where('magasin_id',$data)->has('produit')->with('produit')->get();

            if($selectnbpdt==1){
                $stocks = \App\Bstock::where('magasin_id',$data)->where('qte','<>',0)->has('produit')->with('produit')->get();
            }
        @endphp

        @if($magasin)
            <table cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td align="center">
                        <p style="font-size: 18px;font-weight: bold;margin-bottom: 10px">ETAT DES MAGASINS {{$magasin->nom}}</p>
                    </td>
                </tr>
            </table>

            <table style="" class="mb-20" cellspacing="0" cellpadding="0" align="center" width="100%">
                @php $stotal1 = 0; @endphp
                <tr style="text-transform: uppercase;background-color: #e1e1e8">
                    <td class="tdd"><strong>Ref.No.</strong></th>
                    <td class="tdd"><strong>Nom</strong></th>
                    <td class="tdd"><strong>Unité</strong></th>
                    <td class="tdd"><strong>Catégorie </strong></th>
                    <td class="tdd"><strong>P.U </strong></th>
                    <td class="tdd"><strong>Disponible</strong></th>
                    <td class="tdd"><strong>Montant</strong></th>
                </tr>

                @foreach($stocks as $k=>$produit)
                    @if($produit->produit)
                        @php
                            $cat = \App\Categorie::where('id',$produit->produit->categorie_id)->first();
                            $type = \App\Typeproduit::where('id',$produit->produit->type_id)->first();
                        @endphp

                        <tr>
                            <td class="td">#0{{$k+1}} </td>
                            <td class="td">{{$produit->produit->nom}}</td>
                            <td class="td">{{$type->libelle}}</td>
                            <td class="td">{{$cat->libelle}}</td>
                            <td class="td">@price($produit->produit->price) Fcfa</td>

                            @php $nb = $produit->qte @endphp
                            <td class="td">
                                @if($nb < 5)@price($nb) @else @price($nb) @endif
                            </td>

                            <td class="td"> @php $total = $nb * $produit->produit->price; @endphp
                                @price($total) Fcfa
                            </td>
                        </tr>
                    @endif
                @endforeach
            </table>
        @endif

        {{-- break page --}}
        <div class="breakNow"></div>
    @endforeach

</div>
{{--<footer>
    Cocody Angré, 8ème Tranche, Résidence Pacific Villa 73, 01 BP 473 Abidjan 01 <br>
    Tél. (225)06 32 86 36 / (225) 59 59 09 91, RC : CI-ABJ-2018-B-14915, CC : 18 29 457 U, Capital Social : 10.000.000 FCFA, Régime d'imposition : Réel simplifié
</footer>--}}

<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
<script>
    // $(document).ready(function () {
    //     //alert('demo');
    //     //window.print();
    //     var restorepage = document.body.innerHTML;
    //     var printcontent = document.getElementById("gsetat").innerHTML;
    //     document.body.innerHTML = printcontent;
    //     window.print();
    //     document.body.innerHTML = restorepage;
    // });
</script>

</body>
</html>
