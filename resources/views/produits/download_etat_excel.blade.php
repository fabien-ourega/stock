<html>
<style>
    body{
        font-size: 10px;
        margin: 1.5px;
    }
    table {
        font-size: 11px;
    }

    .td{
        border: 1px solid #000000;
        padding-left: 5px;
        padding: 10px;
    }
    .tdd{
        padding: 3px;
        border-bottom: 1px solid #e2e2e2;
    }
    .txt-center{
        text-align: center;
    }
    .txt-right{
        text-align: right;
    }
    #resultscol td{
        border: 1px solid #000000;
        padding: 3px;
    }

    #fin ul li{
        list-style: none;
        text-align: left;
    }

    .custom-control-label::before,
    .custom-control-label::after {
        top: 0.1rem !important;
        left: -2rem !important;
        width: 1.25rem !important;
        height: 1.25rem !important;
    }

    footer {
        position: fixed;
        bottom: 0cm;
        left: 0cm;
        right: 0cm;
        height: 2cm;

        /** Extra personal styles **/
        color: #000;
        font-size: 10px;
        text-align: center;
    }

</style>
<body>
<div>

    <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td align="center">
                <p style="font-size: 18px;font-weight: bold">ETAT DES PRODUITS</p>
            </td>
        </tr>

    </table>
    <br>
    <table style="" cellspacing="0" cellpadding="0" align="center" width="100%">
        <tr style="background-color: #e1e1e8">
            <td class="td"><strong>NUM</strong></td>
            <td class="td"><strong>CODE</strong></td>
            <td class="td"><strong>NOM</strong></td>
            <td class="td"><strong>TYPE</strong></td>
            <td class="td"><strong>PRIX UNITAIRE</strong></td>
            <td class="td"><strong>CATEGORIE</strong></td>
            <td class="td"><strong>DATE D AJOUT</strong></td>
        </tr>
        @php $sstotal = 0 @endphp
        @foreach($datas as $k=>$data)
            <tr class="">
                <td class="td">{{$k+1}}</td>
                <td class="td">{{$data->code}}</td>
                <td class="td"><strong>{{$data->nom}}</strong></td>
                <td class="td">{{$data->type->libelle}}</td>
                <td class="td">@price($data->price) Fcfa</td>
                <td class="td">{{$data->categorie->libelle}}</td>
                <td class="td">{{$data->created_at->format('d-m-Y')}}</td>
            </tr>
        @endforeach

    </table>
    <br>
</div>

</body>
</html>
