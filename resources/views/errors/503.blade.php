@extends('errors::minimal')

@section('title', __('Site en maintenance'))
@section('code', '503')
@section('message', __($exception->getMessage() ?: 'Site en maintenance'))
