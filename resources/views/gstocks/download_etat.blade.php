<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<style type="text/css" media="all">
    @page { size: auto;  margin: 0mm; }

    body{
        font-size: 10px;
        margin: 1.5px;
    }
    table {
        font-size: 11px;
    }

    .td{
        border: 1px solid #000000;
        padding-left: 5px;
        padding: 10px;
    }
    .tdd{
        padding: 3px;
        border-bottom: 1px solid #e2e2e2;
    }
    .txt-center{
        text-align: center;
    }
    .txt-right{
        text-align: right;
    }
    .pr-20{
        padding-right: 20px;
    }
    .mb-20{
        margin-bottom: 20px;
    }
    #resultscol td{
        border: 1px solid #000000;
        padding: 3px;
    }

    #fin ul li{
        list-style: none;
        text-align: left;
    }

    .custom-control-label::before,
    .custom-control-label::after {
        top: 0.1rem !important;
        left: -2rem !important;
        width: 1.25rem !important;
        height: 1.25rem !important;
    }

    footer {
        position: fixed;
        bottom: 0cm;
        left: 0cm;
        right: 0cm;
        height: 2cm;

        color: #000;
        font-size: 10px;
        text-align: center;
    }

    .breakNow{
        page-break-inside:avoid;
        page-break-after:always;
    }

</style>
<body >
<div class="breakNow" id="gsetat">
    <table border="0" cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td>
                <div class="col">
                    <img src="{{asset('assets/images/logo_dark.png')}}" data-holder-rendered="true" height="50px" />

                </div>
            </td>

        </tr>
    </table>
    <br><br><br>
    <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td align="center">
                <p style="font-size: 18px;font-weight: bold">ETAT DES STOCKS</p>
            </td>
        </tr>

    </table>

    <table style="" class="mb-20" cellspacing="0" cellpadding="0" align="center" width="100%">
        <tr style="background-color: #e1e1e8">
            <td class="td" style="width: 130px"><strong>STOCK</strong></td>
            <td class="td" style="width: 130px"><strong>CATEGORIE</strong></td>
            <td class="td"><strong>PRODUITS</strong></td>
        </tr>

    @foreach($customerArray as $data)

        @if($data == 0)
            @php
                if(count($selectcatId)>0){
                    $cats = \App\Categorie::whereIn('id',$selectcatId)->with('produits')->has('produits')->get();
                    // $cats = \App\Categorie::whereIn('id',$selectcatId)->with('produits')->has('produits')->get()->sortBy(function($query){return $query->produits->nom; });
                }else{
                    $cats = \App\Categorie::with('produits')->has('produits')->get();
                    // $cats = \App\Categorie::with('produits')->has('produits')->get()->sortBy(function($query){return $query->produits->nom; });
                }
                $stotal = 0 ;
            @endphp
            @if(count($cats)>0)


                    @foreach($cats as $j=>$cat)
                    @php $stotal1 = 0 ; @endphp
                    <tr class="">
                        @if($j==0)
                        <td class="td" rowspan="{{count($cats)}}">INTERNE</td>
                        @endif
                        <td class="td">{{$cat->libelle}}</td>
                        <td class="td">
                            <table style="" cellspacing="0" cellpadding="0" align="center" width="100%">
                                <tr style="text-transform: uppercase">
                                    <td class="tdd"><strong>PRODUIT</strong></td>
                                    <td class="tdd"><strong>PRIX UNITAIRE</strong></td>
                                    <td class="tdd"><strong>DISPONIBLE</strong></td>
                                    <td class="tdd"><strong>MONTANT</strong></td>
                                </tr>
                                @foreach($cat->produits as $k=>$pdt)
                                    @php
                                        $bstock = \App\Bstock::where('pdt_id',$pdt->id)->whereNull('pjt_id')->first();
                                        $nb =$bstock ? $bstock->qte : '0';
                                        $total = $nb * $pdt->price;
                                        $stotal1 = $stotal1 +$total;
                                    @endphp
                                    <tr>
                                        <td class="tdd">{{$pdt->nom}}</td>
                                        <td class="tdd">@price($pdt->price) Fcfa</td>
                                        <td class="tdd">{{$nb}}</td>
                                        <td class="tdd">@price($total) Fcfa</td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td class="tdd txt-right pr-20" colspan="3"><strong>TOTAL</strong></td>
                                    @php $stotal = $stotal + $stotal1 @endphp
                                    <td class="tdd"><strong>@price($stotal1) Fcfa</strong></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    @endforeach
                    <tr>
                        <td class="td txt-right" colspan="2">TOTAL</td>
                        <td class="td txt-right pr-20"><strong>@price($stotal) Fcfa</strong></td>
                    </tr>

            @endif

        @else
            @php
                if(count($selectcatId)>0){
                    $cats = \App\Bstock::where('pjt_id',$data)->with('project')->has('produit')->with(['produit' => function($q) use($selectcatId){
                        $q->whereIn('categorie_id',$selectcatId);
                    }])->get();

                    if($selectnbpdt==1){
                        $cats = \App\Bstock::where('pjt_id',$data)->where('qte','<>',0)->with('project')->has('produit')->with(['produit' => function($q) use($selectcatId){
                            $q->whereIn('categorie_id',$selectcatId);
                        }])->get();
                    }

                }else{
                    $cats = \App\Bstock::where('pjt_id',$data)->with('project')->with('produit')->has('produit')->get();
                    if($selectnbpdt==1){
                        $cats = \App\Bstock::where('pjt_id',$data)->where('qte','<>',0)->with('project')->with('produit')->has('produit')->get();
                    }
                }
                $pdtId = $cats->pluck('produit.id');
                $listpdt = \App\Produit::whereIn('id',$pdtId)->with('categorie')->groupBy('categorie_id')->get();
                $stotal = 0 ;

            @endphp

            @if(count($listpdt)>0)
                    @foreach($listpdt as $j=>$cat)
                        @php $stotal1 = 0 ; @endphp
                        <tr class="">
                            @if($j==0)
                            <td class="td" rowspan="{{count($listpdt)}}">{{$cats[0]->project->libelle}}</td>
                            @endif
                            <td class="td">{{$cat->categorie->libelle}}</td>
                            <td class="td">
                                <table style="" cellspacing="0" cellpadding="0" align="center" width="100%">
                                    <tr style="text-transform: uppercase">
                                        <td class="tdd"><strong>PRODUIT</strong></td>
                                        <td class="tdd"><strong>PRIX UNITAIRE</strong></td>
                                        <td class="tdd"><strong>DISPONIBLE</strong></td>
                                        <td class="tdd"><strong>MONTANT</strong></td>
                                    </tr>

                                    @php
                                        $getidpdt =\App\Produit::whereIn('id',$pdtId)->where('categorie_id',$cat->categorie->id)->get();
                                        $mpdtId = $getidpdt->pluck('id');
                                        $cats = \App\Bstock::where('pjt_id',$data)->whereIn('pdt_id',$mpdtId)->with('produit')->get()->sortBy(function($query){return $query->produit->nom; });
                                        //dd($cats);
                                    @endphp

                                    @foreach($cats as $k=>$pdt)
                                        @php
                                            $total = $pdt->qte * $pdt->produit->price;
                                            $stotal1 = $stotal1 +$total;
                                        @endphp
                                        <tr>
                                            <td class="tdd">{{$pdt->produit->nom}}</td>
                                            <td class="tdd">@price($pdt->produit->price) Fcfa</td>
                                            <td class="tdd">{{$pdt->qte}}</td>
                                            <td class="tdd">@price($total) Fcfa</td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td class="tdd txt-right pr-20" colspan="3"><strong>TOTAL</strong></td>
                                        @php $stotal = $stotal + $stotal1 @endphp
                                        <td class="tdd"><strong>@price($stotal1) Fcfa</strong></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    @endforeach

                    <tr>
                        <td class="td txt-right" colspan="2">
                           TOTAL
                        </td>
                        <td class="td txt-right pr-20"><strong>@price($stotal) Fcfa</strong></td>
                    </tr>
            @endif

        @endif

    @endforeach
    </table>
</div>
{{--<footer>
    Cocody Angré, 8ème Tranche, Résidence Pacific Villa 73, 01 BP 473 Abidjan 01 <br>
    Tél. (225)06 32 86 36 / (225) 59 59 09 91, RC : CI-ABJ-2018-B-14915, CC : 18 29 457 U, Capital Social : 10.000.000 FCFA, Régime d'imposition : Réel simplifié
</footer>--}}

<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
<script>
    $(document).ready(function () {
        //alert('demo');
        //window.print();
        var restorepage = document.body.innerHTML;
        var printcontent = document.getElementById("gsetat").innerHTML;
        document.body.innerHTML = printcontent;
        window.print();
        document.body.innerHTML = restorepage;
    });
</script>

</body>
</html>
