@extends('layouts.app')

@section('title')
    Entrées / Sorties
    @parent
@stop

@section('header_styles')
    <!-- DataTables -->
    <link href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('footer_scripts')
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.js') }}"></script>

    <script src="{{ asset('assets/pages/datatables.init.js') }}"></script>

    <!-- Sweet-Alert  -->
    <script src="{{ asset('assets/pages/jquery.sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/imask.js') }}"></script>
    {{--<script src="{{ asset('assets/pages/jquery.sweet-alert.init.js') }}"></script>--}}

    <script type="text/javascript">
        $(document).ready(function () {
        });
    </script>
@endsection


@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="page-title">Entrées / Sorties</h4>
                        <ol class="breadcrumb"> </ol>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <section id="cd-timeline" class="cd-container">

                            @foreach($stories as $story)
                                @php
                                    if(isset($story->project)){
                                        $class = 'cd-success';
                                        $icon = 'fa fa-product-hunt';
                                        $span = 'text-success';
                                    }elseif(isset($story->produit)){
                                        $class = 'cd-danger';
                                        $icon = 'fa fa-shopping-cart';
                                        $span = 'text-danger';
                                    }else{
                                        $class = 'cd-info';
                                        $icon = 'fa fa-file';
                                        $span = 'text-info';
                                    }
                                @endphp
                                <div class="cd-timeline-block">
                                <div class="cd-timeline-img {{$class}}">
                                    <i class="{{$icon}}"></i>
                                </div>

                                <div class="cd-timeline-content">
                                    <h3>
                                    @if(isset($story->project))
                                        {{'Projet '.$story->project->libelle }}
                                    @elseif(isset($story->produit))
                                        {{$story->produit->nom}}
                                    @else
                                        Projet interne
                                    @endif
                                    </h3>

                                    <p class="">{{strtoupper($story->libelle)}}</p>
                                    @if($story->qte)
                                    <span class="{{$span}}">{{$story->qte}} quantité{{$story->qte>1?'s':''}}</span>
                                    @endif

                                    @if(!isset($story->produit))
                                    <a href="{{route('gstock.storie.item',$story->id)}}" class="btn btn-success btn-rounded waves-effect waves-light m-t-15">Voir plus</a>
                                    @endif
                                    <span class="cd-date">{{$story->created_at->format('d')}} {{$memois[$story->created_at->format('m')][0]}}</span>
                                </div>
                            </div>
                            @endforeach
                        </section>
                    </div>
                </div>

            </div> <!-- container -->
        </div> <!-- content -->
    </div>
@endsection
