@extends('layouts.app')

@section('title')
    Entrées / Sorties
    @parent
@stop

@section('header_styles')
    <!-- DataTables -->
    <link href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('footer_scripts')
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.js') }}"></script>

    <script src="{{ asset('assets/pages/datatables.init.js') }}"></script>

    <!-- Sweet-Alert  -->
    <script src="{{ asset('assets/pages/jquery.sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/imask.js') }}"></script>
    {{-- <script src="{{ asset('assets/pages/jquery.sweet-alert.init.js') }}"></script> --}}

    <script type="text/javascript">
        $(document).ready(function() {
            $('#myTable').dataTable({
                "ordering": false,
                language: {
                    lengthMenu: "_MENU_",
                    search: "_INPUT_",
                    searchPlaceholder: "Recherche",
                    sInfoEmpty: "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                    sInfoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    sInfoPostFix: "",
                    sLoadingRecords: "Chargement en cours...",
                    sZeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    sEmptyTable: "Aucune donn&eacute;e disponible dans le tableau",
                    sLengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
                    sInfo: "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    oPaginate: {
                        "sFirst": "Premier",
                        "sLast": "Dernier",
                        "sNext": "Suivant",
                        "sPrevious": "Précédent"
                    },
                }
            });
        });
    </script>
@endsection


@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="page-title">Entrées / Sorties</h4>
                        <ol class="breadcrumb"> </ol>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card-box table-responsive">
                            <table id="myTable" class="table table-striped table-bordered actus">
                                <thead>
                                    <tr>
                                        {{-- <th>Ref.No.</th> --}}
                                        <th>Ic</th>
                                        <th>Date</th>
                                        <th>Action </th>
                                        <th>Quantités</th>
                                        <th>Projet</th>
                                        <th>Produit</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($stories as $k => $story)
                                        @php
                                            if (isset($story->project)) {
                                                $class = 'cd-success';
                                                $icon = 'fa fa-product-hunt';
                                                $span = 'text-success';
                                            } else {
                                                $class = 'cd-info';
                                                $icon = 'fa fa-file';
                                                $span = 'text-info';
                                            }
                                        @endphp

                                        <tr>
                                            {{-- <td>#0{{$k+1}} </td> --}}
                                            <th>
                                                <i class="{{$icon}} {{$span}}"></i>
                                            </th>
                                            <td style="font-weight: bold">{{ $story->created_at->format('d') }}
                                                {{ $memois[$story->created_at->format('m')][0] }}
                                                {{ $story->created_at->format('Y H:i') }} </td>
                                            <td>{{ strtoupper($story->libelle) }}</td>
                                            <td><span class="{{ $span }}">{{ $story->qte }}</span></td>
                                            <td>
                                                @if (isset($story->project))
                                                    <span class="{{ $span }}">{{ 'Projet ' . $story->project->libelle }}</span>
                                                @else
                                                    <span class="{{ $span }}">Stock interne</span>
                                                @endif
                                            </td>
                                            <td>
                                                @if(isset($story->produit))
                                                    <span class="text-danger">{{ $story->produit->nom }}</span>
                                                 @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div> <!-- container -->
        </div> <!-- content -->
    </div>
@endsection
