@extends('layouts.app')

@section('title')
    Mes stocks
    @parent
@stop

@section('header_styles')
    <!-- DataTables -->
    <link href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('footer_scripts')
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.js') }}"></script>

    <script src="{{ asset('assets/pages/datatables.init.js') }}"></script>

    <!-- Sweet-Alert  -->
    <script src="{{ asset('assets/pages/jquery.sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/imask.js') }}"></script>
    {{--<script src="{{ asset('assets/pages/jquery.sweet-alert.init.js') }}"></script>--}}

    <script type="text/javascript">
        $(document).ready(function () {
            $('.select2').select2();

            IMask(document.getElementById('estock'), {
                mask: Number,
                min: 1,
                max: 100000000000,
                thousandsSeparator: ' '
            });

            IMask(document.getElementById('astock'), {
                mask: Number,
                min: 1,
                max: 100000000000,
                thousandsSeparator: ' '
            });


            $('#myTable').dataTable({
                language: {
                    lengthMenu: "_MENU_",
                    search: "_INPUT_",
                    searchPlaceholder: "Recherche",
                    sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                    sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    sInfoPostFix:    "",
                    sLoadingRecords: "Chargement en cours...",
                    sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
                    sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
                    sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    oPaginate: {
                        "sFirst":    "Premier",
                        "sLast":    "Dernier",
                        "sNext":    "Suivant",
                        "sPrevious": "Précédent"
                    },
                }
            });

            $('#myTable1').dataTable({
                language: {
                    lengthMenu: "_MENU_",
                    search: "_INPUT_",
                    searchPlaceholder: "Recherche",
                    sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                    sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    sInfoPostFix:    "",
                    sLoadingRecords: "Chargement en cours...",
                    sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
                    sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
                    sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    oPaginate: {
                        "sFirst":    "Premier",
                        "sLast":    "Dernier",
                        "sNext":    "Suivant",
                        "sPrevious": "Précédent"
                    },
                }
            });
        });

        $('.actus').on('click', '.delete', function (event) {
            event.preventDefault();
            var href = $(this).attr('href');
            swal({
                title: "Êtes-vous sûr?",
                text: "Voulez vous vraiment supprimer cet projet",
                icon: "warning",
                buttons: true,
                buttons: ["Annuler", "Oui"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    window.location = href;
                }
            });
        });

        $('#check_all').on('click', function(e){
            if($(this).is(':checked',true)){
                $(".role_item").prop('checked', true);
            }else{
                $(".role_item").prop('checked', false);
            }
        });

        $('#sendActionGroup').on('click', function(e) {

            if($('#actionGroup').val()=='del_select'){
                var allVals = [];
                $(".role_item:checked").each(function() {
                    allVals.push($(this).attr('data-id'));
                });
                //console.log(allVals.length);
                // return false;
                if(allVals.length <=0)
                {
                    swal("Oops","Veuillez selectionnez des lignes à supprimer!","error" )
                }
                else {
                    //$("#loading").show();
                    swal({
                        title: "Êtes-vous sûr?",
                        text: "Voulez vous vraiment supprimer ces lignes",
                        icon: "warning",
                        buttons: true,
                        buttons: ["Annuler", "Oui"],
                        dangerMode: true,
                    }).then((willDelete) => {
                        if(willDelete) {
                            var valueIds = allVals.join(",");

                            $.ajax({
                                url:"{{ route('users.deletes') }}",
                                method:"GET",
                                data:{value:valueIds},
                                success:function(res){
                                    console.log(res);
                                    if(res == '1'){
                                        //$("#loading").hide();
                                        location.reload();
                                    }else{
                                        //$("#loading").hide();
                                        location.reload();
                                    }
                                }
                            })
                        }
                    });
                }
            }else{
                return false;
                //swal("Oops","Something went wrong!","error" )
            }

        });

        function astock(nb,description,slug){
            $("#formAdd")[0].reset();
            $("#idslug").val(slug);
            $("#lbbe").html(description);
            $("#dispo").val(nb);
        }

        function estock(nb,description,slug){
            $("#formEdit")[0].reset();
            $("#idsluge").val(slug);
            $("#elbbe").html(description);
            $("#edispo").val(nb);
        }

        function tstock(nb,description,slug){
            $("#formTrans")[0].reset();
            $("#tidsluge").val(slug);
            $("#tlbbe").html(description);
            $("#tdispo").val(nb);
        }

        function showform(){
            $('#etats').show();
            $('#showform').hide();
            $('#hideform').show();
        }
        function hideform(){
            $('#etats').hide();
            $('#hideform').hide();
            $('#showform').show();
        }

        function showformintern(){
            $('#iteminterne').show('slow');
            //make acroll to the bottom
            $('html, body').animate({
                scrollTop: $("#iteminterne").offset().top
            }, 2000);

            $('#showformintern').hide();
            $('#hideformintern').show();
        }
        function hideformintern(){
            $('#iteminterne').hide();
            $('#hideformintern').hide();
            $('#showformintern').show();
        }

    </script>
@endsection


@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="page-title">Mes Stocks
                        </h4>
                        @if(in_array("show_stock",Session::get('permissions')))
                        <div class="pull-right">
                            <button onclick="showform()" id="showform"  class="btn btn-default btn-xs"><i class="fa fa-bar-chart"></i> Etat</button>
                            <button onclick="hideform()" id="hideform" class="btn btn-default btn-xs" style="display: none;"><i class="fa fa-bar-chart"></i> Fermer </button>
                        </div>
                        @endif
                        <ol class="breadcrumb"> </ol>
                    </div>
                </div>

                @if(in_array("show_stock",Session::get('permissions')))
                <div style="display: none;" id="etats" class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="standard-modalLabel">Edition d'un etat <br>
                                <small class="text-success"><span id="lbbe"></span></small>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </h4>
                        </div>
                        <form id="formAdd" action="{{route('etats.gstock')}}" class="form-horizontal" method="post" autocomplete="off">
                            @csrf
                            <div class="modal-body">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label class="col-md-12" for="password">Projet</label>
                                        <select name="project[]" id="project" class="form-control select2" multiple required>
                                            <option value="all">TOUS LES PROJETS</option>
                                            @foreach($projects as $project)
                                                <option value="{{$project->id}}">{{$project->libelle}}</option>
                                            @endforeach
                                            {!! count($projects)>0 ? '<option disabled role=separator>':'' !!}
                                            <option value="0">Stock Interne</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label class="col-md-12" for="password">Catégorie</label>
                                        <select name="categorie[]" id="categorie" class="form-control select2" multiple>
                                            <option disabled>Séléctionnez une ou plusieurs catégories</option>
                                            @foreach($categories as $categorie)
                                                <option value="{{$categorie->id}}">{{$categorie->libelle}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group m-b-20">
                                    <div class="col-md-12">
                                        <div class="radio radio-inline">
                                            <input type="radio" id="inlineRadio" value="0" name="nbpdtd" checked="">
                                            <label for="inlineRadio"> Tous les produits </label>
                                        </div>
                                        <div class="radio radio-inline">
                                            <input type="radio" id="inlineRadi" value="1" name="nbpdtd">
                                            <label for="inlineRadi"> Produits disponible </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                        <div class="col-md-12">
                                        <div class="radio radio-inline">
                                            <input type="radio" id="inlineRadio4" value="pdf" name="typexport" checked="">
                                            <label for="inlineRadio4"> PDF </label>
                                        </div>
                                        <div class="radio radio-inline">
                                            <input type="radio" id="inlineRadio5" value="excel" name="typexport">
                                            <label for="inlineRadio5"> EXCEL </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Etat</button>
                            </div>
                        </form>
                    </div><!-- /.modal-content -->
                </div>


                <div id="standard-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="standard-modalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="standard-modalLabel">Approvisionner <br>
                                    <small class="text-success"><span id="lbbe"></span></small>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                </h4>
                            </div>
                            <form id="formAdd" action="{{route('gstock.store.pdt')}}" class="form-horizontal" method="post" autocomplete="off">
                                @csrf
                                <div class="modal-body">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label class="col-md-12" for="password">Quantité en stock </label>
                                            <input type="text" name="dispo" id="dispo" class="form-control col-md-12" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label class="col-md-12" for="password">Quantité a approvisionner <span class="text-danger">*</span></label>
                                            <input type="text" name="astock" id="astock" class="form-control col-md-12" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <input type="hidden" name="idslug" id="idslug">
                                    <button type="button" class="btn btn-light" data-dismiss="modal">Annuler</button>
                                    <button type="submit" class="btn btn-primary">Ajouter</button>
                                </div>
                            </form>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div>

                <div id="edit-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="edit-modalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="standard-modalLabel">Réduire <br>
                                    <small class="text-success"><span id="elbbe"></span></small>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                </h4>
                            </div>
                            <form id="formEdit" action="{{route('gstock.update.pdt')}}" class="form-horizontal" method="post" autocomplete="off">
                                @csrf
                                <div class="modal-body">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label class="col-md-12" for="password">Quantité en stock </label>
                                            <input type="text" name="dispo" id="edispo" class="form-control col-md-12" readonly>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label class="col-md-12" for="password">Quantité a réduire <span class="text-danger">*</span></label>
                                            <input type="text" name="astock" id="estock" class="form-control col-md-12" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <input type="hidden" name="idslug" id="idsluge">
                                    <button type="button" class="btn btn-light" data-dismiss="modal">Annuler</button>
                                    <button type="submit" class="btn btn-primary">Modifier</button>
                                </div>
                            </form>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div>

                <div id="transfere-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="edit-modalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="standard-modalLabel">Transférer <br>
                                    <small class="text-success"><span id="tlbbe"></span></small>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                </h4>
                            </div>
                            <form id="formTrans" action="{{route('gstock.trans.pdt')}}" class="form-horizontal" method="post" autocomplete="off">
                                @csrf
                                <div class="modal-body">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label class="col-md-12" for="password">Quantité en stock </label>
                                            <input type="text" name="dispo" id="tdispo" class="form-control col-md-12" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label class="col-md-12" for="password">Quantité a tranféré <span class="text-danger">*</span></label>
                                            <input type="text" name="astock" id="tstock" class="form-control col-md-12" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label class="col-md-12">Projet <span class="text-danger">*</span></label>
                                            <select name="pjt" id="pjt" class="form-control select2" required>
                                                <option value="" selected>Choisir le projet</option>
                                                @foreach($projects as $pro)
                                                    <option value="{{$pro->id}}">{{$pro->libelle}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <input type="hidden" name="idslug" id="tidsluge">
                                    <button type="button" class="btn btn-light" data-dismiss="modal">Annuler</button>
                                    <button type="submit" class="btn btn-primary">Transféré</button>
                                </div>
                            </form>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div>
                @endif

                @if(in_array("show_stock",Session::get('permissions')))
                <div class="row">
                    <div class="col-md-12">
                        <div class="card-box table-responsive">
                            <div class="">
                                <h4 class="page-title">
                                    STOCKS PAR PROJET
                                    <ol class="breadcrumb"> </ol>
                                </h4>
                            </div>

                            <table id="myTable" class="table table-striped table-bordered actus">
                                <thead>
                                <tr>
                                    <th>Ref.No.</th>
                                    <th>Nom</th>
                                    <th>Nombre de produit </th>
                                    @if(Auth::user()->role_id!=2)
                                    <th>Montant</th>
                                    @endif
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($projects as $k=>$user)
                                    @php $mtt = (new \App\Http\Controllers\StocksController())->getMontantTotal($user->id); @endphp
                                    <tr>
                                        <td>#0{{$k+1}} </td>
                                        <td style="font-weight: bold">{{$user->libelle}}</td>
                                        <td>{{$user->produits->count()}}</td>
                                        @if(Auth::user()->role_id!=2)
                                        <td><span class="label label-info">@price($mtt) Fcfa</span></td>
                                        @endif

                                        <td>
                                            <a href="{{route('gstock.project',$user->id)}}" class="btn btn-default btn-sm waves-effect waves-light"> <i class="fa fa-plus m-r-5"></i> <span>Gérer</span> </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                @endif

                @if(1==2)
                @if(in_array("show_stock_all",Session::get('permissions')))
                <div class="row" style="border-top: 1px solid rgba(0, 0, 0, 0.1);">
                    <div class="col-md-12">
                        <div class="card-box table-responsive">
                            <div class="">
                                <div class="pull-right">
                                    <button onclick="showformintern()" id="showformintern"  class="btn btn-default btn-xs"><i class="fa fa-houzz"></i> Voir plus</button>
                                    <button onclick="hideformintern()" id="hideformintern" class="btn btn-default btn-xs" style="display: none;"><i class="fa fa-houzz"></i> Voir moins </button>
                                </div>
                                <h4 class="page-title">
                                    STOCKS INTERNE
                                    @if(Auth::user()->role_id!=2)
                                    <a href="{{route('gstock.entree.pdt')}}" class="btn btn-success btn-xs">Créer une entrée de stock</a>
                                    @endif
                                    <ol class="breadcrumb"> </ol>
                                </h4>
                            </div>

                            <table id="myTable" class="table table-striped table-bordered actus">
                                <thead>
                                <tr>
                                    <th>Ref.No.</th>
                                    <th>Nom</th>
                                    <th>Nombre de produit </th>
                                    @if(Auth::user()->role_id!=2)
                                    <th>Montant</th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody>
                                    @php $mtt = (new \App\Http\Controllers\StocksController())->getMontantTotal($user->id); @endphp
                                    <tr>
                                        <td>#01 </td>
                                        <td style="font-weight: bold">Stock interne</td>
                                        <td>{{$produits->count()}}</td>
                                        @if(Auth::user()->role_id!=2)
                                        <td><span class="label label-info">@price($produits->sum('mtotal')) Fcfa</span></td>
                                        @endif
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="row" id="iteminterne" style="border-top: 1px solid rgba(0, 0, 0, 0.1); display: none">
                    <br>
                    <div class="col-md-12">
                        <div class="card-box table-responsive">
                            <div class="">
                                <h4 class="page-title">
                                    STOCKS INTERNE
                                    @if(Auth::user()->role_id!=2)
                                    <a href="{{route('gstock.entree.pdt')}}" class="btn btn-success btn-xs">Créer une entrée de stock</a>
                                    @endif
                                    <ol class="breadcrumb"> </ol>
                                </h4>
                            </div>

                            <table id="myTable1" class="table table-striped table-bordered actus">
                                <thead>
                                <tr>
                                    <th>Ref.No.</th>
                                    <th>Nom</th>
                                    <th>Catégorie </th>
                                    @if(Auth::user()->role_id!=2)
                                    <th>P.U </th>
                                    @endif
                                    <th>Disponible</th>
                                    @if(Auth::user()->role_id!=2)
                                    <th>Montant</th>
                                    @if(in_array("appro_stock",Session::get('permissions')))
                                    <th>Actions </th>
                                    @endif
                                    @endif
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($produits as $k=>$produit)
                                    <tr>
                                        <td>#0{{$k+1}} </td>
                                        <td style="font-weight: bold">
                                            {{$produit->nom}} <br>
                                            <small class="text-success">{{$produit->type->libelle}}</small>
                                        </td>
                                        <td>{{$produit->categorie->libelle}}</td>
                                        @if(Auth::user()->role_id!=2)
                                        <td>@price($produit->price) Fcfa</td>
                                        @endif
                                        @php $bstock = \App\Bstock::where('pdt_id',$produit->id)->whereNull('pjt_id')->first(); $nb =$bstock ? $bstock->qte : '0'  @endphp
                                        <td>
                                            @if($nb < 5)
                                                <span class="label label-danger">@price($nb)</span>
                                            @else
                                                <span class="label label-success">@price($nb)</span>
                                            @endif
                                        </td>
                                        @if(Auth::user()->role_id!=2)
                                        <td>
                                            @php $total = $nb * $produit->price; @endphp
                                            <span class="label label-info">@price($total) Fcfa</span>
                                        </td>
                                        @if(in_array("appro_stock",Session::get('permissions')))
                                        <td>
                                            <button data-toggle="modal" data-target="#standard-modal" class="btn btn-default btn-sm waves-effect waves-light" onclick='astock("{{$nb}}","{{$produit->nom}}","{{$produit->id}}")'> <i class="fa fa-plus m-r-5"></i> <span>Approvisionner</span> </button>
                                            @if($nb>0)
                                            <button data-toggle="modal" data-target="#edit-modal" class="btn btn-danger btn-sm waves-effect waves-light" onclick='estock("{{$nb}}","{{$produit->nom}}","{{$produit->id}}")'> <i class="fa fa-minus m-r-5"></i> <span>Réduire</span> </button>
                                            <button data-toggle="modal" data-target="#transfere-modal" class="btn btn-primary btn-sm waves-effect waves-light" onclick='tstock("{{$nb}}","{{$produit->nom}}","{{$produit->id}}")'> <i class="fa fa-reply m-r-5"></i> <span>Transférer</span> </button>
                                            @endif
                                        </td>
                                        @endif
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                @endif
                @endif


            </div> <!-- container -->
        </div> <!-- content -->
    </div>
@endsection
