@extends('layouts.app')

@section('title')
    Entree en stock
    @parent
@stop

@section('header_styles')
    {{--<link href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>--}}
    <link href="{{ asset('assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css"/>
    <style>
        .metable {
            margin: 30px 0;
            border: 1px solid green;
        }
    </style>
@endsection

@section('footer_scripts')
    <!-- Sweet-Alert  -->
    <script src="{{ asset('assets/pages/jquery.sweetalert.min.js') }}"></script>
    {{--<script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>--}}
    <!--form validation init-->
    <script src="{{ asset('assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js') }}" type="text/javascript"></script>

    <script src="{{ asset('assets/plugins/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-select/js/bootstrap-select.min.js') }}"></script>

    <script>
        var valueSelect = '',
            inputs = [],
            arr = [],
            x = 0;

        $(document).ready(function () {
            $('.select2').select2();
        });

        $('#cats').change(function() {

            var idcat = $('#cats :selected').val();
            var projetcid = $('#projectid').val();

            var url ="{{ url('get/'.'id'.'/'.'pjt'.'/produits/cat')}}";
            url=url.replace('id',idcat);
            url=url.replace('pjt',projetcid);
            $.get(url, function (data) {
                //console.log(data);
                optionData ='';
                optionData+='<option selected disabled>Choisir le produit</option>';
                for (var i = 0; i < data.length; i++){
                    optionData+='<option value="'+data[i].produit.id+'" data-libelle="'+data[i].produit.nom+'">'+data[i].produit.nom+'</option>';
                }
                $('#pdt').html(optionData);
            });

        });


        function getQteDispo(quantite,idbstock) {

            var url ="{{ url('get/'.'valmontant'.'/'.'attribut'.'/pdt/dispo')}}";
            url=url.replace('valmontant',quantite);
            url=url.replace('attribut',idbstock);

            return $.ajax({url:url, type:'GET'});
        }

        function add_element_to_array(){
            var qtite = $("#qte").val();
            var produits = $("#pdt").val();
            //console.log(produits);

            if(qtite == "" || qtite<0){
                swal("Oups!", "Veuillez renseigner la quantité", "error");
                return false ;
            }else if(produits == ""){
                swal("Oups!", "Veuillez choisir un produit ", "error");
                return false ;
            } else{
                var valueSelect = $('#pdt');
                var selectId = valueSelect.val(),
                    selectLibell = valueSelect.find(':selected').attr('data-libelle');

                //console.log(selectId,selectLibell);
                arr = {id:selectId , libelle:selectLibell, qte:qtite};
                inputs.push(arr);
                showhtml();
                /*var request = getQteDispo(qtite,selectId);

                $.when(request).done(function(data) {
                    //console.log(data);
                    if(data == "error"){
                        swal("Oups!", "Une erreur s'est produit, veuillez recommencez ", "error");
                        return false ;
                    }else if (data == 'ok'){
                        arr = {id:selectId , libelle:selectLibell, qte:qtite};
                        inputs.push(arr);
                        //console.log(arr);
                        showhtml();
                    }else{
                        //il manque
                        swal("Oups!", "Quantité disponible : "+data , "error");
                        return false ;
                    }
                });*/
            }
        }

        function showhtml() {
            $("#data_session").empty();
            var table = "<tbody>";
            //console.log(inputs);
            table += '<tr>';
            table += '<td class="border-b">NUM</td>';
            table += '<td class="border-b text-lg font-medium">PRODUIT</td>';
            table += '<td class="border-b text-lg font-medium">QUANTITE</td>';
            table += '<td class="border-b">ACTION</td>';
            table += '</tr>';

            for (var i = 0; i < inputs.length; i++) {
                var y = i + 1;

                table += '<tr data-row-id='+i+'>';
                table += '<td class="border-b">' + y + '</td>';
                table += '<td class="border-b">' + inputs[i].libelle + '</td>';
                table += '<td class="border-b">' + inputs[i].qte + '</td>';
                table += '<td class="border-b"><a href="#" onclick="deleteSession('+i+')" class="inline-block text-1xl text-orange-500 mx-2" title="Supprimer la ligne"><i data-feather="trash" class="w-4 h-4 mr-2"></i> Supprimer</a></div>';
                table +='<input type="hidden" value="' + inputs[i].id + ';' + inputs[i].libelle + ';' + inputs[i].qte +'" name="datasession[]">';
                table += '</tr>';
            };

            /*table += '<tr>';
             table += '<td class="border-b"></td>';
             table += '<td class="border-b text-lg font-medium">Montant Total</td>';
             table += '<td class="border-b text-lg font-medium" id="mttotal"></td>';
             table += '<td class="border-b"></td>';
             table += '</tr>';*/

            table += '</tbody>';

            $("#data_session").append(table);
        }

        function deleteSession(id) {
            inputs.splice(id,1);
            showhtml();
        }

        function submitForm() {
            event.preventDefault();
            if($('#libell').val()==''){
                swal("Oups!", "Veuillez renseigner le champ Nom.");
            }else if($('#pjt').val()==''){
                swal("Oups!", "Veuillez renseigner le champ Projet.");
            }else{
                var href = $(this).attr('href');
                swal({
                    title: "Êtes-vous sûr?",
                    text: "Vous êtes sur le point de faire une entre en stock ! Cette action est irréversible ? ",
                    icon: "warning",
                    buttons: true,
                    buttons: ["Annuler", "Oui"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        document.getElementById('addbtn').disabled = true;
                        document.getElementById('addappelfonds').submit();
                    }
                });
            }

        }

    </script>
@endsection


@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">

                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Ajouté une entrée de stock</h4>
                        <ol class="breadcrumb"> Projet : <span class="text-success">{{$project->libelle}}</span></ol>
                    </div>
                </div>

                <div class="row">
                    <form enctype="multipart/form-data" id="addappelfonds" class="form-horizontal" method="post" action="{{route('gstock.entree.pjt.store')}}" role="form">
                        @csrf
                        <div class="col-md-12">
                            <div class="card-box">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="col-sm-12">
                                                    <label class="col-md-12">Catégorie <span class="text-danger">*</span></label>
                                                    <select name="cats" id="cats" class="form-control select2">
                                                        <option disabled selected>Choisir la catégorie</option>
                                                        @php
                                                            $idpdts = \App\Produit::whereIn('id',$produitids)->with('categorie')->groupBy('categorie_id')->get();
                                                        @endphp
                                                        @foreach($idpdts as $idpdt)
                                                            @if($idpdt->categorie)
                                                                <option value="{{$idpdt->categorie->id}}" data-libelle="{{$idpdt->categorie->libelle}}">{{$idpdt->categorie->libelle}}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="col-sm-12">
                                                    <label class="col-md-12">Produits <span class="text-danger">*</span></label>
                                                    <select name="pdt" id="pdt" class="form-control select2">
                                                        {{--<option value="" selected>Choisir le produit</option>
                                                        @foreach($produits as $produit)
                                                            @if($produit->produit)
                                                                <option value="{{$produit->produit->id}}" data-libelle="{{$produit->produit->nom}}">{{$produit->produit->nom}}</option>
                                                            @endif
                                                        @endforeach--}}
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="col-md-12">
                                                    <label class="col-md-12" for="qte">Quantité <span class="text-danger">*</span></label>
                                                    <input type="number" min="1" id="qte" name="qte" class="form-control col-md-12" required>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="col-span-12">
                                                    <label class="col-md-12" style="opacity:0;">-</label>
                                                    <a onclick="add_element_to_array()" class="btn btn-primary btn-md waves-effect waves-light"><i class="fa fa-plus"></i> AJOUTER</a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="w-full px-3 m-3 py-2 border border-solid border-red-200 rounded-sm">
                                            <input type="hidden" id="varieMontant">
                                            <input type="hidden" id="projectid" value="{{$project->id}}" name="projectid">
                                            <table class="table metable" id="data_session">
                                            </table>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="text-right mt-5">
                                                <button id="addbtn" type="button" onclick="submitForm()" class="btn btn-success text-white"><i class="fa fa-save"></i> Enregistrer</button>
                                                <a href="#" class="btn btn-default text-gray-700 mr-1">Annuler</a>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div> <!-- container -->
        </div> <!-- content -->
    </div>
@endsection
