@extends('layouts.app')

@section('title')
    Factures en attente de traitement
    @parent
@stop

@section('header_styles')
    <!-- DataTables -->
    <link href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/switchery/css/switchery.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css"/>

@endsection

@section('footer_scripts')
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.js') }}"></script>

    <script src="{{ asset('assets/pages/datatables.init.js') }}"></script>

    <!-- Sweet-Alert  -->
    <script src="{{ asset('assets/pages/jquery.sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/switchery/js/switchery.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#myModal').on('shown.bs.modal', function() {
                $(document).off('focusin.modal');
            });

            $('.select2').select2();

            $('#myTable').dataTable({
                language: {
                    lengthMenu: "_MENU_",
                    search: "_INPUT_",
                    searchPlaceholder: "Recherche",
                    sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                    sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    sInfoPostFix:    "",
                    sLoadingRecords: "Chargement en cours...",
                    sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
                    sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
                    sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    oPaginate: {
                        "sFirst":    "Premier",
                        "sLast":    "Dernier",
                        "sNext":    "Suivant",
                        "sPrevious": "Précédent"
                    },
                }
            });

            $("#modepaie").change(function() {
                var valeur = $('#modepaie').val();
                //console.log(valeur);
                $('#avance_espece').hide();
                $('#avance_caisse').hide();

                if(valeur == 1){
                    //Chèque
                    $('#cheque').show();
                    $('#virement').hide();
                    $('#avance').hide();

                }else if(valeur==2){
                    //Virement
                    $('#cheque').hide();
                    $('#virement').show();
                    $('#avance').hide();

                }else if(valeur==3){
                    //Avance
                    $('#cheque').hide();
                    $('#virement').hide();
                    $('#avance').show();

                }else {
                    $('#cheque').hide();
                    $('#virement').hide();
                    $('#avance').hide();
                }
            });

            $("#avance").change(function() {
                var valeur = $('#avance_select').val();
                //console.log(valeur);

                if(valeur == 'espece'){
                    //espece
                    $('#avance_espece').hide();
                    $('#avance_caisse').show();

                }else if(valeur == 'cheque'){
                    $('#avance_espece').show();
                    $('#avance_caisse').hide();
                }else {
                    $('#avance_espece').hide();
                    $('#avance_caisse').hide();
                }
            });

        });

        $('.confirmSignature').click(function(e) {
            event.preventDefault();
            var href = $(this).attr('href');
            swal({
                title: "Êtes-vous sûr?",
                text: "Confirmez-vous que la facture physique a été établie !",
                icon: "warning",
                buttons: true,
                buttons: ["Annuler", "Oui"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    window.location = href;
                }
            });
        });

        $('#check_all').on('click', function(e){
            if($(this).is(':checked',true)){
                $(".role_item").prop('checked', true);
            }else{
                $(".role_item").prop('checked', false);
            }
        });

        $('#sendActionGroup').on('click', function(e) {

            if($('#actionGroup').val()=='del_select'){
                var allVals = [];
                $(".role_item:checked").each(function() {
                    allVals.push($(this).attr('data-id'));
                });
                //console.log(allVals.length);
                // return false;
                if(allVals.length <=0)
                {
                    swal("Oops","Veuillez selectionnez des lignes à supprimer!","error" )
                }
                else {
                    //$("#loading").show();
                    swal({
                        title: "Êtes-vous sûr?",
                        text: "Voulez vous vraiment supprimer ces lignes",
                        icon: "warning",
                        buttons: true,
                        buttons: ["Annuler", "Oui"],
                        dangerMode: true,
                    }).then((willDelete) => {
                        if(willDelete) {
                            var valueIds = allVals.join(",");

                            $.ajax({
                                url:"{{ route('news.deletes') }}",
                                method:"GET",
                                data:{value:valueIds},
                                success:function(res){
                                    console.log(res);
                                    if(res == '1'){
                                        //$("#loading").hide();
                                        location.reload();
                                    }else{
                                        //$("#loading").hide();
                                        location.reload();
                                    }
                                }
                            })
                        }
                    });
                }
            }else{
                return false;
                //swal("Oops","Something went wrong!","error" )
            }

        });

        function update_status(el,ref){
            if(el.checked){
                var status = 1;
            }else{
                var status = 0;
            }
            //console.log(el,ref);
            $('#myModal').modal('show');

            var id = el.value;
            $("#tidsluge").val(id);
            $("#tlbbe").html(ref);
            /*var token =$("meta[property='csrf-token']").attr('content');

            $.post("{route('bl.status')}}", {_token:token, id:el.value, status:status}, function(data){
                if(data == 1){
                    document.getElementById('redaonly').readOnly = true;
                    swal("Succès!","Le bon de livraison a bien été traité","success");
                    location.reload();
                }else{
                    swal("Erreur!","Un problème est survenu","error");
                }
            });*/
        }

        function showform(){
            $('#etats').show();
            $('#showform').hide();
            $('#hideform').show();
        }
        function hideform(){
            $('#etats').hide();
            $('#hideform').hide();
            $('#showform').show();
        }

        //Onclick #submitbtn show sweetalert confirmation
        $('#submitbtn').on('click', function(e) {
            e.preventDefault();
            swal({
                title: "Merci de confirmer",
                text: "Cette facture a t-elle une remise ?",
                icon: "warning",
                buttons: true,
                buttons: ["Non", "Oui"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    //show sweetalert with input
                    swal({
                        title: "Remise",
                        text: "Veuillez entrer le montant de la remise",
                        content: "input",
                        icon: "warning",
                        buttons: true,
                        buttons: ["Annuler", "Valider"],
                        dangerMode: true,
                    }).then((inputValue) => {
                        if (inputValue) {
                            // console.log(inputValue);
                            if (inputValue === null){
                                swal("Ok","Merci de saisir le montant de la remise","error");
                                return false ;
                                //$('#validfacture').submit();
                            }
                            if (inputValue === "") {
                                swal("Ok","Merci de saisir le montant de la remise","error");
                                return false ;
                            }

                            //verifier si le montant est un nombre
                            if (isNaN(inputValue)) {
                                swal("Ok","La valeur saisie n'est pas un nombre","error");
                                return false ;
                            }

                            //put the value in the input
                            $('#remise').val(inputValue);
                            //submit the form
                            $('#validfacture').submit();
                        }else{
                            //show erreur message
                            swal("Ok","Veuiilez saisir le montant de la remise","error");
                            return false;
                        }
                    });

                }else{
                    swal("Ok","Enregistrement de la facture sans remise en cours","success");
                    $('#validfacture').submit();
                }
            });
        });

    </script>
@endsection


@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="page-title">
                            Factures en attente de traitement
                        </h4>
                        <div class="pull-right">
                            <button onclick="showform()" id="showform"  class="btn btn-default btn-xs"><i class="fa fa-bar-chart"></i> Etat</button>
                            <button onclick="hideform()" id="hideform" class="btn btn-default btn-xs" style="display: none;"><i class="fa fa-bar-chart"></i> Fermer </button>
                        </div>
                        <ol class="breadcrumb"> </ol>
                    </div>
                </div>

                @include('factures.etat')

                <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="standard-modalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="standard-modalLabel">Factures<br>
                                    <small class="text-success"><span id="tlbbe"></span></small>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                </h4>
                            </div>
                            <form id="validfacture" enctype="multipart/form-data" action="{{route('factures.validbl')}}" class="form-horizontal" method="post" autocomplete="off">
                                @csrf
                                <input type="hidden" id="tidsluge" name="tidsluge">
                                <div class="modal-body">
                                    <input type="hidden" name="remise" id="remise" value="0">
                                    <div class="form-group">
                                        <div class="form-group col-sm-12">
                                            <label class="col-md-12" for="password">Mode de paiement <span class="text-danger">*</span></label>
                                            <select name="modepaie" id="modepaie" class="form-control">
                                                <option selected disabled>Mode de paiement</option>
                                                {{--@foreach($modes as $mode)--}}
                                                <option value="1">Chèque</option>
                                                <option value="2">Virement</option>
                                                <option value="3">Avance</option>
                                                {{--@endforeach--}}
                                            </select>
                                        </div>
                                        <div class="form-group col-md-12" id="cheque" style="display: none;">
                                            <label for="numcheque">Numéro du chèque</label>
                                            <input type="text" class="form-control" name="numchequeone" id="numcheque">
                                        </div>
                                        <div class="form-group col-md-12" id="virement" style="display: none;">
                                            <label for="datevirement">Date de virement</label>
                                            <input type="date" class="form-control" name="datevirement" id="datevirement">
                                        </div>

                                        <div id="avance" style="display: none;">
                                            <select name="avance" id="avance_select" class="form-control">
                                                <option selected disabled>Mode d'avance</option>
                                                <option value="espece">Espèce</option>
                                                <option value="cheque">Chèque</option>
                                            </select>

                                            <div class="form-group col-md-12" id="avance_espece" style="display: none;">
                                                <label for="numcheque">Numéro du chèque</label>
                                                <input type="text" class="form-control" name="numcheque" id="numcheque_avance">
                                            </div>
                                            <div class="form-group col-md-12" id="avance_caisse" style="display: none;">
                                                <label for="datevirement">Numéro du bon de caisse</label>
                                                <input type="text" class="form-control" name="numboncaisse" id="numboncaisse_avance">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-light" data-dismiss="modal">Annuler</button>
                                    <button type="button" id="submitbtn" class="btn btn-primary">Enregistrer</button>
                                </div>
                            </form>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div>


                <div class="row">
                    <div class="col-md-12">
                        <div class="card-box table-responsive">
                            <table id="myTable" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Ref.No.</th>
                                    <th>Nom</th>
                                    <th>Project</th>
                                    <th>Facture physique</th>
                                    <th>Status</th>
                                    <th>Montant</th>
                                    {{--<th>Date d'ajout</th>--}}
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($bons as $k=>$bon)
                                    <tr>
                                        <td>{{$bon->reference ?? 'N/A'}} </td>
                                        <td style="font-weight: bold">
                                            {{$bon->slug}}
                                            <div class="row-actions-edit">
                                                <span class="edit"> <a href="{{route('factures.show',$bon->id)}}" target="_blank">Voir</a> | </span>
                                                <span class="trash"><a href="{{route('factures.download',$bon->id)}}" style="color: red">Télécharger</a></span>
                                            </div>
                                        </td>
                                        <td>{{$bon->project->libelle}}</td>
                                        <td>
                                            @if($bon->facturephysique == 0)
                                                <span class="label label-table label-danger">Non établie</span>
                                            @else
                                                <span class="label label-table label-success">Etablie</span>
                                            @endif
                                        </td>
                                        <td>@if($bon->facturephysique == 0)
                                                <a href="{{route('factures.getFphysik',encrypt($bon->id))}}" class="confirmSignature btn btn-sm btn-warning">
                                                    <span class="fa fa-exclamation-triangle"></span> Etablie
                                                </a>
                                            @else

                                                @if($bon->etat_facture == 0)
                                                    <button type="button" class="btn btn-danger btn-sm waves-effect waves-light" onclick="update_status(this,`{{$bon->slug}}`)" value="{{$bon->id}}"><i class="fa fa-edit"></i> En attente de paiement</button>
                                                @else
                                                    <span class="label label-default">Traité</span><br>
                                                    <small class="text-success">{{$bon->modepaie->libelle}}</small><br>
                                                    <small class="text-info">
                                                    @if($bon->mode_facture == 1)
                                                        Numéro du chèque : {{$bon->valeur_facture}}
                                                    @elseif($bon->mode_facture == 2)
                                                        Date de virement : {{$bon->valeur_facture ? date('d/m/Y',strtotime($bon->valeur_facture)):''}}
                                                    @else
                                                        @if($bon->type_facture=='espece')
                                                            Numéro du bon de caisse : {{$bon->valeur_facture}}
                                                        @else
                                                            Numéro du chèque : {{$bon->valeur_facture}}
                                                        @endif
                                                    @endif
                                                    </small>
                                                @endif
                                            @endif
                                        </td>
                                        <td>
                                            <p title="Montant Hors taxes">HT : <span class="label label-table label-info">@price($bon->total['ht']) Fcfa</span></p>
                                            <p title="Montant Toute taxe comprise">TTC : <span class="label label-table label-success">@price($bon->total['ttc']) Fcfa</span></p>
                                        </td>
{{--                                        <td>{{$bon->updated_at->format('d/m/Y')}}</td>--}}
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div> <!-- container -->
        </div> <!-- content -->
    </div>
@endsection
