<html>
<style>
    body{
        font-size: 10px;
        margin: 1.5px;
    }
    table {
        font-size: 11px;
    }

    .td{
        border: 1px solid #000000;
        padding-left: 5px;
        padding: 10px;
    }
    .txt-center{
        text-align: center;
    }
    .txt-right{
        text-align: right;
    }
    #resultscol td{
        border: 1px solid #000000;
        padding: 3px;
    }

    #fin ul li{
        list-style: none;
        text-align: left;
    }

    .custom-control-label::before,
    .custom-control-label::after {
        top: 0.1rem !important;
        left: -2rem !important;
        width: 1.25rem !important;
        height: 1.25rem !important;
    }

    footer {
        position: fixed;
        bottom: 0cm;
        left: 0cm;
        right: 0cm;
        height: 2cm;

        /** Extra personal styles **/
        color: #000;
        font-size: 10px;
        text-align: center;
    }

</style>
<body>
<div>
    <table border="0" cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td>
                <div class="col">
                    <img src="{{asset('assets/images/logo_dark.png')}}" data-holder-rendered="true" height="50px" />

                </div>
            </td>

        </tr>
    </table>
    <br>
    <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td align="center">
                <p style="font-size: 18px;font-weight: bold">FACTURE</p>
            </td>
        </tr>

    </table>
    <br>
    <table border="0" cellspacing="0" cellpadding="0" align="center" width="100%" >
        <tr>

            <td width="75%">
                <div class="col company-details">
                    <strong>{{$bl->client ? $bl->client->nom : ''}}</strong><br>
                    {{$bl->client ? $bl->client->adresse : ''}}<br>
                    {{$bl->client ? 'Contact: '.$bl->client->contact : ''}}
                </div>
            </td>
            <td>
                <div class="address">
                    Date: {{$bl->created_at->format('d/m/Y')}}<br>
                    Projet :  <strong>{{$bl->project->libelle}}</strong><br>
                    N° :  <strong>{{$bl->reference ?? $bl->slug}}</strong>
                </div>
            </td>
        </tr>
    </table>
    <br/>
    <br>

    <table style="" cellspacing="0" cellpadding="0" align="center" width="100%">
        <tr style="background-color: #e1e1e8">
            <td class="td"><strong>DESIGNATION</strong></td>
            <td class="td"><strong>UNITE</strong></td>
            <td class="td"><strong>QUANTITES</strong></td>
            <td class="td"><strong>PRIX UNITAIRE</strong></td>
            <td class="td"><strong>TOTAL HT</strong></td>
        </tr>
        @php $sstotal = 0 @endphp
        @foreach($listbls as $k=>$listbl)
            <tr class="">
                <td class="td">{{$listbl->produits->nom}}</td>
                @php
                    $unit =\App\Typeproduit::find($listbl->produits->type_id);
                    $total = $listbl->qte * $listbl->price ;
                    $sstotal = $sstotal + $total;
                @endphp
                <td class="td">{{$unit ? $unit->libelle :''}}</td>
                <td class="td">{{$listbl->qte}}</td>
                <td class="td">@price($listbl->price) Fcfa</td>
                <td class="td">@price($total) Fcfa</td>
            </tr>
        @endforeach
        @php $sstotal = $sstotal - $bl->remise @endphp
        @if($bl->remise != 0)
        <tr>
            <td colspan="3"></td>
            <td style="text-align:center">Remise: </td>
            <td style="padding:3mm;border:1px #CFD1D2 solid;"><strong>@price($bl->remise) Fcfa</strong></td>
        </tr>@endif
        <tr>
            <td colspan="3"></td>
            <td style="text-align:center">Total HT: </td>
            <td style="padding:3mm;border:1px #CFD1D2 solid;"><strong>@price($sstotal) Fcfa</strong></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td style="text-align:center">TVA (18%) : </td>
            <td style="padding:3mm;border:1px #CFD1D2 solid;"><strong>@price($tva = 0.18 * $sstotal) Fcfa</strong></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td style="text-align:center">Total TTC : </td>
            <td style="padding:3mm;border:1px #CFD1D2 solid;"><strong>@price($sstotal + $tva) Fcfa</strong></td>
        </tr>

    </table>
    <br>
</div>
<footer>
    Cocody Angré, 8ème Tranche, Résidence Pacific Villa 73, 01 BP 473 Abidjan 01 <br>
    Tél. (225)06 32 86 36 / (225) 59 59 09 91, RC : CI-ABJ-2018-B-14915, CC : 18 29 457 U, Capital Social : 10.000.000 FCFA, Régime d'imposition : Réel simplifié
</footer>
</body>
</html>
