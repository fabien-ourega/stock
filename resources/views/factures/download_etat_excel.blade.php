<html>
<style>
    body{
        font-size: 10px;
        margin: 1.5px;
    }
    table {
        font-size: 11px;
    }

    .td{
        border: 1px solid #000000;
        padding-left: 5px;
        padding: 10px;
    }
    .txt-center{
        text-align: center;
    }
    .txt-right{
        text-align: right;
    }
    #resultscol td{
        border: 1px solid #000000;
        padding: 3px;
    }

    #fin ul li{
        list-style: none;
        text-align: left;
    }

    .custom-control-label::before,
    .custom-control-label::after {
        top: 0.1rem !important;
        left: -2rem !important;
        width: 1.25rem !important;
        height: 1.25rem !important;
    }

    footer {
        position: fixed;
        bottom: 0cm;
        left: 0cm;
        right: 0cm;
        height: 2cm;

        /** Extra personal styles **/
        color: #000;
        font-size: 10px;
        text-align: center;
    }

</style>
<body>
<div>

    <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td align="center">
                <p style="font-size: 18px;font-weight: bold">ETAT DES FACTURES</p>
                @if(isset($date) and isset($date2))
                <span style="font-size: 14px">Du {{date('d/m/Y',strtotime($date))}} au {{date('d/m/Y',strtotime($date2))}}</span>
                @endif
            </td>
        </tr>

    </table>
    <br>
    <br>
    <table style="" cellspacing="0" cellpadding="0" align="center" width="100%">
        <tr style="background-color: #e1e1e8">
            <td class="td"><strong>NUM</strong></td>
            <td class="td"><strong>REFERENCE</strong></td>
            <td class="td"><strong>NOM</strong></td>
            <td class="td"><strong>DETAIL</strong></td>
            <td class="td"><strong>PROJET</strong></td>
            <td class="td"><strong>CLIENT</strong></td>
            <td class="td"><strong>STATUS</strong></td>
            <td class="td"><strong>MODE PAIEMENT</strong></td>
            <td class="td"><strong>DATE PAIEMENT</strong></td>
            <td class="td"><strong>REMISE</strong></td>
            <td class="td"><strong>MONTANT HT</strong></td>
            <td class="td"><strong>MONTANT TTC</strong></td>
        </tr>
        @php $totalht = 0;$totalttc = 0  @endphp
        @foreach($datas as $k=>$data)
            @php $totalht = $totalht + $data->total['ht'];
                $totalttc = $totalttc + $data->total['ttc'];
                $listbls = \App\Blitem::where('bl_id',$data->id)->with('produits')->get();
            @endphp
            <tr class="">
                <td class="td">{{$k+1}}</td>
                <td class="td">{{$data->reference ?? '-'}}</td>
                <td class="td">{{$data->slug}}</td>
                <td class="td">
                    @foreach($listbls as $k=>$listbl)
                        <li> {{$listbl->produits ? $listbl->produits->nom : '-'}}  | QTE: {{$listbl->qte}} | PU: @price($listbl->price)</li>
                    @endforeach
                </td>
                <td class="td">{{$data->project->libelle}}</td>
                <td class="td">{{$data->client ? $data->client->nom : '-'}}</td>
                <td class="td">{{$data->etat_facture==1 ? "TRAITE" : "EN COURS DE TRAITEMENT"}}</td>
                <td class="td">{{$data->modepaie ? $data->modepaie->libelle: ''}}</td>
                <td class="td">
                    @if($data->etat_facture==1)
                        {{$data->updated_at->format('d/m/Y')}}
                    @else
                        -
                    @endif
                </td>
                <td class="td">@if($data->remise != 0)@price($data->remise) Fcfa @else - @endif</td>
                <td class="td">@price($data->total['ht'])</td>
                <td class="td">@price($data->total['ttc'])</td>
            </tr>
        @endforeach
        <tr class="">
            <td class="" colspan="7" style="text-align: right;padding-right: 10px;">
                <strong>TOTAL</strong>
            </td>
            <td class="td"><strong>@price($totalht)</strong></td>
            <td class="td"><strong>@price($totalttc)</strong></td>
        </tr>

    </table>
    <br>
</div>
</body>
</html>
