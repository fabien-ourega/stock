<html>
<style>
    body{
        font-size: 10px;
        margin: 1.5px;
    }
    table {
        font-size: 10px;
    }

    .td{
        border: 1px solid #000000;
        padding-left: 5px;
        padding: 7px;
    }
    .txt-center{
        text-align: center;
    }
    .txt-right{
        text-align: right;
    }
    #resultscol td{
        border: 1px solid #000000;
        padding: 3px;
    }

    #fin ul li{
        list-style: none;
        text-align: left;
    }

    .custom-control-label::before,
    .custom-control-label::after {
        top: 0.1rem !important;
        left: -2rem !important;
        width: 1.25rem !important;
        height: 1.25rem !important;
    }

    footer {
        position: fixed;
        bottom: 0cm;
        left: 0cm;
        right: 0cm;
        height: 2cm;

        /** Extra personal styles **/
        color: #000;
        font-size: 10px;
        text-align: center;
    }

</style>
<body>
<div>
    <table border="0" cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td>
                <div class="col">
                    <img src="{{asset('assets/images/logo_dark.png')}}" data-holder-rendered="true" height="50px" />

                </div>
            </td>

        </tr>
    </table>
    <br><br><br>
    <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td align="center">
                <p style="font-size: 18px;font-weight: bold">ETAT DES FACTURES</p>
                @if(isset($date) and isset($date2))
                <span style="font-size: 14px">Du {{date('d/m/Y',strtotime($date))}} au {{date('d/m/Y',strtotime($date2))}}</span>
                @endif
            </td>
        </tr>

    </table>
    <br>
    <br>
    <table style="" cellspacing="0" cellpadding="0" align="center" width="100%">
        <tr style="background-color: #e1e1e8">
            <td class="td"><strong>NUM</strong></td>
            <td class="td"><strong>REFERENCE</strong></td>
            <td class="td"><strong>NOM</strong></td>
            <td class="td"><strong>PROJET</strong></td>
            <td class="td"><strong>CLIENT</strong></td>
            <td class="td"><strong>STATUS</strong></td>
            <td class="td"><strong>MODE PAIEMENT</strong></td>
            <td class="td"><strong>DATE PAIEMENT</strong></td>
            <td class="td"><strong>REMISE</strong></td>
            <td class="td"><strong>MONTANT HT</strong></td>
            <td class="td"><strong>MONTANT TTC</strong></td>
        </tr>
        @php $totalht = 0;$totalttc = 0  @endphp
        @foreach($datas as $k=>$data)
            @php $totalht = $totalht + $data->total['ht'];
                $totalttc = $totalttc + $data->total['ttc']
            @endphp
            <tr class="">
                <td class="td">{{$k+1}}</td>
                <td class="td">{{$data->reference ?? '-'}}</td>
                <td class="td">{{$data->slug}}</td>
                <td class="td">{{$data->project->libelle}}</td>
                <td class="td">{{$data->client ? $data->client->nom : '-'}}</td>
                <td class="td">{{$data->etat_facture==1 ? "TRAITE" : "EN COURS DE TRAITEMENT"}}</td>
                <td class="td">{{$data->modepaie ? $data->modepaie->libelle: '-'}}</td>
                <td class="td">
                    @if($data->etat_facture==1)
                        {{$data->updated_at->format('d/m/Y')}}
                    @else
                        -
                    @endif
                </td>
                <td class="td">@if($data->remise != 0)@price($data->remise) Fcfa @else - @endif</td>
                <td class="td">@price($data->total['ht']) Fcfa</td>
                <td class="td">@price($data->total['ttc']) Fcfa</td>
            </tr>
        @endforeach
        <tr class="">
            <td class="" colspan="9" style="text-align: right;padding-right: 10px;">
                <strong>TOTAL</strong>
            </td>
            <td class="td"><strong>@price($totalht) Fcfa</strong></td>
            <td class="td"><strong>@price($totalttc) Fcfa</strong></td>
        </tr>

    </table>
    <br>
</div>
<footer>
    Cocody Angré, 8ème Tranche, Résidence Pacific Villa 73, 01 BP 473 Abidjan 01 <br>
    Tél. (225)06 32 86 36 / (225) 59 59 09 91, RC : CI-ABJ-2018-B-14915, CC : 18 29 457 U, Capital Social : 10.000.000 FCFA, Régime d'imposition : Réel simplifié
</footer>
</body>
</html>
