@extends('layouts.app')

@section('title')
    Facture
    @parent
@stop

@section('header_styles')
    <!-- DataTables -->
    <link href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
    <style>
        tr.spacend td{
            padding-bottom: 3em!important;
        }
    </style>
@endsection

@section('footer_scripts')
    <!-- Sweet-Alert  -->
    <script src="{{ asset('assets/pages/jquery.sweetalert.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
        });
    </script>
@endsection


@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="page-title">
                            Facture
                        </h4>
                        <ol class="breadcrumb"> </ol>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">

                            <div class="panel-body">
                                <div class="clearfix">
                                    <div class="pull-left">
                                        <h4 class="text-right"><img src="{{asset('assets/images/logo_dark.png')}}" alt="velonic" width="150"></h4>

                                    </div>
                                    <div class="pull-right">
                                        <h4>Facture <br>
                                            <strong>{{$bl->slug}}</strong>
                                        </h4>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="pull-left m-t-30">
                                            <address>
                                                <strong>{{$bl->client ? $bl->client->nom : ''}}</strong><br>
                                                {{$bl->client ? $bl->client->adresse : ''}}<br>
                                                {{$bl->client ? 'Contact: '.$bl->client->contact : ''}}
                                            </address>
                                        </div>
                                        <div class="pull-right m-t-30">
                                            <p><strong>Date: </strong> {{date('d/m/Y')}}</p>
                                            <p class="m-t-10"><strong>Status: </strong> @if($bl->etat_facture==1)<span class="label label-success">Payé</span>@else <span class="label label-danger">En attente de paiement </span> @endif</p>
                                            @if($bl->etat_facture==1)
                                                <p class="m-t-10"><strong>Mode de paiement: </strong> <span class="label label-success">{{$bl->modepaie->libelle}}</span></p>
                                                <p class="m-t-10"><strong>
                                                    @if($bl->mode_facture == 1)
                                                        Numéro du chèque :
                                                        @elseif($bl->mode_facture == 2)
                                                            Date de virement : <span class="label label-info">{{$bl->valeur_facture}}</span>
                                                        @else
                                                            @if($bl->type_facture=='espece')
                                                                Numéro du bon de caisse : <span class="label label-info">{{$bl->valeur_facture}}</span>
                                                            @else
                                                                Numéro du chèque : <span class="label label-info">{{$bl->valeur_facture}}</span>
                                                            @endif
                                                        @endif
                                                    </strong>

                                                </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="m-h-50"></div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table m-t-30">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Désignation</th>
                                                    <th>Unité</th>
                                                    <th>Quantités</th>
                                                    <th>Prix unitaire</th>
                                                    <th>Total HT</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @php $sstotal = 0 @endphp
                                                @foreach($listbls as $k=>$listbl)
                                                    {{--<tr class="{{count($listbls) == $k+1 ? 'spacend' : ''}}">--}}
                                                    <tr>
                                                        <td>{{$k+1}}</td>
                                                        <td>{{$listbl->produits->nom}}</td>
                                                        @php
                                                            $unit =\App\Typeproduit::find($listbl->produits->type_id);
                                                            $total = $listbl->qte * $listbl->price ;
                                                            $sstotal = $sstotal + $total;
                                                        @endphp
                                                        <td>{{$unit ? $unit->libelle :''}}</td>
                                                        <td>{{$listbl->qte}}</td>
                                                        <td>@price($listbl->price) Fcfa</td>
                                                        <td>@price($total) Fcfa</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row" style="border-radius: 0px;">
                                    <div class="col-md-9">
                                        @php
                                            $sstotal= $sstotal - $bl->remise;
                                            $tva = 0.18 * $sstotal;
                                            $numberToWords = new NumberToWords\NumberToWords();
                                            $numberTransformer = $numberToWords->getNumberTransformer('fr');
                                            $tt = $numberTransformer->toWords($sstotal + $tva);
                                        @endphp
                                        <br><br>
                                        <p>
                                            Arrêtée la presente facture pour la somme de :<br> <span class="text-uppercase"><b>{{$tt}} FRANCS CFA.</b></span>
                                        </p>
                                    </div>
                                    <div class="col-md-3">
                                        @if($bl->remise != 0)<p class="text-right"><b>Remise: </b>@price($bl->remise) Fcfa</p>@endif
                                        <p class="text-right"><b>Total HT: </b>@price($sstotal) Fcfa</p>
                                        <p class="text-right"><b>TVA (18%) </b>: @price($tva) Fcfa</p>
                                        <hr>
                                        <h3 class="text-right">Total TTC : @price($sstotal + $tva) Fcfa</h3>
                                    </div>
                                </div>
                                <hr>
                                <div class="hidden-print">
                                    <div class="pull-right">
                                        <a href="{{route('factures.download',$bl->id)}}" class="btn btn-inverse waves-effect waves-light"><i class="fa fa-download"></i> Télécharger</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </div> <!-- container -->
        </div> <!-- content -->
    </div>
@endsection
