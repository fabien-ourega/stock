<div style="margin-left: 240px;overflow: hidden;padding: 5px 5px 0px 5px;margin-bottom: -60px;margin-top: 60px">
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span></button>
            <h4><i class="icon fa fa-check"></i> Success !</h4>
            {!!Session::get('success')!!}
        </div>
    @endif

    @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true" style="color: #fff">×</span></button>
            <h4><i class="icon fa fa-ban"></i> Alert !</h4>
            {!!Session::get('error')!!}
        </div>
    @endif

    @if($errors->any())
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true" style="color: #fff">×</span></button>
            @foreach($errors->all() as $errorr)
                {!! $errorr !!}<br/>
            @endforeach
        </div>
    @endif

</div>
