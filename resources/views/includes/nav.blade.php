<!-- ========== Left Sidebar Start ========== -->

<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <!--- Divider -->
        <div id="sidebar-menu">
            <ul>

                {{--<li class="text-muted menu-title">Navigation</li>--}}

                <li> <a href="{{route('home')}}"><i class="fa fa-dashboard"></i> <span> Tableau de bord </span></a></li>


                <li class="text-muted menu-title">Gestion</li>

                @if(in_array('all_mgs', Session::get('permissions')))
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-hospital-o"></i><span>Magasins <span class="menu-arrow"></span> </span> </a>
                    <ul class="list-unstyled">
                        <li><a href="{{route('magasins')}}">Tous les magasins</a></li>
                        <li><a href="{{route('magasins.create')}}">Ajouter un magasin</a></li>
                    </ul>
                </li>
                @endif

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-houzz"></i><span>Stocks Projets<span class="menu-arrow"></span> </span> </a>
                    <ul class="list-unstyled">
                        <li><a href="{{route('gstock')}}">Tous les stocks</a></li>
                        <li><a href="{{route('gstock.storie')}}">Entrées / Sorties</a></li>
                    </ul>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-file"></i><span> Bon de commande <span class="menu-arrow"></span> </span> </a>
                    <ul class="list-unstyled">
                        <li><a href="{{route('bl.waiting')}}">BC attente validation</a></li>
                        <li><a href="{{route('bl')}}">BC attente traitement</a></li>
                        <li><a href="{{route('bl.valid')}}">BC traité</a></li>
                        <li><a href="{{route('bl.brouillon')}}">BC brouillon</a></li>
                        {{-- <li><a href="{{route('bl.indexarchive')}}">BC archivés</a></li> --}}
                        <li><a href="{{route('bl.rejeter')}}">BC rejetés</a></li>
                    </ul>
                </li>

                <li> <a href="{{route('bl.indexarchive')}}"><i class="fa fa-file-archive-o"></i> <span> BC archivés</span></a></li>


                @if(in_array('add_fct', Session::get('permissions')) )
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-file-pdf-o"></i><span> Factures <span class="menu-arrow"></span> </span> </a>
                    <ul class="list-unstyled">
                        <li><a href="{{route('factures')}}">Factures en attente</a></li>
                        <li><a href="{{route('factures.valid')}}">Factures traitées</a></li>
                    </ul>
                </li>
                @endif

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-user"></i><span> Clients <span class="menu-arrow"></span> </span> </a>
                    <ul class="list-unstyled">
                        <li><a href="{{route('clients.create')}}">Ajouter</a></li>
                        <li><a href="{{route('clients')}}">Tous les clients</a></li>
                    </ul>
                </li>
                @if (in_array('add_four', Session::get('permissions')))
                <li class="text-muted menu-title">Gest.Achats</li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-users"></i><span> Fournisseurs <span class="menu-arrow"></span> </span> </a>
                    <ul class="list-unstyled">
                        <li><a href="{{route('frsseur.create')}}">Ajouter</a></li>
                        <li><a href="{{route('frsseur')}}">Tous les fournisseurs</a></li>
                    </ul>
                </li>
                @endif
                @if (in_array('add_achat', Session::get('permissions')))
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-shopping-bag"></i><span> Achats <span class="menu-arrow"></span> </span> </a>
                    <ul class="list-unstyled">
                        <li><a href="{{route('achats.create')}}">Ajouter</a></li>
                        <li><a href="{{route('achats')}}">Tous les achats</a></li>
                        <li><a href="{{route('achats.rejeter')}}">Achats rejetés</a></li>
                    </ul>
                </li>
                @endif

                {{-- @if(in_array(Auth::user()->role_id, [1,3])) --}}

                    <li class="text-muted menu-title">Administration</li>

                    {{-- <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-shopping-cart"></i><span> Produits <span class="menu-arrow"></span> </span> </a>
                        <ul class="list-unstyled">
                            <li><a href="{{route('categories')}}">Catégories</a></li>
                            <li><a href="{{route('produits.types')}}">Type de produit</a></li>
                            <li><a href="{{route('produits')}}">Tous les produits</a></li>
                            <li><a href="{{route('produits.create')}}">Ajouter</a></li>
                        </ul>
                    </li> --}}

                    @if(in_array('show_pdt', Session::get('permissions')))
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-shopping-cart"></i><span> Produits <span class="menu-arrow"></span> </span> </a>
                        <ul class="list-unstyled">
                            <li><a href="{{route('categories')}}">Catégories</a></li>
                            <li><a href="{{route('produits.types')}}">Type de produit</a></li>
                            <li><a href="{{route('produits')}}">Tous les produits</a></li>
                            <li><a href="{{route('produits.create')}}">Ajouter</a></li>
                        </ul>
                    </li>
                    @endif

                    @if(in_array('add_pjt', Session::get('permissions')))
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-tasks"></i><span> Projets <span class="menu-arrow"></span> </span> </a>
                        <ul class="list-unstyled">
                            {{--<li><a href="{{route('projects.create')}}">Ajouter</a></li>--}}
                            <li><a href="{{route('projects')}}">Tous les projets</a></li>
                            <li><a href="{{route('projects.cloturer')}}">Projets clôturés</a></li>
                        </ul>
                    </li>
                    @endif
                    @if(Auth::user()->role_id==1)
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-users"></i><span> Utilisateurs <span class="menu-arrow"></span> </span> </a>
                        <ul class="list-unstyled">
                            <li><a href="{{route('users')}}">Tous les utilisateurs</a></li>
                            <li><a href="{{route('users.create')}}">Ajouter</a></li>
                            <li><a href="{{route('roles')}}">Rôles</a></li>
                        </ul>
                    </li>
                    @endif
                    {{--<li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-image"></i><span> Médiathèques </span> </a>
                        <ul class="list-unstyled">
                            <li><a href="{{route('albums')}}">Photos</a></li>
                            <li><a href="{{route('videos')}}">Vidéos</a></li>
                        </ul>
                    </li>--}}

                {{-- @endif --}}



            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- Left Sidebar End -->
