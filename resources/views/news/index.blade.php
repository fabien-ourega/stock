@extends('layouts.app')

@section('title')
    Actualités
    @parent
@stop

@section('header_styles')
    <!-- DataTables -->
    <link href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('footer_scripts')
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.js') }}"></script>

    <script src="{{ asset('assets/pages/datatables.init.js') }}"></script>

    <!-- Sweet-Alert  -->
    <script src="{{ asset('assets/pages/jquery.sweetalert.min.js') }}"></script>
    {{--<script src="{{ asset('assets/pages/jquery.sweet-alert.init.js') }}"></script>--}}

    <script type="text/javascript">
        $(document).ready(function () {
            $('#myTable').dataTable({
                language: {
                    lengthMenu: "_MENU_",
                    search: "_INPUT_",
                    searchPlaceholder: "Recherche",
                    sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                    sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    sInfoPostFix:    "",
                    sLoadingRecords: "Chargement en cours...",
                    sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
                    sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
                    sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    oPaginate: {
                        "sFirst":    "Premier",
                        "sLast":    "Dernier",
                        "sNext":    "Suivant",
                        "sPrevious": "Précédent"
                    },
                }
            });

        });

        $('.actus').on('click', '.delete', function (event) {
            event.preventDefault();
            var href = $(this).attr('href');
            swal({
                title: "Êtes-vous sûr?",
                text: "Voulez vous vraiment supprimer cet article",
                icon: "warning",
                buttons: true,
                buttons: ["Annuler", "Oui"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    window.location = href;
                }
        });
        });

        $('#check_all').on('click', function(e){
            if($(this).is(':checked',true)){
                $(".role_item").prop('checked', true);
            }else{
                $(".role_item").prop('checked', false);
            }
        });

        $('#sendActionGroup').on('click', function(e) {

            if($('#actionGroup').val()=='del_select'){
                var allVals = [];
                $(".role_item:checked").each(function() {
                    allVals.push($(this).attr('data-id'));
                });
                //console.log(allVals.length);
                // return false;
                if(allVals.length <=0)
                {
                    swal("Oops","Veuillez selectionnez des lignes à supprimer!","error" )
                }
                else {
                    //$("#loading").show();
                    swal({
                        title: "Êtes-vous sûr?",
                        text: "Voulez vous vraiment supprimer ces lignes",
                        icon: "warning",
                        buttons: true,
                        buttons: ["Annuler", "Oui"],
                        dangerMode: true,
                    }).then((willDelete) => {
                        if(willDelete) {
                            var valueIds = allVals.join(",");

                            $.ajax({
                                url:"{{ route('news.deletes') }}",
                                method:"GET",
                                data:{value:valueIds},
                                success:function(res){
                                    console.log(res);
                                    if(res == '1'){
                                        //$("#loading").hide();
                                        location.reload();
                                    }else{
                                        //$("#loading").hide();
                                        location.reload();
                                    }
                                }
                            })
                        }
                    });
                }
            }else{
                return false;
                //swal("Oops","Something went wrong!","error" )
            }

        });

    </script>
@endsection


@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="page-title">
                            Articles
                            <a href="{{route('news.create')}}" class="btn btn-default btn-xs">Ajouter</a>
                        </h4>
                        <ol class="breadcrumb"> </ol>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12">
                        <div class="card-box table-responsive">
                            <ul class="subsubsub">
                                <li><a href="{{route('news')}}" style="color: red">Tous ({{count($actus)}}) |</a></li>
                                <li><a href="{{route('news.show_type','publies')}}">Publiés ({{$countPublie}}) |</a></li>
                                <li><a href="{{route('news.show_type','corbeille')}}">Corbeille ({{$countDel}}) |</a></li>
                                <li><a href="{{route('news.show_type','brouillons')}}">Brouillons ({{$countBrouille}}) </a></li>
                            </ul>

                            <div class="form-inline">
                                <div class="form-group">
                                    <select name="actionGroup" class="form-control" id="actionGroup">
                                        <option value="">Actions groupée</option>
                                        <option value="del_select">Déplacer dans la corbeille</option>
                                    </select>
                                </div>
                                <button type="submit" id="sendActionGroup" class="btn btn-default waves-effect waves-light m-l-10 btn-md">Appliquer</button>
                            </div>
                            <br>

                            <table id="myTable" class="table table-striped table-bordered actus">
                                <thead>
                                <tr>
                                    {{--<th>Ref.No.</th>--}}
                                    <th> <input type="checkbox" id="check_all"></th>
                                    <th>Titre</th>
                                    <th>Auteur</th>
                                    <th>Catégories</th>
                                    <th>Date d'ajout</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($actus as $k=>$actu)
                                <tr>
                                    {{--<td>#0{{$k+1}} </td>--}}
                                    <td>
                                        @if(Auth::user()->role_id=='3')
                                            @if(Auth::user()->id==$actu->auteur)
                                            <input type="checkbox" class="role_item" data-id="{{$actu->id}}">
                                            @endif
                                        @else
                                            <input type="checkbox" class="role_item" data-id="{{$actu->id}}">
                                        @endif
                                    </td>
                                    <td style="font-weight: bold">
                                        {{$actu->title}}
                                        @if(Auth::user()->role_id=='3')
                                            @if(Auth::user()->id==$actu->auteur)
                                                <div class="row-actions-edit">
                                                    <span class="edit"> <a href="{{route('news.edit',$actu->id)}}">Modifier</a> | </span>
                                                    <span class="trash"><a href="{{route('news.delete',$actu->id)}}" class="delete" style="color: red">Corbeille</a> | </span>
                                                    <span class="view"><a href="{{route('news.show',$actu->slug)}}" rel="afficher">Afficher</a></span>
                                                </div>
                                            @else
                                                <div class="row-actions-edit">
                                                    <span class="view"><a href="{{route('news.show',$actu->slug)}}" rel="afficher">Afficher</a></span>
                                                </div>
                                            @endif
                                        @else
                                            <div class="row-actions-edit">
                                                <span class="edit"> <a href="{{route('news.edit',$actu->id)}}">Modifier</a> | </span>
                                                <span class="trash"><a href="{{route('news.delete',$actu->id)}}" class="delete" style="color: red">Corbeille</a> | </span>
                                                <span class="view"><a href="{{route('news.show',$actu->slug)}}" rel="afficher">Afficher</a></span>
                                            </div>
                                        @endif
                                    </td>
                                    <td>{{$actu->author->name}}</td>
                                    @php $cats = (new \App\Http\Controllers\NewsController())->getCatlibell($actu->cat_id) @endphp
                                    <td>
                                        @foreach($cats as $cat)
                                            {{$cat->libelle}} {{!$loop->last ? ',' : ''}}
                                        @endforeach
                                    </td>
                                    <td>{{$actu->created_at->format('d/m/Y')}}</td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div> <!-- container -->
        </div> <!-- content -->
    </div>
@endsection
