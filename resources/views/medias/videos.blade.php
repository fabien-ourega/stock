@extends('layouts.app')

@section('title')
    Vidéos
    @parent
@stop

@section('header_styles')
    <!-- DataTables -->
    <link href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>

@endsection

@section('footer_scripts')
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.js') }}"></script>

    <script src="{{ asset('assets/pages/datatables.init.js') }}"></script>

    <!-- Sweet-Alert  -->
    <script src="{{ asset('assets/pages/jquery.sweetalert.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#myTable').dataTable({
                language: {
                    lengthMenu: "_MENU_",
                    search: "_INPUT_",
                    searchPlaceholder: "Recherche",
                    sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                    sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    sInfoPostFix:    "",
                    sLoadingRecords: "Chargement en cours...",
                    sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
                    sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
                    sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    oPaginate: {
                        "sFirst":    "Premier",
                        "sLast":    "Dernier",
                        "sNext":    "Suivant",
                        "sPrevious": "Précédent"
                    },
                }
            });
        });


        $('.photos').on('click', '.delete', function (event) {
            event.preventDefault();
            var href = $(this).attr('href');
            swal({
                title: "Êtes-vous sûr?",
                text: "Voulez vous vraiment supprimer cette vidéo",
                icon: "warning",
                buttons: true,
                buttons: ["Annuler", "Oui"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    window.location = href;
                }
            });
        });

        $('#check_all').on('click', function(e){
            if($(this).is(':checked',true)){
                $(".role_item").prop('checked', true);
            }else{
                $(".role_item").prop('checked', false);
            }
        });

        $('#sendActionGroup').on('click', function(e) {

            if($('#actionGroup').val()!='action_groupp'){
                var allVals = [];
                $(".role_item:checked").each(function() {
                    allVals.push($(this).attr('data-id'));
                });
                //console.log(allVals.length);
                // return false;
                if(allVals.length <=0)
                {
                    swal("Oops","Veuillez selectionnez des lignes !","error" )
                }
                else {
                    if($('#actionGroup').val()=='del_select'){
                        //$("#loading").show();
                        swal({
                            title: "Êtes-vous sûr?",
                            text: "Voulez vous vraiment supprimer ces vidéos",
                            icon: "warning",
                            buttons: true,
                            buttons: ["Annuler", "Oui"],
                            dangerMode: true,
                        }).then((willDelete) => {
                            if(willDelete) {
                                var valueIds = allVals.join(",");

                                $.ajax({
                                    url:"{{ route('videos.deletes') }}",
                                    method:"GET",
                                    data:{value:valueIds},
                                    success:function(res){
                                        console.log(res);
                                        if(res == '1'){
                                            //$("#loading").hide();
                                            location.reload();
                                        }else{
                                            //$("#loading").hide();
                                            location.reload();
                                        }
                                    }
                                })
                            }
                        });
                    }else{
                        return false ;
                    }
                }
            }else{
                return false;
                //swal("Oops","Something went wrong!","error" )
            }

        });


    </script>
@endsection


@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">
                            Vidéo
                        </h4>
                        <ol class="breadcrumb"> </ol>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-4">

                        <div class="row">
                            <form class="form-horizontal" method="post" action="{{route('videos.store')}}" role="form">
                                @csrf
                                <div class="card-box">
                                    <h4 class="font-bold">Ajouter une nouvelle vidéo</h4>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <input class="form-control" type="lien" id="lien" name="lien" placeholder="Lien de la video youtube" required/>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary btn-md waves-effect waves-light" name="btn" value="add"><i class="fa fa-check-square"></i> Ajouter une nouvelle video</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                    <div class="col-md-8">
                        <div class="card-box table-responsive">

                            <div class="form-inline">
                                <div class="form-group">
                                    <select name="actionGroup" class="form-control" id="actionGroup">
                                        <option value="action_groupp">Actions groupée</option>
                                        <option value="del_select">Supprimer</option>
                                    </select>
                                </div>
                                <button type="submit" id="sendActionGroup" class="btn btn-default waves-effect waves-light m-l-10 btn-md">Appliquer</button>
                            </div>
                            <br>

                            <table id="myTable" class="table table-striped table-bordered photos">
                                <thead>
                                <tr>
                                    <th>
                                        <input type="checkbox" id="check_all">
                                    </th>
                                    <th>Titre</th>
                                    <th style="width: 60px">Vidéo</th>
                                    <th>Date d'ajout</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($videos as $k=>$video)
                                    <tr>
                                        <td>
                                            <input type="checkbox" class="role_item" data-id="{{$video->id}}">
                                        </td>
                                        <td style="font-weight: bold;text-transform: uppercase">
                                            {{$video->title}}
                                            <div class="row-actions-edit">
                                                <span class="trash"><a href="{{route('videos.delete',$video->id)}}" class="delete" style="color: red;text-transform: none">Supprimer</a></span>
                                            </div>
                                        </td>
                                        <td><img class="img-responsive" src="{{$video->img}}" width="80" height="80">
                                            <div class="row-actions-edit">
                                                <a href="{{$video->lien}}" target="_blank">Lire la vidéo</a>
                                            </div>
                                        </td>
                                        <td>
                                            {{$video->created_at->format('d/m/Y')}}
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div> <!-- container -->
        </div> <!-- content -->
    </div>
@endsection
