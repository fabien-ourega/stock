@extends('layouts.app')

@section('title')
    Photos
    @parent
@stop

@section('header_styles')
    <!-- DataTables -->
    <link href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('assets/plugins/dropify/dropify.css') }}">

@endsection

@section('footer_scripts')
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.js') }}"></script>

    <script src="{{ asset('assets/pages/datatables.init.js') }}"></script>

    <!-- Sweet-Alert  -->
    <script src="{{ asset('assets/pages/jquery.sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/dropify/dropify.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#myTable').dataTable({
                language: {
                    lengthMenu: "_MENU_",
                    search: "_INPUT_",
                    searchPlaceholder: "Recherche",
                    sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                    sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    sInfoPostFix:    "",
                    sLoadingRecords: "Chargement en cours...",
                    sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
                    sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
                    sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    oPaginate: {
                        "sFirst":    "Premier",
                        "sLast":    "Dernier",
                        "sNext":    "Suivant",
                        "sPrevious": "Précédent"
                    },
                }
            });
        });

        $('.dropify').dropify();

        $('.photos').on('click', '.delete', function (event) {
            event.preventDefault();
            var href = $(this).attr('href');
            swal({
                title: "Êtes-vous sûr?",
                text: "Voulez vous vraiment supprimer cette photo",
                icon: "warning",
                buttons: true,
                buttons: ["Annuler", "Oui"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    window.location = href;
                }
            });
        });

        $('#check_all').on('click', function(e){
            if($(this).is(':checked',true)){
                $(".role_item").prop('checked', true);
            }else{
                $(".role_item").prop('checked', false);
            }
        });

        $('#sendActionGroup').on('click', function(e) {

            if($('#actionGroup').val()!='action_groupp'){
                var allVals = [];
                $(".role_item:checked").each(function() {
                    allVals.push($(this).attr('data-id'));
                });
                //console.log(allVals.length);
                // return false;
                if(allVals.length <=0)
                {
                    swal("Oops","Veuillez selectionnez des lignes !","error" )
                }
                else {
                    if($('#actionGroup').val()=='del_select'){
                        //$("#loading").show();
                        swal({
                            title: "Êtes-vous sûr?",
                            text: "Voulez vous vraiment supprimer ces photos",
                            icon: "warning",
                            buttons: true,
                            buttons: ["Annuler", "Oui"],
                            dangerMode: true,
                        }).then((willDelete) => {
                            if(willDelete) {
                                var valueIds = allVals.join(",");

                                $.ajax({
                                    url:"{{ route('photos.deletes') }}",
                                    method:"GET",
                                    data:{value:valueIds},
                                    success:function(res){
                                        console.log(res);
                                        if(res == '1'){
                                            //$("#loading").hide();
                                            location.reload();
                                        }else{
                                            //$("#loading").hide();
                                            location.reload();
                                        }
                                    }
                                })
                            }
                        });
                    }else if($('#actionGroup').val()=='act_select'){
                        swal({
                            title: "Êtes-vous sûr?",
                            text: "Voulez vous vraiment activer ces lignes ",
                            icon: "warning",
                            buttons: true,
                            buttons: ["Annuler", "Oui"],
                            dangerMode: true,
                        }).then((willDelete) => {
                            if(willDelete) {
                                var valueIds = allVals.join(",");

                                $.ajax({
                                    url:"{{ route('photos.actives') }}",
                                    method:"GET",
                                    data:{value:valueIds},
                                    success:function(res){
                                        console.log(res);
                                        if(res == '1'){
                                            //$("#loading").hide();
                                            location.reload();
                                        }else{
                                            //$("#loading").hide();
                                            location.reload();
                                        }
                                    }
                                })
                            }
                        });
                    }else{
                        //$('#actionGroup').val()=='deact_select'
                        swal({
                            title: "Êtes-vous sûr?",
                            text: "Voulez vous vraiment désactiver ces lignes",
                            icon: "warning",
                            buttons: true,
                            buttons: ["Annuler", "Oui"],
                            dangerMode: true,
                        }).then((willDelete) => {
                            if(willDelete) {
                                var valueIds = allVals.join(",");

                                $.ajax({
                                    url:"{{ route('photos.inactives') }}",
                                    method:"GET",
                                    data:{value:valueIds},
                                    success:function(res){
                                        console.log(res);
                                        if(res == '1'){
                                            //$("#loading").hide();
                                            location.reload();
                                        }else{
                                            //$("#loading").hide();
                                            location.reload();
                                        }
                                    }
                                })
                            }
                        });
                    }
                }
            }else{
                return false;
                //swal("Oops","Something went wrong!","error" )
            }

        });


    </script>
@endsection


@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">
                            Photos
                        </h4>
                        <ol class="breadcrumb"> </ol>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-4">

                        <div class="row">
                            <form enctype="multipart/form-data" class="form-horizontal" method="post" action="{{route('photos.store')}}" role="form">
                                @csrf
                                <div class="card-box">
                                    <h4 class="font-bold">Ajouter une nouvelle photo</h4>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <input type="file" id="fileUser" name="fileUser[]" class="dropify" multiple/>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <input type="hidden" name="album" value="{{$idalbum}}">
                                                <button type="submit" class="btn btn-primary btn-md waves-effect waves-light" name="btn" value="add"><i class="fa fa-check-square"></i> Ajouter une nouvelle photo</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                    <div class="col-md-8">
                        <div class="card-box table-responsive">

                            <div class="form-inline">
                                <div class="form-group">
                                    <select name="actionGroup" class="form-control" id="actionGroup">
                                        <option value="action_groupp">Actions groupée</option>
                                        <option value="act_select">Activer</option>
                                        <option value="deact_select">Désactiver</option>
                                        <option value="del_select">Supprimer</option>
                                    </select>
                                </div>
                                <button type="submit" id="sendActionGroup" class="btn btn-default waves-effect waves-light m-l-10 btn-md">Appliquer</button>
                            </div>
                            <br>

                            <table id="myTable" class="table table-striped table-bordered photos">
                                <thead>
                                <tr>
                                    <th>
                                        <input type="checkbox" id="check_all">
                                    </th>
                                    <th>Photo</th>
                                    <th>Status</th>
                                    <th>Date d'ajout</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($photos as $k=>$photo)
                                    <tr>
                                        <td>
                                            <input type="checkbox" class="role_item" data-id="{{$photo->id}}">
                                        </td>
                                        <td style="font-weight: bold">
                                            <img class="img-responsive" src="{{asset('assets/mediatheques/photos/'.$photo->img)}}" width="80" height="80">
                                            <div class="row-actions-edit">
                                                @if($photo->status=='1')
                                                    <span class="edit"> <a href="{{route('photos.update',[$photo->id,'enable'])}}">Désactiver</a> | </span>
                                                @else
                                                    <span class="edit"> <a href="{{route('photos.update',[$photo->id,'enabled'])}}">Activer</a> | </span>
                                                @endif
                                                <span class="trash"><a href="{{route('photos.delete',$photo->id)}}" class="delete" style="color: red">Supprimer</a></span>
                                            </div>
                                        </td>
                                        <td>
                                            @if($photo->status=='1')
                                                <span class="label label-success">Actif</span>
                                            @else
                                                <span class="label label-danger">Inactif</span>
                                            @endif
                                        </td>
                                        <td>
                                            {{$photo->created_at->format('d/m/Y')}}
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div> <!-- container -->
        </div> <!-- content -->
    </div>
@endsection
